package com.majithg.rewardago.dailog;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.majithg.rewardago.util.App;

/**
 * Created by majithg on 09-May-16.
 */
public class DialogCliam extends DialogFragment
//        implements AdapterView.OnItemClickListener
{

    View body;
    Button button,button2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_claim, null, false);

//        mylist = (ListView) view.findViewById(R.id.list);
        body = view.findViewById(R.id.parentLayout_claim);

        button = (Button) view.findViewById(R.id.imageButton);
        button2 = (Button) view.findViewById(R.id.imageButton2);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Toast.makeText(getActivity(), getResources().getString(R.string.b1), Toast.LENGTH_SHORT).show();
                App.vp_message_main.setCurrentItem(3);
                MainActivity.bottomNavigation.setCurrentItem(3);
                getActivity().finish();

            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Toast.makeText(getActivity(), getResources().getString(R.string.b2), Toast.LENGTH_SHORT).show();
//                dismiss();

//                App.vp_message_main.setCurrentItem(3);
//                MainActivity.bottomNavigation.setCurrentItem(3);

                getActivity().finish();
                MainActivity.bottomNavigation.setCurrentItem(0);
                App.vp_message_main.setCurrentItem(0);


                /*new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        App.vp_message_main.setCurrentItem(0);

                        getActivity().finish();
                    }
                }, 1000);*/



            }
        });

    }


}

