package com.majithg.rewardago.dailog;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.majithg.rewardago.dashboard.Person;
import com.majithg.rewardago.util.App;

//import com.majithg.rewardago.util.App;

/**
 * Created by majithg on 09-May-16.
 */
public class DialogYAY extends DialogFragment
//        implements AdapterView.OnItemClickListener
{

    View body;
    Button button,button2;

//    ImageView imageView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_yay, null, false);

//        mylist = (ListView) view.findViewById(R.id.list);
        body = view.findViewById(R.id.parentLayout);

        button = (Button) view.findViewById(R.id.imageButton);
        button2 = (Button) view.findViewById(R.id.imageButton2);

       /* imageView = (ImageView) view.findViewById(R.id.qr_code);

        byte[] byteArray = getArguments().getByteArray("key");
        Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        imageView.setImageBitmap(bitmap);*/


        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Person.canAddItem = false;
                FragmentManager manager = App.appmyContext.getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.remove(manager.findFragmentByTag("MY_REDEMPTION"));
                transaction.commit();*/
                getActivity().finish();
                App.vp_message_main.setCurrentItem(0);
                MainActivity.bottomNavigation.setCurrentItem(0);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
                App.vp_message_main.setCurrentItem(4);
                MainActivity.bottomNavigation.setCurrentItem(4);
            }
        });

    }


}

