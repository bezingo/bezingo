package com.majithg.rewardago.dailog;

import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.majithg.rewardago.home.content.CouponDetailsActivity;
import com.majithg.rewardago.util.App;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by majithg on 09-May-16.
 */
public class DialogShare extends DialogFragment
//        implements AdapterView.OnItemClickListener
{

    private static final String TAG = DialogShare.class.getSimpleName();
    String pass;

//    String[] listitems = { "Second", "Minute", "Hour", "Day", "Week", "Fortnight", "Month", "Year" };

//    ListView mylist;
    View body;
    Button button;
    ImageView imageView,imageView2,imageView3,imageView4,imageView5;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_share, null, false);

//        mylist = (ListView) view.findViewById(R.id.list);
        body = view.findViewById(R.id.parentLayout);

        button = (Button) view.findViewById(R.id.shbt);

        imageView = (ImageView) view.findViewById(R.id.ic1);
        imageView2 = (ImageView) view.findViewById(R.id.ic2);
        imageView3 = (ImageView) view.findViewById(R.id.ic3);
        imageView4 = (ImageView) view.findViewById(R.id.ic4);
        imageView5 = (ImageView) view.findViewById(R.id.ic5);


        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return view;
    }

    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            Log.wtf(TAG, "UTF-8 should always be supported", e);
            throw new RuntimeException("URLEncoder.encode() failed for " + s);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
//                android.R.layout.simple_list_item_1, listitems);
//                R.layout.tv, listitems);

//        mylist.setAdapter(adapter);

//        mylist.setOnItemClickListener(this);



        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Toast.makeText(getActivity(), getResources().getString(R.string.b2), Toast.LENGTH_SHORT).show();
                MainActivity.bottomNavigation.setCurrentItem(0);
                App.vp_message_main.setCurrentItem(0);
                getActivity().finish();
                /*new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        App.vp_message_main.setCurrentItem(0);
                        getActivity().finish();
                    }
                }, 1000);*/

            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getActivity(), "fb", Toast.LENGTH_SHORT).show();
                String tweetUrl = String.format("https://www.facebook.com/sharer/sharer.php?u=%s",
//                        urlEncode(App.EXTRA_PARAM_TITLE+" - "),
                        urlEncode(CouponDetailsActivity.EXTRA_PARAM_COUPON_URL));
//                String tweetUrl = "https://www.facebook.com/sharer.php?s=100&p[url]="
//                        +"http://rewardago.com/coupon/hp-250-g4-discounted-price-rs-64000-00/";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(tweetUrl));
                getActivity().startActivity(intent);

            }
        });

        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Toast.makeText(getActivity(), "twit", Toast.LENGTH_SHORT).show();
                // Create intent using ACTION_VIEW and a normal Twitter url:
                String tweetUrl = String.format("https://twitter.com/intent/tweet?text=%s&url=%s",
//                        urlEncode(App.EXTRA_PARAM_TITLE+" - "),
                        urlEncode(CouponDetailsActivity.EXTRA_PARAM_TITLE+" - "),
//                        urlEncode("-"),
                        urlEncode(CouponDetailsActivity.EXTRA_PARAM_COUPON_URL));

                Log.v(TAG, tweetUrl);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(tweetUrl));
                getActivity().startActivity(intent);

            }
        });

        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getActivity(), "g+", Toast.LENGTH_SHORT).show();
                String tweetUrl = String.format("https://plus.google.com/share?url=%s",
//                        urlEncode(App.EXTRA_PARAM_TITLE+" - "),
                        urlEncode(CouponDetailsActivity.EXTRA_PARAM_COUPON_URL));
//                String tweetUrl = "https://www.facebook.com/sharer.php?s=100&p[url]="
//                        +"http://rewardago.com/coupon/hp-250-g4-discounted-price-rs-64000-00/";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(tweetUrl));
                getActivity().startActivity(intent);
            }
        });

        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getActivity(), "whatsapp", Toast.LENGTH_SHORT).show();
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
//                sendIntent.putExtra(Intent.EXTRA_TITLE,App.EXTRA_PARAM_TITLE);
                sendIntent.putExtra(Intent.EXTRA_TEXT, CouponDetailsActivity.EXTRA_PARAM_COUPON_URL);
                sendIntent.setType("text/plain");
                sendIntent.setPackage("com.whatsapp");
                startActivity(sendIntent);
            }
        });

        imageView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getActivity(), "mail", Toast.LENGTH_SHORT).show();
//                String[] recipients = new String[]{"sendme@me.com", ""};
                Intent testIntent = new Intent(android.content.Intent.ACTION_SEND);
                testIntent.setType("text/email");
                testIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, CouponDetailsActivity.EXTRA_PARAM_TITLE);
//                testIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, App.EXTRA_PARAM_TITLE);
//                testIntent.putExtra(android.content.Intent.EXTRA_TEXT, App.EXTRA_PARAM_SUBTITLE+"\n\n"
                testIntent.putExtra(android.content.Intent.EXTRA_TEXT, CouponDetailsActivity.EXTRA_PARAM_SUBTITLE+"\n\n"
                        +CouponDetailsActivity.EXTRA_PARAM_COUPON_URL);
//                testIntent.putExtra(android.content.Intent.EXTRA_EMAIL, recipients);
                startActivity(testIntent);
            }
        });



    }

   /* @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {

        dismiss();
        Toast.makeText(getActivity(), listitems[position], Toast.LENGTH_SHORT).show();

        pass = listitems[position];
    }*/

}

