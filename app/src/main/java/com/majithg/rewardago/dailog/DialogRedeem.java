package com.majithg.rewardago.dailog;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.majithg.rewardago.home.content.CouponDetailsActivity;
import com.majithg.rewardago.util.App;

//import com.majithg.rewardago.util.App;

/**
 * Created by majithg on 09-May-16.
 */
public class DialogRedeem extends DialogFragment
//        implements AdapterView.OnItemClickListener
{

    View body;
    Button button,button2;

    ImageView imageView;

    TextView qr_text, info_text, pickme;

    View view;

    private boolean shown = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.dialog_redeem, null, false);

//        mylist = (ListView) view.findViewById(R.id.list);
        body = view.findViewById(R.id.parentLayout);

//        button = (Button) view.findViewById(R.id.imageButton);
        button2 = (Button) view.findViewById(R.id.imageButton2);

        imageView = (ImageView) view.findViewById(R.id.qr_code);
        qr_text = (TextView) view.findViewById(R.id.qr_text);
        qr_text.setText(getArguments().getString("key_qr_text"));


        pickme = (TextView) view.findViewById(R.id.pickme);
        info_text = (TextView) view.findViewById(R.id.imageButton2_txt);

//        if(Integer.parseInt(App.EXTRA_PARAM_COUPON_TYPE)==2){
            if(Integer.parseInt(CouponDetailsActivity.EXTRA_PARAM_COUPON_TYPE)==2){
            info_text.setVisibility(View.GONE);
            button2.setVisibility(View.VISIBLE);
        } else if(Integer.parseInt(CouponDetailsActivity.EXTRA_PARAM_COUPON_TYPE)==1){
//                Toast.makeText(getActivity(),"Etype: "+getActivity().getIntent().getIntExtra(CouponDetailsActivity.EXTRA_PARAM_ENTERING_TPYE,0),Toast.LENGTH_SHORT).show();
                if(getActivity().getIntent().getIntExtra(CouponDetailsActivity.EXTRA_PARAM_ENTERING_TPYE,0)==5){
//                    Toast.makeText(getActivity(),"do nothing",Toast.LENGTH_SHORT).show();
                }else {
                    info_text.setVisibility(View.GONE);
                    button2.setVisibility(View.VISIBLE);
                }
            }


//        if(!App.EXTRA_PARAM_COUPON_HIDE_QR_CODE) {
        if(!CouponDetailsActivity.EXTRA_PARAM_COUPON_HIDE_QR_CODE) {
            byte[] byteArray = getArguments().getByteArray("key_qr_code");
            Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            imageView.setImageBitmap(bitmap);
        }else {
//            imageView.setVisibility(View.GONE);
            imageView.setVisibility(View.INVISIBLE);
//            qr_text.setText(getArguments().getString("key"));

            pickme.setText("This is your promo code");
        }



        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogYAY dialogYAY = new DialogYAY();
                dialogYAY.show(getFragmentManager(),"salaam");
//                dismiss();// unable to use after override
                onDismiss(getDialog()); // it's also working
            }
        });


    }



    @Override
    public void show(FragmentManager manager, String tag) {
        if (shown) return;

        super.show(manager, tag);
        shown = true;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        shown = false;
        super.onDismiss(dialog);

        if(info_text.getVisibility() == View.VISIBLE){
            getActivity().finish();
            MainActivity.bottomNavigation.setCurrentItem(0);
            App.vp_message_main.setCurrentItem(0);
        }

        /*if(Integer.parseInt(CouponDetailsActivity.EXTRA_PARAM_COUPON_TYPE)==1){
            getActivity().finish();
            MainActivity.bottomNavigation.setCurrentItem(0);
            App.vp_message_main.setCurrentItem(0);
        }*/


        /*if(Integer.parseInt(CouponDetailsActivity.EXTRA_PARAM_COUPON_TYPE)==1 && (
                getActivity().getIntent().getIntExtra(CouponDetailsActivity.EXTRA_PARAM_ENTERING_TPYE,0) ==3 ||
                getActivity().getIntent().getIntExtra(CouponDetailsActivity.EXTRA_PARAM_ENTERING_TPYE,0) ==4
        )
        ){
//            super.onDismiss(dialog);
//            Toast.makeText(getActivity(),"after dissmiss",Toast.LENGTH_SHORT).show();
            getActivity().finish();
            MainActivity.bottomNavigation.setCurrentItem(0);
            App.vp_message_main.setCurrentItem(0);
        }*//*else if (Integer.parseInt(CouponDetailsActivity.EXTRA_PARAM_COUPON_TYPE)==2){
//            super.onDismiss(dialog);
        }*/



    }

}

