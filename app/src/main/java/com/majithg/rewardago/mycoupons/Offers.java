package com.majithg.rewardago.mycoupons;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.majithg.rewardago.dashboard.Person;
import com.majithg.rewardago.favourite.FavListAdapter;
import com.majithg.rewardago.home.Home;
import com.majithg.rewardago.home.content.CouponDetailsActivity;
import com.majithg.rewardago.home.content.Place;
import com.majithg.rewardago.util.AlertDialogFragment;
import com.majithg.rewardago.util.App;
import com.majithg.rewardago.util.FragmentLifecycle;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * ZLB
 */
public class Offers extends Fragment implements FragmentLifecycle {
    private static final String ARG_PARAM1 = "param1";
    private static final int limit = 50;
    private static final String TAG = Offers.class.getSimpleName();

    private int messageType;

    private String jsonData;

    private String s1;
    private String s2;
    private String s3;

    private ProgressBar progressBar;
    private SwipeRefreshLayout mSwipeRefreshWidget;

    private RecyclerView recyclerViewoffers;
    private TextView emptyViewoffers;

    private FragmentActivity myContext;
    private List<Place> mCurrentWeather = new ArrayList<>();
    private StaggeredGridLayoutManager mStaggeredLayoutManager;
    private ActListAdapter actListAdapter;

//    private List<Data_Offers> mCurrentWeather = new ArrayList<>();

//    private Recycler_View_Adapter_Offers adapter_offers;

    public static boolean canAddItem = false;

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }
    
    public Offers() {
    }

    public static Offers newInstance(int param1) {
        Offers fragment = new Offers();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            messageType = getArguments().getInt(ARG_PARAM1);
        } else {
            messageType = 0;
        }

        setHasOptionsMenu(true);

        getForecast();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.offers, container, false);

        progressBar = (ProgressBar) root.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);

        mSwipeRefreshWidget = (SwipeRefreshLayout) root.findViewById(R.id.swipe_refresh_widget_off);
        mSwipeRefreshWidget.setColorScheme(R.color.colorPrimaryDark, R.color.colorPrimaryDark, R.color.colorPrimaryDark, R.color.colorPrimaryDark);


        recyclerViewoffers = (RecyclerView) root.findViewById(R.id.recyclerviewoffer);
        emptyViewoffers = (TextView) root.findViewById(R.id.empty_view_offer);

        mSwipeRefreshWidget.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshWidget.setRefreshing(true);
                getForecast();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {


                        mSwipeRefreshWidget.post(new Runnable() {
                            @Override
                            public void run() {
//                                mSwipeRefreshLayout.setRefreshing(true);
                                mSwipeRefreshWidget.setRefreshing(false);
                            }
                        });


                    }
                }, 2000);
            }
        });



        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                mSwipeRefreshWidget.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
            }
        }, 5000);


/*

// it's working when the fragment is gone backstack... i mean this view pager (3fragms) until alive
        getFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                Toast.makeText(getActivity(), "onBackStackChanged()" + TAG, Toast.LENGTH_SHORT).show();
//                if(getFragmentManager().getBackStackEntryCount()==1) {
////                    onResume();
//                    Toast.makeText(getActivity(), "onBackStackChanged()" + TAG, Toast.LENGTH_SHORT).show();
//                }
            }
        });

*/


        return root;
    }




    private void getForecast() {

        String forecastUrl = "http://rewardago.com/cron/my_coupons.php?id="
                +MainActivity.UID+"?_=" + System.currentTimeMillis();

        if (isNetworkAvailable()){

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    myContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            toggleRefresh();
                            progressBar.setVisibility(View.GONE);
                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    });

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {
                    /*runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toggleRefresh();
                        }
                    });*/
                    try {
//                        String jsonData = response.body().string();
                        jsonData = stripHtml(response.body().string());

                        /*myContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(myContext, jsonData,Toast.LENGTH_LONG).show();
                            }
                        });*/

//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {
                            mCurrentWeather = getCurrentDetails(jsonData);
                            myContext.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
//                                    updateDisplay();
                                    progressBar.setVisibility(View.GONE);
                                    mSwipeRefreshWidget.setRefreshing(false);
                                }
                            });

                        } else {
                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        }

        else {
            Toast.makeText(myContext, getString(R.string.network_unavailable_message),Toast.LENGTH_LONG).show();
//            mSwipeRefreshWidget.setRefreshing(false);   // i think.. an error may occur here, if we put
//            progressBar.setVisibility(View.GONE);
        }

    }


    private List<Place> getCurrentDetails(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);

        JSONArray currently  = forecast.getJSONArray("MyCoupons");

        List<Place> blogCoupons = new ArrayList<>();

        for (int i = 0; i < currently.length(); i++) {

            Place currentWeather = new Place();

            JSONObject post = currently.getJSONObject(i);

//            String title = post.getString("id");
            currentWeather.setId(post.getInt("offer_id"));
            currentWeather.setName(post.getString("offer_name"));
            currentWeather.setName2(post.getString("Subtitle"));
            currentWeather.setImageName(post.getString("image"));
            currentWeather.setImageName2(post.getString("store_logo"));

            currentWeather.setRedeem_status(post.getString("Redeem_status"));

            blogCoupons.add(currentWeather);
        }


//        Log.d(TAG, currentWeather.getFormattedTime());


        myContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //TODO plz enable later
                if (mCurrentWeather.isEmpty()) {
                    recyclerViewoffers.setVisibility(View.GONE);
                    emptyViewoffers.setVisibility(View.VISIBLE);
                }
                else {
                    recyclerViewoffers.setVisibility(View.VISIBLE);
                    emptyViewoffers.setVisibility(View.GONE);
                }


                mStaggeredLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
                recyclerViewoffers.setLayoutManager(mStaggeredLayoutManager);
                recyclerViewoffers.setHasFixedSize(true);
                actListAdapter = new ActListAdapter(getActivity(), mCurrentWeather);
                recyclerViewoffers.setAdapter(actListAdapter);
//                mAdapter.setOnItemClickListener(onItemClickListener);

                actListAdapter.setOnItemClickListener(new ActListAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent transitionIntent = new Intent(view.getContext(), CouponDetailsActivity.class);
//                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_POSITION, position);
//                        Toast.makeText(myContext, "PP "+mCurrentWeather.get(position).getId(),Toast.LENGTH_LONG).show();
                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_ENTERING_TPYE, 4);
                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_ID, mCurrentWeather.get(position).getId());
//                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_TITLE, mCurrentWeather.get(position).getName());
                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_REDEEM_STATUS, mCurrentWeather.get(position).getRedeem_status());
//                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_SUBTITLE, mCurrentWeather.get(position).getName2());
//                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_IMAGE, mCurrentWeather.get(position).getImageName());
                        myContext.startActivity(transitionIntent);
                    }
                });



                progressBar.setVisibility(View.GONE);
                mSwipeRefreshWidget.setRefreshing(false);

            }

        });


        return blogCoupons;
    }


    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.fragment_menu, menu);

        if(canAddItem) {
            MainActivity.toolbar.getMenu().clear();
            MainActivity.toolbar.getMenu().add(0, 5, Menu.NONE, null).setIcon(R.drawable.ic_arrow_24dp).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        }else {
            MainActivity.toolbar.getMenu().clear();
            MainActivity.toolbar.getMenu().add(0, 10, Menu.NONE, null).setIcon(R.drawable.ic_arrow_24dp).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//        App.menu_id = item.getItemId();

        if (id == 5) {

            MainActivity.toolbar.getMenu().clear();
            Offers.canAddItem = false;
            MainActivity.bottomNavigation.setCurrentItem(4);
            App.vp_message_main.setCurrentItem(4);
            return true;

        } else if(id == 10){
            MainActivity.toolbar.getMenu().clear();
//            Offers.canAddItem = false;
            MainActivity.bottomNavigation.setCurrentItem(0);
            App.vp_message_main.setCurrentItem(0);
            return true;
        }



        return super.onOptionsItemSelected(item);
    }





    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
//        setMainRecyclerView();
//        setCallLogs(curLog);

//        Toast.makeText(getActivity(), "onResume(): " + TAG, Toast.LENGTH_SHORT).show();

        getForecast();
    }


    @Override
    public void onPauseFragment() {
        Log.i(TAG, "onPauseFragment()");
        // TODO very important
//        Toast.makeText(getActivity(), "onPauseFragment(): " + TAG, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResumeFragment() {
        Log.i(TAG, "onResumeFragment()");
        // TODO very important
//        Toast.makeText(getActivity(), "onResumeFragment(): " + TAG, Toast.LENGTH_SHORT).show();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)
                myContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if(networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;

        }

        return isAvailable;
    }

    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(myContext.getFragmentManager(), "error_dialog");
    }

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }


}
