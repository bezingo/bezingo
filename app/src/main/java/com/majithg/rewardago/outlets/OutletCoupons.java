package com.majithg.rewardago.outlets;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.majithg.rewardago.home.content.CouponDetailsActivity;
import com.majithg.rewardago.home.content.Place;
import com.majithg.rewardago.home.content.TravelListAdapter;
import com.majithg.rewardago.util.AlertDialogFragment;
import com.majithg.rewardago.util.App;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class OutletCoupons extends AppCompatActivity {

    private ProgressBar progressBar;
    private SwipeRefreshLayout mSwipeRefreshWidget;
    private RecyclerView mRecyclerView;

    public final static String EXTRA_PARAM_STORE_ID = "store_id";
    public final static String EXTRA_PARAM_STORE_NAME = "store_name";

    private String jsonData;
    private StaggeredGridLayoutManager mStaggeredLayoutManager;

    private TravelListAdapter mAdapter;
    private List<Place> mCurrentWeather = new ArrayList<>();
    private Toolbar toolbar;
    private String sId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outlet_coupons);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_24dp);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);

        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText(getIntent().getStringExtra(EXTRA_PARAM_STORE_NAME));

//     int sID =  getIntent().getIntExtra(EXTRA_PARAM_STORE_ID,0);
        sId = getIntent().getStringExtra(EXTRA_PARAM_STORE_ID);
//        Toast.makeText(OutletCoupons.this, sId,Toast.LENGTH_SHORT).show();


        progressBar = (ProgressBar) findViewById(R.id.progress_bar_oc);
        progressBar.setVisibility(View.VISIBLE);

        mSwipeRefreshWidget = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_widget_oc);
        mSwipeRefreshWidget.setColorScheme(R.color.colorPrimaryDark, R.color.colorPrimaryDark, R.color.colorPrimaryDark, R.color.colorPrimaryDark);

        mRecyclerView = (RecyclerView) findViewById(R.id.list_oc);

        mSwipeRefreshWidget.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getForecast();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mSwipeRefreshWidget.setRefreshing(false);
                        progressBar.setVisibility(View.GONE);
                    }
                }, 2000);

            }
        });



//        isListView = true;

//        MainActivity.toolbar.getMenu().clear();



        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                mSwipeRefreshWidget.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
            }
        }, 5000);


        getForecast();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getForecast();
    }

    private void getForecast() {

        String forecastUrl = "http://rewardago.com/cron/outlets.php?id="
                +sId
                ;

        if (isNetworkAvailable()){

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    });

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
                      runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//                                    updateDisplay();
                                progressBar.setVisibility(View.GONE);
                                mSwipeRefreshWidget.setRefreshing(false);
                            }
                        });
                        jsonData = response.body().string();
//                        jsonData = stripHtml(response.body().string());

                       /* runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(OutletCoupons.this, jsonData,Toast.LENGTH_SHORT).show();
                            }
                        });*/
                        if (response.isSuccessful()) {
                            mCurrentWeather = getCurrentDetails(jsonData);
                        } else {
                            alertUserAboutError();
                        }
                    }
                    catch (IOException e) {
//                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
//                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }



                }
            });
        }

        else {
            Toast.makeText(this, getString(R.string.network_unavailable_message),Toast.LENGTH_LONG).show();
//            mSwipeRefreshWidget.setRefreshing(false);   // i think.. an error may occur here, if we put
//            progressBar.setVisibility(View.GONE);
//            stopRefreshing(); don't use anything here ...
        }

    }

    private List<Place> getCurrentDetails(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);


//        String timezone = forecast.getString("timezone");
//        Log.i(TAG, getString(R.string.debug_from_JSON) + timezone);

//        JSONObject currently = forecast.getJSONObject("currently");
        JSONArray currently  = forecast.getJSONArray("Deals");
//        JSONArray currently  = forecast.getJSONArray("json");


//        ArrayList<HashMap<String, String>> blogCoupons = new ArrayList<HashMap<String, String>>();
        List<Place> blogCoupons = new ArrayList<>();

        for (int i = 0; i < currently.length(); i++) {
//            for (int i = 0; i < 5; i++) {
            Place currentWeather = new Place();

            JSONObject post = currently.getJSONObject(i);

//            String title = post.getString("id");
            currentWeather.setId(post.getInt("id"));
            currentWeather.setName(post.getString("title"));
            currentWeather.setName2(post.getString("Sub_Title"));
//            currentWeather.setImageResourceId(post.getString("image"));
            currentWeather.setImageName(post.getString("image"));
            currentWeather.setImageName2(post.getString("Store_logo"));



           /* title = Html.fromHtml(title).toString();
            String author = post.getString(KEY_AUTHOR);
            author = Html.fromHtml(author).toString();

            HashMap<String, String> blogPost = new HashMap<String, String>();
            blogPost.put(KEY_TITLE, title);
            blogPost.put(KEY_AUTHOR, author);*/


//            blogPosts.add(blogPost);
            blogCoupons.add(currentWeather);
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                mStaggeredLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
                mRecyclerView.setLayoutManager(mStaggeredLayoutManager);
                mRecyclerView.setHasFixedSize(true);
                mAdapter = new TravelListAdapter(getApplicationContext(), mCurrentWeather);
                mRecyclerView.setAdapter(mAdapter);
//                mAdapter.setOnItemClickListener(onItemClickListener);

                mAdapter.setOnItemClickListener(new TravelListAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent transitionIntent = new Intent(view.getContext(), CouponDetailsActivity.class);
//                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_POSITION, position);
//                        Toast.makeText(myContext, "PP "+mCurrentWeather.get(position).getId(),Toast.LENGTH_LONG).show();
                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_ENTERING_TPYE, 1);
                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_ID, mCurrentWeather.get(position).getId());
//                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_TITLE, mCurrentWeather.get(position).getName());
//                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_SUBTITLE, mCurrentWeather.get(position).getName2());
//                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_IMAGE, mCurrentWeather.get(position).getImageName());
                        startActivity(transitionIntent);
                    }
                });



                progressBar.setVisibility(View.GONE);
                mSwipeRefreshWidget.setRefreshing(false);

            }

        });


//        return currentWeather;
        return blogCoupons;
    }


   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
                onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if(networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;

        }

        return isAvailable;
    }

    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(getFragmentManager(), "error_dialog");
    }

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString().replaceAll("\n", "").trim(); // it's not working properly..
    }
}
