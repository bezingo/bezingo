package com.majithg.rewardago.outlets;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.majithg.rewardago.home.Home;
import com.majithg.rewardago.home.content.CouponDetailsActivity;
import com.majithg.rewardago.home.content.Place;
import com.majithg.rewardago.util.AlertDialogFragment;
import com.majithg.rewardago.util.App;
import com.majithg.rewardago.util.FragmentLifecycle;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * ZLB
 */
public class outlets extends Fragment implements FragmentLifecycle
//        ,Updateable
{

    public static final String TAG = outlets.class.getSimpleName();
    Context applicationContext = MainActivity.getContextOfApplication();

    private static final String ARG_PARAM1 = "param1";
    private static final int limit = 50;

    private int messageType;

    private RecyclerView mRecyclerView;
    private StaggeredGridLayoutManager mStaggeredLayoutManager;

//    private boolean isListView;

    private List<Place> mCurrentWeather = new ArrayList<>();
//    Place currentWeather;


    private ProgressBar progressBar;
private SwipeRefreshLayout mSwipeRefreshWidget;

    public static FragmentActivity myContext;

//    MainViewPagerAdapter mmadapter;
//    private Calendar startTime = Calendar.getInstance();
    private OutListAdapter mAdapter;

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
//        Toast.makeText(getActivity(),"onAttach()", Toast.LENGTH_SHORT).show();

    }


    public outlets() {
    }

    public static outlets newInstance(int param1) {
        outlets fragment = new outlets();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.v(TAG, "In frag's on save instance state ");
//        outState.putSerializable("starttime", startTime);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Toast.makeText(getActivity(),"onCreate()", Toast.LENGTH_SHORT).show();

        if (getArguments() != null) {
            messageType = getArguments().getInt(ARG_PARAM1);
        } else {
            messageType = 0;
        }

        setHasOptionsMenu(true);

       getForecast();


    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

//        Toast.makeText(getActivity(),"onCreateView()", Toast.LENGTH_SHORT).show();

        View root = inflater.inflate(R.layout.outlets, container, false);

        progressBar = (ProgressBar) root.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);

        mSwipeRefreshWidget = (SwipeRefreshLayout) root.findViewById(R.id.swipe_refresh_widget);
        mSwipeRefreshWidget.setColorScheme(R.color.colorPrimaryDark, R.color.colorPrimaryDark, R.color.colorPrimaryDark, R.color.colorPrimaryDark);


        mRecyclerView = (RecyclerView) root.findViewById(R.id.outletlist);



        mSwipeRefreshWidget.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getForecast();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mSwipeRefreshWidget.setRefreshing(false);
                    }
                }, 2000);

            }
        });

//        MainActivity.toolbar.getMenu().clear();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                mSwipeRefreshWidget.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
            }
        }, 5000);


        return root;
    }




   /* public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_menu, menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.store) {
//            bottomNavigation.setCurrentItem(0);
//            vp_message.setCurrentItem(0);
//            getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
            return true;
        }*/
        if (id == 5) {
//            bottomNavigation.setCurrentItem(0);
//            vp_message.setCurrentItem(0);

            App.HomeCanAddItem = false;

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    reemove();
                }
            }, 50);

            return true;
        }
       /* if (id == R.id.back_arrow) {
//            bottomNavigation.setCurrentItem(0);
//            vp_message.setCurrentItem(0);
            MainActivity.toolbar.getMenu().clear();
            MainActivity.bottomNavigation.setCurrentItem(0);
            App.vp_message_main.setCurrentItem(0);

            Home.canAddItem = false;

            return true;
        }*/
        /*else if (id == android.R.id.home) {
//            onBackPressed();
            sliderLayout.toggleMenu();
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    private void reemove(){
//      int dd = getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit(); // also working
//        Toast.makeText(myContext, ""+dd,Toast.LENGTH_SHORT).show(); // also working

        FragmentManager manager = myContext.getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.remove(this);
        transaction.commit();
    }



    private void getForecast() {

        String forecastUrl = "http://rewardago.com/cron/outlets.php";

        if (isNetworkAvailable()){

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    myContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            toggleRefresh();
                            progressBar.setVisibility(View.GONE);
                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    });

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
                        String jsonData = response.body().string();
                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {
                            mCurrentWeather = getCurrentDetails(jsonData);


                        } else {
                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        }

        else {
            Toast.makeText(myContext, getString(R.string.network_unavailable_message),Toast.LENGTH_LONG).show();
//            mSwipeRefreshWidget.setRefreshing(false);
//            progressBar.setVisibility(View.GONE);
        }

    }



    private List<Place> getCurrentDetails(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);

        JSONArray currently  = forecast.getJSONArray("Store");

        List<Place> blogCoupons = new ArrayList<>();

        for (int i = 0; i < currently.length(); i++) {

            Place currentWeather = new Place();

            JSONObject post = currently.getJSONObject(i);

            currentWeather.setId(post.getInt("id"));
            currentWeather.setName(post.getString("name"));
            currentWeather.setImageName(post.getString("image"));

            blogCoupons.add(currentWeather);
        }



        myContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                mStaggeredLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
                mRecyclerView.setLayoutManager(mStaggeredLayoutManager);
                mRecyclerView.setHasFixedSize(true);
                mAdapter = new OutListAdapter(getActivity(), mCurrentWeather);
                mRecyclerView.setAdapter(mAdapter);
//                mAdapter.setOnItemClickListener(onItemClickListener);
                mAdapter.setOnItemClickListener(new OutListAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
//                       String s = String.format(mCurrentWeather.get(position).getId()+"") ;
//                        Toast.makeText(myContext, ""+mCurrentWeather.get(position).getId(),Toast.LENGTH_SHORT).show();
                        Intent transitionIntent = new Intent(view.getContext(), OutletCoupons.class);
//                       transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_ENTERING_TPYE, 1);
                        transitionIntent.putExtra(OutletCoupons.EXTRA_PARAM_STORE_ID, ""+mCurrentWeather.get(position).getId() );
                        transitionIntent.putExtra(OutletCoupons.EXTRA_PARAM_STORE_NAME, mCurrentWeather.get(position).getName());
//                        transitionIntent.putExtra(OutletCoupons.EXTRA_PARAM_STORE_ID, 547);
                 myContext.startActivity(transitionIntent);
//                        reemove();
                    }
                });

                progressBar.setVisibility(View.GONE);
                mSwipeRefreshWidget.setRefreshing(false);

            }
        });

        return blogCoupons;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)
                myContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if(networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;

        }

        return isAvailable;
    }

    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(myContext.getFragmentManager(), "error_dialog");
    }

    @Override
    public void onResume() {
        super.onResume();
//        Toast.makeText(getActivity(),"onResume()", Toast.LENGTH_SHORT).show();
        getForecast();
    }

    @Override
    public void onPauseFragment() {
        Log.i(TAG, "onPauseFragment()");
//        Toast.makeText(getActivity(), "onPauseFragment():" + TAG, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onResumeFragment() {
        Log.i(TAG, "onResumeFragment()");
//        Toast.makeText(getActivity(), "onResumeFragment():" + TAG, Toast.LENGTH_SHORT).show();


    }

   /* @Override
    public void update() {
        Toast.makeText(getActivity(), "update", Toast.LENGTH_SHORT).show();
    }*/

}
