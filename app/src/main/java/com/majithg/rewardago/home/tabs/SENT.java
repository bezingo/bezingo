package com.majithg.rewardago.home.tabs;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.majithg.rewardago.R;
import com.majithg.rewardago.home.content.CouponDetailsActivity;
import com.majithg.rewardago.util.AlertDialogFragment;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * ZLB
 */
public class SENT extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final int limit = 50;

    private int messageType;

    private static final String TAG = SENT.class.getSimpleName();

    private String jsonData;
    private FragmentActivity myContext;


    private String currentWeather1;
    private String currentWeather2;
    private String currentWeather3;
    private String currentWeather4;
    private String currentWeather5;
    private String currentWeather6;
    private String currentWeather7;


    TextView terms1;
    TextView terms2;
    TextView terms3;
    TextView terms4;
    TextView terms5;
    TextView terms6;
    TextView terms7;

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }

    public SENT() {
    }

    public static SENT newInstance(int param1) {
        SENT fragment = new SENT();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            messageType = getArguments().getInt(ARG_PARAM1);
        } else {
            messageType = 0;
        }
        getForecast();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.sent, container, false);
        terms1 = (TextView) root.findViewById(R.id.terms1);
        terms2 = (TextView) root.findViewById(R.id.terms2);
        terms3 = (TextView) root.findViewById(R.id.terms3);
        terms4 = (TextView) root.findViewById(R.id.terms4);
        terms5 = (TextView) root.findViewById(R.id.terms5);
        terms6 = (TextView) root.findViewById(R.id.terms6);
        terms7 = (TextView) root.findViewById(R.id.terms7);

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
//        setMainRecyclerView();
//        setCallLogs(curLog);
        getForecast();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

     @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    private void getForecast() {

        String forecastUrl = "http://rewardago.com/cron/terms.php?id="+ CouponDetailsActivity.cid;

        if (isNetworkAvailable()){

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
//                         jsonData = response.body().string();
                        jsonData = stripHtml(response.body().string());




                        /*myContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(myContext, jsonData, Toast.LENGTH_LONG).show();
                            }
                        });*/


                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {

                           getCurrentDetails(jsonData);





                        } else {
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        }

        else {
            Toast.makeText(myContext, getString(R.string.network_unavailable_message), Toast.LENGTH_LONG).show();

        }

    }

    private void getCurrentDetails(String jsonData) throws JSONException {
         JSONObject forecast = new JSONObject(jsonData);

        JSONObject currently = forecast.getJSONObject("Terms");


        currentWeather1 = currently.getString("terms1");
        currentWeather2 = currently.getString("terms2");
        currentWeather3 = currently.getString("terms3");
        currentWeather4 = currently.getString("terms4");
        currentWeather5 = currently.getString("terms5");
        currentWeather6 = currently.getString("terms6");
        currentWeather7 = currently.getString("terms7");


        myContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                terms1.setText(currentWeather1);
                terms2.setText(currentWeather2);
                terms3.setText(currentWeather3);
                terms4.setText(currentWeather4);
                terms5.setText(currentWeather5);
                terms6.setText(currentWeather6);
                terms7.setText(currentWeather7);

            }

        });


    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)
                myContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if(networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;

        }

        return isAvailable;
    }

    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(myContext.getFragmentManager(), "error_dialog");
    }

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }
}
