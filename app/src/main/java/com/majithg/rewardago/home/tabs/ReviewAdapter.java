package com.majithg.rewardago.home.tabs;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.majithg.rewardago.R;
import com.majithg.rewardago.util.CircleImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by megha on 15-03-06.
 */
public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder> {

  Context mContext ;
//  OnItemClickListener mItemClickListener;
  List<ReviewData> mlist;


  int clr;

  public ReviewAdapter(Context context, List<ReviewData> list) {
    this.mContext = context;
    this.mlist = list;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_review, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder, int position) {
//    final Place place = new PlaceData().placeList().get(position);


//      Place currentWeather = new Place();
      ReviewData place = mlist.get(position);

      Picasso.with(mContext).load(place.getReviewerImage())
              .error(R.drawable.placeholder2)
              .placeholder(R.drawable.placeholder2)
              .into(holder.reviewerImage);
      holder.reviewerName.setText(place.getReviewerName());
      holder.reviewerComment.setText(place.getReviewerComment());
      holder.reviewerRate.setRating(Integer.valueOf(place.getReviewerRate()));
//      holder.placeNameDesc.setText(place.name); // to remove second subtitle
      //Download image using picasso library
     /* Picasso.with(mContext).load(place.getImageName())
              .error(R.drawable.placeholder2)
              .placeholder(R.drawable.placeholder2)
              .into(holder.placeImage);*/
//      holder.placeImage.setImageResource(place.imageResourceId);



  }
  @Override
  public int getItemCount() {
    return (null != mlist ? mlist.size() : 0);
  }

/*  @Override
  public int getItemCount() {
    return new PlaceData().placeList().size();
  }*/

  public class ViewHolder extends RecyclerView.ViewHolder
//          implements View.OnClickListener
  {
    public CircleImageView reviewerImage;
    public TextView reviewerName;
    public TextView reviewerComment;
      public RatingBar reviewerRate;

    public ViewHolder(View itemView) {
      super(itemView);
        reviewerImage = (CircleImageView) itemView.findViewById(R.id.reviewerImage);
        reviewerName = (TextView) itemView.findViewById(R.id.reviewerName);
        reviewerComment = (TextView) itemView.findViewById(R.id.reviewerComment);
        reviewerRate = (RatingBar) itemView.findViewById(R.id.reviewerRate);
//      placeHolder.setOnClickListener(this);
    }

    /*@Override
    public void onClick(View v) {
      if (mItemClickListener != null) {
        mItemClickListener.onItemClick(itemView, getPosition());
      }
    }*/
  }

  public interface OnItemClickListener {
    void onItemClick(View view, int position);
  }

/*  public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
    this.mItemClickListener = mItemClickListener;
  }*/



}
