package com.majithg.rewardago.home.tabs;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.majithg.rewardago.R;
import com.majithg.rewardago.home.content.CouponDetailsActivity;
import com.majithg.rewardago.util.AlertDialogFragment;
import com.majithg.rewardago.util.App;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * ZLB
 */
public class CVS extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final int limit = 50;
    private static final String TAG = CVS.class.getSimpleName();

    TextView mTilte,details, num, addr, days;

    private int messageType;
    private FragmentActivity myContext;


    public CVS() {

    }


    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }


    public static CVS newInstance(int param1) {
        CVS fragment = new CVS();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setRetainInstance(true);
        if (getArguments() != null) {
            messageType = getArguments().getInt(ARG_PARAM1);
        } else {
            messageType = 0;
        }

//        getForecast();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.cvs, container, false);


        mTilte = (TextView) root.findViewById(R.id.textView);
        details = (TextView) root.findViewById(R.id.detail);
        num = (TextView) root.findViewById(R.id.num);
        addr = (TextView) root.findViewById(R.id.addr);
        days = (TextView) root.findViewById(R.id.days);

       /* mTilte.setText(App.EXTRA_PARAM_TITLE);
        details.setText(App.EXTRA_PARAM_SUBTITLE);
        num.setText(App.EXTRA_PARAM_STORE_CONTACT);
        addr.setText(App.EXTRA_PARAM_STORE_ADDRESS);
        days.setText(App.EXTRA_PARAM_COUPON_EXPIRY);*/

        mTilte.setText(CouponDetailsActivity.EXTRA_PARAM_TITLE);
        details.setText(CouponDetailsActivity.EXTRA_PARAM_SUBTITLE);
        num.setText(CouponDetailsActivity.EXTRA_PARAM_STORE_CONTACT);
        addr.setText(CouponDetailsActivity.EXTRA_PARAM_STORE_ADDRESS);
        days.setText(CouponDetailsActivity.EXTRA_PARAM_COUPON_EXPIRY);



        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
//        mTilte.setText(CouponDetailsActivity.str1);
    }

    @Override
    public void onResume() {
        super.onResume();
//        setMainRecyclerView();
//        setCallLogs(curLog);
//        getForecast();
    }



   /* private void getForecast() {

        String forecastUrl = "http://rewardago.com/cron/json.php?id="+CouponDetailsActivity.cid;

        if (isNetworkAvailable()){

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    myContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            toggleRefresh();
//                            progressBar.setVisibility(View.GONE);
//                            mSwipeRefreshWidget.setRefreshing(false);

                        }
                    });

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
//                         jsonData = response.body().string();
                        jsonData = stripHtml(response.body().string());




                        myContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//                                Toast.makeText(myContext, jsonData,Toast.LENGTH_LONG).show();
                            }
                        });


                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {

                            mCurrentWeather = getCurrentDetails(jsonData);





                        } else {
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        }

        else {
            Toast.makeText(myContext, getString(R.string.network_unavailable_message), Toast.LENGTH_LONG).show();

        }

    }

    private String getCurrentDetails(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);


//        String timezone = forecast.getString("timezone");
//        Log.i(TAG, getString(R.string.debug_from_JSON) + timezone);

//        JSONObject currently = forecast.getJSONObject("Offer");
//        JSONArray currently  = forecast.getJSONArray("History");
//        JSONArray currently  = forecast.getJSONArray("json");


//        ArrayList<HashMap<String, String>> blogCoupons = new ArrayList<HashMap<String, String>>();
//        List<Hdata> blogCoupons = new ArrayList<>();

//        for (int i = 0; i < currently.length(); i++) {
//
//            Hdata currentWeather = new Hdata();

//            JSONObject post = currently.getJSONObject(i);

//            currentWeather.setId(post.getInt("offer_id"));
//            currentWeather.setOutletName(post.getString("store_name"));
//            currentWeather.setHistoryTitle(post.getString("title"));
//            currentWeather.setClaimedDate(post.getString("claimed_date"));
//            currentWeather.setRedeempedDate(post.getString("redeemed_date"));
//
        currentWeather = forecast.getString("Type");
        currentWeather2 = forecast.getString("contact");
//        currentWeather3 = forecast.getString("Email");
        currentWeather4 = forecast.getInt("Expire");
        currentWeather5 = forecast.getString("Sub_Title");

//            blogCoupons.add(currentWeather);
//        }



        myContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                                    updateDisplay();

               *//* new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms


//                        progressBar.setVisibility(View.GONE);
//                        mSwipeRefreshWidget.setRefreshing(false);

                    }
                }, 3000);*//*



//                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//                h_adapter = new HRVA(mCurrentWeather, getActivity());
//                recyclerView.setAdapter(App.h_adapter);
//                recyclerView.setAdapter(h_adapter);
//                recyclerView.addItemDecoration(new MyItemDecoration());

                details.setText(currentWeather5);
                num.setText(currentWeather2);
                addr.setText(""+currentWeather3);
                days.setText(""+currentWeather4);

//                App.adapter_offers.notifyDataSetChanged();

            }

        });

        return currentWeather;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)
                myContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if(networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;

        }

        return isAvailable;
    }

    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(myContext.getFragmentManager(), "error_dialog");
    }

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }*/
}
