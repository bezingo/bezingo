package com.majithg.rewardago.home.content;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.majithg.rewardago.dailog.DialogCliam;
import com.majithg.rewardago.dailog.DialogShare;
import com.majithg.rewardago.dashboard.Person;
import com.majithg.rewardago.dashboard.redemption.MyRedemption;
import com.majithg.rewardago.home.tabs.CVS;
import com.majithg.rewardago.home.tabs.CustomViewPager2;
import com.majithg.rewardago.home.tabs.SENT;
import com.majithg.rewardago.home.tabs.ViewPagerAdapter2;
import com.majithg.rewardago.home.tabs.XML;
import com.majithg.rewardago.mycoupons.Offers;
import com.majithg.rewardago.util.AlertDialogFragment;
import com.majithg.rewardago.util.App;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CouponDetailsActivity extends AppCompatActivity {

    public final static String EXTRA_PARAM_ID = "coupon_id";
    public final static String EXTRA_PARAM_ENTERING_TPYE = "entering_tpye";

//    public  static String EXTRA_PARAM_COUPON_TYPE;
//    public  static String EXTRA_PARAM_COUPON_QR_CODE;

//    public  static String EXTRA_PARAM_COUPON_CLAIMED;
//    public  static String EXTRA_PARAM_COUPON_REDEEMED;
//    public  static String EXTRA_PARAM_COUPON_EXPIRY ;

//    public  static String EXTRA_PARAM_TITLE;
//    public  static String EXTRA_PARAM_SUBTITLE;
//    public  static String EXTRA_PARAM_IMAGE;
//    public  static String EXTRA_PARAM_STORE_LOGO;
//    public  static String EXTRA_PARAM_STORE_CONTACT;
//    public  static String EXTRA_PARAM_STORE_ADDRESS;


    public static final String TAG = CouponDetailsActivity.class.getCanonicalName();
    public static final String EXTRA_PARAM_POSITION = "position";
    public static final String EXTRA_PARAM_REDEEM_STATUS = "Redeem_status";
    public static final String EXTRA_PARAM_QR_CODE = "qr_code";



    public  static String EXTRA_PARAM_COUPON_TYPE;
    public  static String EXTRA_PARAM_COUPON_QR_CODE;
    public  static boolean EXTRA_PARAM_COUPON_HIDE_QR_CODE = false;
    //    public  static String EXTRA_PARAM_COUPON_CLAIMED;
//    public  static String EXTRA_PARAM_COUPON_REDEEMED;
    public  static String EXTRA_PARAM_COUPON_EXPIRY ;
    public  static String EXTRA_PARAM_TITLE;
    public  static String EXTRA_PARAM_SUBTITLE;
    public  static String EXTRA_PARAM_IMAGE;
    public  static String EXTRA_PARAM_STORE_LOGO;
    public  static String EXTRA_PARAM_STORE_CONTACT;
    public  static String EXTRA_PARAM_STORE_ADDRESS;
    public static String EXTRA_PARAM_COUPON_URL;

    //    private ImageButton mAddButton;
    private Place mPlace;
//    private ArrayList<String> mTodoList;
    int defaultColorForRipple;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private List<Fragment> list_fragment;
    private CustomViewPager2 vp_message2;
    private ViewPagerAdapter2 fAdapter2;
    LinearLayout tab1, tab2, tab3;
//    Button claim, share;
    DialogCliam dialogCliam;
    DialogShare dialogShare;
//    public Recycler_View_Adapter2 adapter;
    public int messageType;

    private ImageView mImageView;
//    private TextView mTitle;
//    private TextView mTitle2;
//    private LinearLayout mTitleHolder;
//    private InputMethodManager mInputManager;
//    private ArrayAdapter mToDoAdapter;
    private int isEditTextVisible;
    private ImageButton mAddButton;

    public static int cid;

    public static String str1;
    public static String str2;
    public static String str3;
    private static int posi;
    public static int i = 0;
    private String json_couponDetails;

//    Data2 data2;

    ProgressDialog progressDialog;
    private ImageView mImageView22;


    //
//            String[] fcids = new String[0];
    private List<String> fcids = new ArrayList<>();
    private List<String> cmduids = new ArrayList<>();
    private List<String> rmduids = new ArrayList<>();

    private boolean errorAdding = true;
    private boolean errorRemoving = true;

    private String addedRespone;
    private String removedRespone;
    private String ddd;
    private String json_couponFavouriteDetails;
    private String sss;
    private String ssssss;
    private String claimedDetails;
    private String redeemedDetails;
    private int cType;



//    DialogCliam dialogCliam;
    FragmentManager fragmentManager;
    private View positiveAction;
    private EditText nm;
    private EditText mail;
    private EditText note;
    private RatingBar reviewerRate;

    private boolean bnm = false;
    private boolean bmail = false;
    private boolean bnote = false;
    private String jsonSubs;
    private boolean errorStatus = true;


//    String forecastJsonStr = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        toolbar.setTitle(null);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_24dp);
        getSupportActionBar().setHomeButtonEnabled(true);

//        toolbar.setLogo(R.drawable.ic_stars_24dp);



       /* ImageView imageView2 = (ImageView) findViewById(R.id.bkbt);
        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ImageView imageView = (ImageView) findViewById(R.id.like);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRatingDialog();
            }
        });*/


        fragmentManager = getFragmentManager();
        App.manager = fragmentManager;


        App.appdialogCliam = new DialogCliam();
        App.appdialogCliam.setCancelable(false);


        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        cid = getIntent().getIntExtra(EXTRA_PARAM_ID, 0);

        couponDetails(cid);


        App.claim = (Button) findViewById(R.id.cliam);
        App.share = (Button) findViewById(R.id.share);


        mImageView = (ImageView) findViewById(R.id.placeImage);
        mImageView22 = (ImageView) findViewById(R.id.placeImage22);

//        Toast.makeText(this, "" + MainActivity.UID, Toast.LENGTH_SHORT).show();
//        Toast.makeText(this, "" + cid, Toast.LENGTH_SHORT).show();

        mAddButton = (ImageButton) findViewById(R.id.btn_add);
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgButtonClicked();
            }
        });


        vp_message2 = (CustomViewPager2) findViewById(R.id.vp_message2);

//        layout_tab = (TabLayout) findViewById(R.id.layout_tab);


        CVS f0 = CVS.newInstance(0);
        SENT f1 = SENT.newInstance(1);
        XML f2 = XML.newInstance(2);


        list_fragment = new ArrayList<>();
        list_fragment.add(f0);
        list_fragment.add(f1);
        list_fragment.add(f2);

        fAdapter2 = new ViewPagerAdapter2(getSupportFragmentManager(), list_fragment);
        vp_message2.setAdapter(fAdapter2);


        tab1 = (LinearLayout) findViewById(R.id.tab1);
        tab2 = (LinearLayout) findViewById(R.id.tab2);
        tab3 = (LinearLayout) findViewById(R.id.tab3);

        tab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp_message2.setCurrentItem(0);
            }
        });

        tab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp_message2.setCurrentItem(1);
            }
        });

        tab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp_message2.setCurrentItem(2);
            }
        });

        App.claim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                buttonClaimRedeemRedeemagain();
//                App.getInstance().testing(); // now working
//                inflateDialogClaim(fragmentManager); // it's working

            }
        });

        App.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogShare = new DialogShare();
                dialogShare.show(getFragmentManager(), "dialog");
            }
        });

    }



    private void imgButtonClicked() {


        if(App.claim.getText().toString()=="Claim Coupon") {

            if (isEditTextVisible == 0) {
// TODO add to favourite
                addToFavourite();
            } else if (isEditTextVisible == 1) {
                // TODO remove favourite
                removeFavourite();
            }

        }else if(App.claim.getText().toString()=="") {
            Toast.makeText(this, "please wait", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this, "please select new coupon", Toast.LENGTH_SHORT).show();
            }


    }

    private void buttonClaimRedeemRedeemagain(){



        if(App.claim.getText().toString() == "Claim Coupon"){
            Toast.makeText(this,"please wait",Toast.LENGTH_SHORT).show();
            /*final Toast toast = Toast.makeText(getApplicationContext(), "please wait", Toast.LENGTH_SHORT);
            toast.show();

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    toast.cancel();
                    App.getInstance().claimCoupon(cid);
                }
            }, 500);*/
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    App.getInstance().claimCoupon(cid);
                }
            }, 500);


        }else if(App.claim.getText().toString() == "Show Coupon"){
//            Toast.makeText(this,App.claim.getText().toString(),Toast.LENGTH_SHORT).show();
            App.vp_message_main.setCurrentItem(3);
            MainActivity.bottomNavigation.setCurrentItem(3);
            finish();

        }else if(App.claim.getText().toString() == "Redeem Again" || App.claim.getText().toString() == "Redeem Coupon"){
//            Toast.makeText(this,"Redeem or Redeem Again",Toast.LENGTH_SHORT).show();
            Toast.makeText(this,"Generating your QR code...",Toast.LENGTH_SHORT).show();
            /*final Toast toast = Toast.makeText(getApplicationContext(), "please wait", Toast.LENGTH_SHORT);
            toast.show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    toast.cancel();
//                    App.getInstance().redeemCoupon(cid);
                }
            }, 500);*/
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
//                    toast.cancel();
                    App.getInstance().redeemCoupon(cid);
                }
            }, 500);


        }else if(App.claim.getText().toString() == "Redeemed"){
            Toast.makeText(this,"Redirecting to My Redemption Page",Toast.LENGTH_SHORT).show();
            App.vp_message_main.setCurrentItem(4);
            MainActivity.bottomNavigation.setCurrentItem(4);
            finish();

            MainActivity.toolbar.getMenu().clear();
            App.personCanAddItem = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                   /* MyRedemption fone = MyRedemption.newInstance(20);
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.insideP, fone, "MY_REDEMPTION");
                    transaction.addToBackStack(null);
                    transaction.commit();*/
                    Person.textMyRedemp.performClick();
                }
            }, 2000);


        }else if(App.claim.getText().toString() == "Show QR Code"){
            Toast.makeText(this,"Generating your QR code...",Toast.LENGTH_SHORT).show();
            /*final Toast toast = Toast.makeText(getApplicationContext(), "please wait", Toast.LENGTH_SHORT);
            toast.show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    toast.cancel();
                }
            }, 500);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    App.getInstance().showRedeemedDialog(getIntent().getStringExtra(EXTRA_PARAM_QR_CODE));
                }
            }, 600);*/
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    App.getInstance().showRedeemedDialog(getIntent().getStringExtra(EXTRA_PARAM_QR_CODE));
                }
            }, 500);

        }else{
            Toast.makeText(this,"something went wrong",Toast.LENGTH_SHORT).show();
        }

    }


    private void addToFavourite() {

        progressDialog.setMessage("Adding favourite ...");
        progressDialog.show();

        String forecastUrl = "http://rewardago.com/cron/favourite.php?id=" + MainActivity.UID + "&oid=" + cid;

        if (isNetworkAvailable()) {

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            toggleRefresh();
//                            progressBar.setVisibility(View.GONE);
//                            mSwipeRefreshWidget.setRefreshing(false);
                            progressDialog.dismiss();
                        }
                    });

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
                        addedRespone = response.body().string();
//                        addedRespone = stripHtml(response.body().string());


                       /* runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(CouponDetailsActivity.this, addedRespone, Toast.LENGTH_LONG).show();
                            }
                        });*/


//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {

                            addingFavourite(addedRespone).dismiss();

                        } else {
                            progressDialog.dismiss();
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
//                    } catch (IOException e) {
//                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    } catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        } else {
            progressDialog.dismiss();
            Toast.makeText(this, getString(R.string.network_unavailable_message), Toast.LENGTH_LONG).show();

        }

    }

    private ProgressDialog addingFavourite(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);
        JSONArray jsonArray = forecast.getJSONArray("User");
        JSONObject jsonObject = jsonArray.getJSONObject(0);
        errorAdding = jsonObject.getBoolean("error");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!errorAdding) {
                    errorAdding = true;
                    isEditTextVisible = 1;
                    mAddButton.setImageResource(R.drawable.ic_favorite_red_24dp);
                    final Toast toast = Toast.makeText(getApplicationContext(), "Bookmarked", Toast.LENGTH_SHORT);
                    toast.show();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            toast.cancel();
                        }
                    }, 500);

                } else {
                    Toast.makeText(CouponDetailsActivity.this, "something went wrong", Toast.LENGTH_SHORT).show();
                }


            }
        });

        return progressDialog;
    }

    private void removeFavourite() {

        progressDialog.setMessage("Removing favourite ...");
        progressDialog.show();

        String forecastUrl = "http://rewardago.com/cron/favourite.php?id=" + MainActivity.UID + "&oid=" + cid + "&type=rmv";

        if (isNetworkAvailable()) {

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            toggleRefresh();
//                            progressBar.setVisibility(View.GONE);
//                            mSwipeRefreshWidget.setRefreshing(false);
                            progressDialog.dismiss();
                        }
                    });

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
                        removedRespone = response.body().string();
//                        removedRespone = stripHtml(response.body().string());


                       /* runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(CouponDetailsActivity.this, removedRespone, Toast.LENGTH_LONG).show();
                            }
                        });*/


//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {

                            removingFavourite(removedRespone);

                        } else {
                            progressDialog.dismiss();
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
//                    } catch (IOException e) {
//                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    } catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        } else {
            progressDialog.dismiss();
            Toast.makeText(this, getString(R.string.network_unavailable_message), Toast.LENGTH_LONG).show();

        }

    }

    private void removingFavourite(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);
//        JSONArray jsonArray = forecast.getJSONArray("User");
//        JSONObject jsonObject = jsonArray.getJSONObject(0);
        errorRemoving = forecast.getBoolean("error");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                progressDialog.dismiss();

                if (!errorRemoving) {
                    errorRemoving = true;
                    isEditTextVisible = 0;
                    mAddButton.setImageResource(R.drawable.ic_favorite_border_white_24dp);
                    final Toast toast = Toast.makeText(getApplicationContext(), "un-bookmarked", Toast.LENGTH_SHORT);
                    toast.show();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            toast.cancel();
                        }
                    }, 500);

                    if(getIntent().getIntExtra(EXTRA_PARAM_ENTERING_TPYE, 0) == 3){
                        // TODO Here.. no need to check redemeemed
//                        Toast.makeText(CouponDetailsActivity.this, "plz wait...", Toast.LENGTH_SHORT).show();
                        App.getInstance().claimCoupon(cid);
                    }



                } else {
                    Toast.makeText(CouponDetailsActivity.this, "something went wrong", Toast.LENGTH_SHORT).show();
                }


            }
        });

//        return progressDialog;
    }


    private void couponDetails(int s) {

        progressDialog.setMessage("Loading...");
        progressDialog.show();

        String forecastUrl = "http://rewardago.com/cron/json.php?id=" + s;
//                +getIntent().getStringExtra(EXTRA_PARAM_ID);

        if (isNetworkAvailable()) {

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
                        json_couponDetails = response.body().string();
//                        json_couponDetails = stripHtml(response.body().string());


                        /*runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(CouponDetailsActivity.this, "WTF "+json_couponDetails,Toast.LENGTH_LONG).show();
                            }
                        });*/


//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {

                            getCurrentDetails(json_couponDetails);

                        } else {
                            progressDialog.dismiss();
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
//                    } catch (IOException e) {
//                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    } catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        } else {
            progressDialog.dismiss();
            Toast.makeText(this, getString(R.string.network_unavailable_message), Toast.LENGTH_LONG).show();

        }

    }

    private void getCurrentDetails(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);

       /* App.EXTRA_PARAM_TITLE = forecast.getString("title");
        App.EXTRA_PARAM_SUBTITLE = forecast.getString("Sub_Title");
        App.EXTRA_PARAM_IMAGE = forecast.getString("Image");
        App.EXTRA_PARAM_STORE_LOGO = forecast.getString("Store_logo");
        App.EXTRA_PARAM_STORE_CONTACT = forecast.getString("contact");
        App.EXTRA_PARAM_STORE_ADDRESS = forecast.getString("Store_address");

        App.EXTRA_PARAM_COUPON_TYPE = forecast.getString("Type");
        App.EXTRA_PARAM_COUPON_QR_CODE = forecast.getString("QR");
        App.EXTRA_PARAM_COUPON_EXPIRY = String.valueOf(forecast.getInt("Expire"));

        App.EXTRA_PARAM_COUPON_HIDE_QR_CODE = forecast.getBoolean("hide_qr");*/

        EXTRA_PARAM_TITLE = forecast.getString("title");
        EXTRA_PARAM_SUBTITLE = forecast.getString("Sub_Title");
        EXTRA_PARAM_IMAGE = forecast.getString("Image");
        EXTRA_PARAM_STORE_LOGO = forecast.getString("Store_logo");
        EXTRA_PARAM_STORE_CONTACT = forecast.getString("contact");
        EXTRA_PARAM_STORE_ADDRESS = forecast.getString("Store_address");

        EXTRA_PARAM_COUPON_TYPE = forecast.getString("Type");
        EXTRA_PARAM_COUPON_QR_CODE = forecast.getString("QR");
        EXTRA_PARAM_COUPON_EXPIRY = String.valueOf(forecast.getInt("Expire"));
        EXTRA_PARAM_COUPON_URL = forecast.getString("target");


        EXTRA_PARAM_COUPON_HIDE_QR_CODE = forecast.getBoolean("hide_qr");

//        getIntent().putExtra(CouponDetailsActivity.EXTRA_PARAM_COUPON_CLAIMED, forecast.getString("Type"));
//        getIntent().putExtra(CouponDetailsActivity.EXTRA_PARAM_COUPON_REDEEMED, forecast.getString("Type"));

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

//                Picasso.with(App.getInstance()).load(App.EXTRA_PARAM_IMAGE)
                Picasso.with(App.getInstance()).load(EXTRA_PARAM_IMAGE)
                        .error(R.drawable.placeholder2)
                        .placeholder(R.drawable.placeholder2)
                        .into(mImageView);

//                Picasso.with(App.getInstance()).load(App.EXTRA_PARAM_STORE_LOGO)
                Picasso.with(App.getInstance()).load(EXTRA_PARAM_STORE_LOGO)
                        .error(R.drawable.placeholder2)
                        .placeholder(R.drawable.placeholder2)
                        .into(mImageView22);

//                progressDialog.dismiss();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                        tab3.performClick(); // also working
                        vp_message2.setCurrentItem(2);
                    }
                }, 10);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                        tab1.performClick(); // also working
                        vp_message2.setCurrentItem(0);


                    }
                }, 100);

//                new FetchWeatherData().execute();
favouriteChecker();

            }
        });

//        return progressDialog;
    }

    private void favouriteChecker() {

//        progressDialog.setMessage("fetching data ...");
//        progressDialog.show();

        String forecastUrl = "http://rewardago.com/cron/favourite.php?id="
                + MainActivity.UID + "?_=" + System.currentTimeMillis();
//

        if (isNetworkAvailable()) {

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
                        json_couponFavouriteDetails = response.body().string();
//                        json_couponFavouriteDetails = stripHtml(response.body().string());


//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                Toast.makeText(CouponDetailsActivity.this, json_couponFavouriteDetails,Toast.LENGTH_LONG).show();
//                            }
//                        });


//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {
//
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    progressDialog.dismiss();
//                                    Toast.makeText(CouponDetailsActivity.this, "response successful",Toast.LENGTH_SHORT).show();
//                                    Toast.makeText(CouponDetailsActivity.this, json_couponFavouriteDetails,Toast.LENGTH_LONG).show();
//                                }
//                            });

                            getFavouriteCoupons(json_couponFavouriteDetails);

                        } else {
                            progressDialog.dismiss();
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
//                    } catch (IOException e) {
//                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    } catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        } else {
            progressDialog.dismiss();
            Toast.makeText(this, getString(R.string.network_unavailable_message), Toast.LENGTH_LONG).show();

        }

    }

    private void getFavouriteCoupons(String jsonData) throws JSONException {
            JSONObject forecast = new JSONObject(jsonData);

            JSONArray currently = forecast.getJSONArray("favourite");

            fcids.clear();

            for (int i = 0; i < currently.length(); i++) {
                JSONObject post = currently.getJSONObject(i);
                fcids.add(post.getString("coup_id"));
            }

            /*List<String> list = new ArrayList<String>();
            list.add("Item 1");
            list.add("Item 2");
            String joined = TextUtils.join(", ", list);*/
//            Toast.makeText(CouponDetailsActivity.this,"favoured coupons:\n"+TextUtils.join(", ", fcids),Toast.LENGTH_SHORT).show();
            String[] sArray = fcids.toArray(new String[0]);
            sss = Arrays.toString(sArray);
            
            runOnUiThread(new Runnable() {
              @Override
              public void run() {
//                  Toast.makeText(CouponDetailsActivity.this, "favoured coupons:\n" + sss, Toast.LENGTH_SHORT).show();
//                  Toast.makeText(CouponDetailsActivity.this, "" + fcids.contains("" + cid), Toast.LENGTH_SHORT).show();

//                  progressDialog.dismiss();


                  if (fcids.contains(String.valueOf(cid))) {    // if contains wants to work... you must provide same datatype
                      isEditTextVisible = 1;
                      mAddButton.setImageResource(R.drawable.ic_favorite_red_24dp);
                  } else {
                      isEditTextVisible = 0;
                      mAddButton.setImageResource(R.drawable.ic_favorite_border_white_24dp);
                  }


//                  checkClaimedCoupons();

                  progressDialog.dismiss();



                  if(getIntent().getIntExtra(EXTRA_PARAM_ENTERING_TPYE, 0) == 3){
                      // TODO Here.. no need to check
                      // TODO redemeemed
                      removeFavourite();
//                      App.getInstance().claimCoupon(cid); // it will define at remvFav with condition

                  }else if(getIntent().getIntExtra(EXTRA_PARAM_ENTERING_TPYE, 0) == 3
                          && getIntent().getStringExtra(EXTRA_PARAM_REDEEM_STATUS) == "1"){

                      App.claim.setText("Redeem Again");
                      App.claim.setBackgroundColor(getResources().getColor(R.color.redeemAgain));

                  }else if(getIntent().getIntExtra(EXTRA_PARAM_ENTERING_TPYE, 0) == 5){
                      App.claim.setText("Show QR Code");
                      App.claim.setBackgroundColor(getResources().getColor(R.color.showCode));

                  }else{
                    checkRedeemedCoupons();
                  }


                   }
                 });

           


//            return progressDialog;

        }

    private void checkRedeemedCoupons() {

//        progressDialog.setMessage("checking redeemed ...");
//        progressDialog.show();

        String forecastUrl = "http://rewardago.com/cron/users.php?id="
                + cid + "?_=" + System.currentTimeMillis();
//

        if (isNetworkAvailable()) {

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {


                        redeemedDetails = response.body().string();
//                        redeemedDetails = stripHtml(response.body().string());

                       /* runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//                                progressDialog.dismiss();
//                                Toast.makeText(CouponDetailsActivity.this, redeemedDetails,Toast.LENGTH_LONG).show();
                            }
                        });*/


//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {

                            getRedeemedCoupons(redeemedDetails);

                        } else {
                            progressDialog.dismiss();
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
//                    } catch (IOException e) {
//                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    } catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        } else {
            progressDialog.dismiss();
            Toast.makeText(this, getString(R.string.network_unavailable_message), Toast.LENGTH_LONG).show();

        }

    }

    private void getRedeemedCoupons(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);


        cType = Integer.parseInt(forecast.getString("Type"));

        JSONArray currently = forecast.getJSONArray("Redeemed");

        rmduids.clear();

        for (int i = 0; i < currently.length(); i++) {
            rmduids.add( currently.getString(i) );
        }

            /*List<String> list = new ArrayList<String>();
            list.add("Item 1");
            list.add("Item 2");
            String joined = TextUtils.join(", ", list);*/
//            Toast.makeText(CouponDetailsActivity.this,"favoured coupons:\n"+TextUtils.join(", ", fcids),Toast.LENGTH_SHORT).show();
//        String[] sArray = cmduids.toArray(new String[0]);
//        ssssss = Arrays.toString(sArray);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {







//                  Toast.makeText(CouponDetailsActivity.this, "claimed users:\n" + ssssss, Toast.LENGTH_SHORT).show();



//                Toast.makeText(CouponDetailsActivity.this, "" + rmduids.contains(MainActivity.UID), Toast.LENGTH_SHORT).show();
                if (rmduids.contains(MainActivity.UID)) {    // if contains wants to work... you must provide same datatype
                    if(cType == 2){
                        // TODO here goes "redeemed_status":"0", "redeemed_status":"1".
                        // TODO progressDialog.dismiss();
//                        Toast.makeText(CouponDetailsActivity.this, "coupon type: "+cType,Toast.LENGTH_LONG).show();

                       /* if("redeem_status" == "1"){
                            App.claim.setText("Redeem Again");
                            App.claim.setBackgroundColor(getResources().getColor(R.color.redeemAgain));
                        }else{
                            App.claim.setText("Redeem Coupon");
                            App.claim.setBackgroundColor(getResources().getColor(R.color.redeemAgain));
                        }*/


                        App.claim.setText("Redeem Again");
                        App.claim.setBackgroundColor(getResources().getColor(R.color.redeemAgain));
//                        Toast.makeText(CouponDetailsActivity.this, claim.getText().toString(),Toast.LENGTH_LONG).show();
                    }else {
                        App.claim.setText("Redeemed");
                        App.claim.setBackgroundColor(getResources().getColor(R.color.redeemed));
//                        App.claim.setEnabled(false);
//                        Toast.makeText(CouponDetailsActivity.this, claim.getText().toString(),Toast.LENGTH_LONG).show();
                    }
                    progressDialog.dismiss();


                } else {
                    if(getIntent().getIntExtra(EXTRA_PARAM_ENTERING_TPYE,0) == 4){
                        App.claim.setText("Redeem Coupon");
                        App.claim.setBackgroundColor(getResources().getColor(R.color.redeemAgain));
                        progressDialog.dismiss();
                    }else {
                        checkClaimedCoupons();
                    }



                }



            }
        });




//            return progressDialog;

    }

    private void checkClaimedCoupons() {

//        progressDialog.setMessage("checking claimed ...");
//        progressDialog.show();

        String forecastUrl = "http://rewardago.com/cron/claimed.php?id="
                + cid + "?_=" + System.currentTimeMillis();
//

        if (isNetworkAvailable()) {

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
                        claimedDetails = response.body().string();
//                        claimedDetails = stripHtml(response.body().string());

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//                                progressDialog.dismiss();
//                                Toast.makeText(CouponDetailsActivity.this, claimedDetails,Toast.LENGTH_SHORT).show();
                            }
                        });


//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {

                            getClaimedCoupons(claimedDetails);

                        } else {
                            progressDialog.dismiss();
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
//                    } catch (IOException e) {
//                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    } catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        } else {
            progressDialog.dismiss();
            Toast.makeText(this, getString(R.string.network_unavailable_message), Toast.LENGTH_LONG).show();

        }

    }

    private void getClaimedCoupons(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);

        JSONArray currently = forecast.getJSONArray("Claimed");

        cmduids.clear();

        for (int i = 0; i < currently.length(); i++) {
            cmduids.add( currently.getString(i) );
        }

            /*List<String> list = new ArrayList<String>();
            list.add("Item 1");
            list.add("Item 2");
            String joined = TextUtils.join(", ", list);*/
//            Toast.makeText(CouponDetailsActivity.this,"favoured coupons:\n"+TextUtils.join(", ", fcids),Toast.LENGTH_SHORT).show();
//        String[] sArray = cmduids.toArray(new String[0]);
//        ssssss = Arrays.toString(sArray);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                  Toast.makeText(CouponDetailsActivity.this, "claimed users:\n" + ssssss, Toast.LENGTH_SHORT).show();

progressDialog.dismiss();

//                  Toast.makeText(CouponDetailsActivity.this, "" + cmduids.contains(MainActivity.UID), Toast.LENGTH_SHORT).show();
                if (cmduids.contains(MainActivity.UID)) {    // if contains wants to work... you must provide same datatype
//                    isEditTextVisible = 1;
//                    mAddButton.setImageResource(R.drawable.ic_favorite_white_24dp);
                    App.claim.setText("Show Coupon");
                    App.claim.setBackgroundColor(getResources().getColor(R.color.showCoupon));
//                    Toast.makeText(CouponDetailsActivity.this, claim.getText().toString(),Toast.LENGTH_LONG).show();

                } else {
//                    isEditTextVisible = 0;
//                    mAddButton.setImageResource(R.drawable.ic_favorite_border_white_24dp);
                    App.claim.setText("Claim Coupon");
                    App.claim.setBackgroundColor(getResources().getColor(R.color.claimCoupon));
//                    Toast.makeText(CouponDetailsActivity.this, claim.getText().toString(),Toast.LENGTH_LONG).show();
                }



            }
        });




//            return progressDialog;

    }





    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if (networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;

        }

        return isAvailable;
    }


    // here error will come
    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(getFragmentManager(), "error_dialog");
    }

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }



    public static void inflateDialogClaim(FragmentManager manager){
        DialogCliam dialogCliam = new DialogCliam();
        dialogCliam.show(manager,"slaam");
    }


    @Override
    public void onBackPressed() {

        App.personCanAddItem = true;
//        Offers.canAddItem = false;


// can be applied..... EXTRA_PARAM_ENTERING_TPYE or vp_message_main or bottomNavigation
        if(App.vp_message_main.getCurrentItem()==2){
            App.vp_message_main.setCurrentItem(0);
//            MainActivity.bottomNavigation.setCurrentItem(0);

            MainActivity.bottomNavigation.setCurrentItem(2);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    App.vp_message_main.setCurrentItem(2);
                }
            }, 300);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    App.vp_message_main.setCurrentItem(4);

                }
            }, 600);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    App.vp_message_main.setCurrentItem(2);

                    finish();
                }
            }, 900);



        }else if(App.vp_message_main.getCurrentItem()==3){
            MainActivity.bottomNavigation.setCurrentItem(3);
            App.vp_message_main.setCurrentItem(0);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    App.vp_message_main.setCurrentItem(3);
                    finish();
                }
            }, 600);



        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.like_menu , menu);
        return true;
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_review:
                Toast.makeText(this, "You have selected Bookmark Menu", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;

        } else if(id == R.id.like){
            showRatingDialog();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showRatingDialog() {

        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .iconRes(R.mipmap.ic_launcher)
                .limitIconToDefaultSize()
                .title("Rating & Reviews")

                .customView(R.layout.dialog_rating_review, true)
                .positiveText("OK")
                .negativeText("CANCEL")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                     int s = (int) reviewerRate.getRating();
//                        Toast.makeText(CouponDetailsActivity.this, ""+s, Toast.LENGTH_SHORT).show();
                        rating(nm.getText().toString().trim(),
                                mail.getText().toString().trim(),
                                note.getText().toString().trim(),
                                s);

//                        rButton = (RadioButton) dialog.getCustomView().findViewById(rGroup.getCheckedRadioButtonId());
//                        Toast.makeText(myContext, rButton.getTag().toString(), Toast.LENGTH_SHORT).show();
//                        Toast.makeText(myContext, passwordInput.getText().toString().trim(), Toast.LENGTH_SHORT).show();

//                        removeSubscription(rButton.getTag().toString(),
//                                passwordInput.getText().toString().trim() );

                    }
                }).build();

        positiveAction = dialog.getActionButton(DialogAction.POSITIVE);


//        rGroup = (RadioGroup) dialog.getCustomView().findViewById(R.id.rb);
        reviewerRate = (RatingBar) dialog.findViewById(R.id.reviewerRate);
        nm = (EditText) dialog.getCustomView().findViewById(R.id.nm);
        mail = (EditText) dialog.getCustomView().findViewById(R.id.mail);
        note = (EditText) dialog.getCustomView().findViewById(R.id.note);

        nm.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                positiveAction.setEnabled(s.toString().trim().length() > 0);
            }

            @Override
            public void afterTextChanged(Editable s) {
                String name = s.toString().trim();
                if (name.isEmpty()) {
                    nm.setError("name is empty");
                    bnm = false;
                } else {
                    nm.setError(null);
                    bnm = true;

                }
                positiveAction.setEnabled((bnm && bmail && bnote));
               /* if(bnm && bmail && bnote){
                    positiveAction.setEnabled(s.toString().trim().length() > 3);
                }*/

            }
        });
        mail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                positiveAction.setEnabled(s.toString().trim().length() > 0);
            }

            @Override
            public void afterTextChanged(Editable s) {
                String email = s.toString().trim();
                if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    mail.setError("enter a valid email address");
                    bmail = false;
                } else {
                    mail.setError(null);
                    bmail = true;
                }
                positiveAction.setEnabled((bnm && bmail && bnote));
            }
        });
        note.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                positiveAction.setEnabled(s.toString().trim().length() > 0);
            }

            @Override
            public void afterTextChanged(Editable s) {
                String notte = s.toString().trim();
                if (notte.isEmpty() || notte.length() < 3) {
                    note.setError("at least 3 characters");
                    bnote = false;
                } else {
                    note.setError(null);
                    bnote = true;
                }
                positiveAction.setEnabled((bnm && bmail && bnote));
            }
        });


        dialog.show();
        positiveAction.setEnabled(false); // disabled by default
        
    }

    private void rating(String name, String email, String note, int rate) {

        String forecastUrl = "http://rewardago.com/cron/json.php?rid="+cid
                +"&nm="+name+"&mail="+email+"&note="+note+"&r="+rate;
//                +MainActivity.UID+"&r="+reason+"&rm="+remarks;
//                +MainActivity.UID+"?_=" + System.currentTimeMillis();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("removing subscription");
        progressDialog.setCancelable(false);
        progressDialog.show();

        if (isNetworkAvailable()){

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    /*myContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            toggleRefresh();
//                            progressBar.setVisibility(View.GONE);
//                            mSwipeRefreshWidget.setRefreshing(false);

                        }
                    });*/
                    progressDialog.dismiss();
                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
//                        String jsonData = response.body().string();
//                        jsonData = response.body().string();
                        jsonSubs = stripHtml(response.body().string());




                       /* runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(CouponDetailsActivity.this, jsonSubs,Toast.LENGTH_LONG).show();
                            }
                        });*/


//                        Log.v(TAG, jsonSubs);
                        if (response.isSuccessful()) {

                            removingSubscription(jsonSubs);





                        } else {
                            progressDialog.dismiss();
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        }

        else {
            progressDialog.dismiss();
            Toast.makeText(this, getString(R.string.network_unavailable_message), Toast.LENGTH_LONG).show();
        }

    }

    private void removingSubscription(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);
        JSONArray jsonArray = forecast.getJSONArray("Review");
        JSONObject currently = jsonArray.getJSONObject(0);

        errorStatus = currently.getBoolean("Error");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {


//                Toast.makeText(myContext, status, Toast.LENGTH_SHORT).show();
                if(!errorStatus){
                    errorStatus = true;
                    Toast.makeText(CouponDetailsActivity.this, "success", Toast.LENGTH_SHORT).show();


                    vp_message2.setCurrentItem(0);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            vp_message2.setCurrentItem(2);
                        }
                    }, 80);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    }, 120);


                }else {
                    Toast.makeText(CouponDetailsActivity.this, "not success", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


}
