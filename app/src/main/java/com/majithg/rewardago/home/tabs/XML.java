package com.majithg.rewardago.home.tabs;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.majithg.rewardago.home.content.CouponDetailsActivity;
import com.majithg.rewardago.util.AlertDialogFragment;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * ZLB
 */
public class XML extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final int limit = 50;
    private static final String TAG = XML.class.getSimpleName();

    Context applicationContext = MainActivity.getContextOfApplication();


    Cursor curLog;
    private int messageType;
    private FragmentActivity myContext;
    private ReviewAdapter mAdapter;

    private String jsonData;

    private List<ReviewData> mCurrentWeather = new ArrayList<>();


    private RecyclerView mRecyclerView;
    private TextView emptyViewoffers;


    public XML() {
    }

    public static XML newInstance(int param1) {
        XML fragment = new XML();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        myContext = (FragmentActivity) activity;
        super.onAttach(activity);
//        Toast.makeText(getActivity(),"onAttach()", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setRetainInstance(true);

        if (getArguments() != null) {
            messageType = getArguments().getInt(ARG_PARAM1);
        } else {
            messageType = 0;
        }

        getForecast();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.xml, container, false);

        mRecyclerView = (RecyclerView) root.findViewById(R.id.review_list);
        emptyViewoffers = (TextView) root.findViewById(R.id.empty_view_xml);

        return root;
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
//        setMainRecyclerView();
//        setCallLogs(curLog);
        getForecast();
    }




     /*public void setMainRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        SwipeRecyclerViewAdapter mAdapter = new SwipeRecyclerViewAdapter(applicationContext, mDataSet);
        ((SwipeRecyclerViewAdapter) mAdapter).setMode(Attributes.Mode.Single);
        mRecyclerView.setAdapter(mAdapter);
    }*/


    private void getForecast() {

        String forecastUrl = "http://rewardago.com/cron/review.php?id=" + CouponDetailsActivity.cid
                +"?_=" + System.currentTimeMillis();
//        String forecastUrl = "http://rewardago.com/cron/review.php?id=557";

        if (isNetworkAvailable()) {

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    myContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            toggleRefresh();
//                            progressBar.setVisibility(View.GONE);
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    });

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {
                    /*runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toggleRefresh();
                        }
                    });*/
                    try {
//                        String jsonData = response.body().string();
                        jsonData = stripHtml(response.body().string());

                       /* myContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(myContext, jsonData,Toast.LENGTH_LONG).show();
                            }
                        });*/

                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {
                         mCurrentWeather = getCurrentDetails(jsonData);
                           /* myContext.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
//                                    updateDisplay();
                                    progressBar.setVisibility(View.GONE);
                                    mSwipeRefreshWidget.setRefreshing(false);
                                }
                            });*/

                        } else {
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    } catch (IOException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    } catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        } else {
            Toast.makeText(myContext, getString(R.string.network_unavailable_message), Toast.LENGTH_LONG).show();
//            mSwipeRefreshWidget.setRefreshing(false);   // i think.. an error may occur here, if we put
//            progressBar.setVisibility(View.GONE);
        }

    }


    private List<ReviewData> getCurrentDetails(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);

        JSONArray currently = forecast.getJSONArray("Review");

//        ArrayList<HashMap<String, String>> blogCoupons = new ArrayList<HashMap<String, String>>();
        List<ReviewData> blogCoupons = new ArrayList<>();

        for (int i = 0; i < currently.length(); i++) {
            ReviewData currentWeather = new ReviewData();
            JSONObject post = currently.getJSONObject(i);

//            String title = post.getString("id");
//            currentWeather.setId(post.getInt("id"));
            currentWeather.setReviewerImage(post.getString("Image"));
            currentWeather.setReviewerName(post.getString("name"));
            currentWeather.setReviewerComment(post.getString("note"));
            currentWeather.setReviewerRate(post.getString("ratings"));




           /* title = Html.fromHtml(title).toString();
            String author = post.getString(KEY_AUTHOR);
            author = Html.fromHtml(author).toString();

            HashMap<String, String> blogPost = new HashMap<String, String>();
            blogPost.put(KEY_TITLE, title);
            blogPost.put(KEY_AUTHOR, author);*/


//            blogPosts.add(blogPost);
            blogCoupons.add(currentWeather);
        }


        myContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                                    updateDisplay();



               /* if (mCurrentWeather.isEmpty()) {
                    mRecyclerView.setVisibility(View.GONE);
                    emptyViewoffers.setVisibility(View.VISIBLE);
                }
                else {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    emptyViewoffers.setVisibility(View.GONE);
                }*/

                mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                mRecyclerView.setHasFixedSize(true);
                mAdapter = new ReviewAdapter(getActivity(), mCurrentWeather);
                mRecyclerView.setAdapter(mAdapter);
//                mAdapter.setOnItemClickListener(onItemClickListener);



            }

        });


//        return currentWeather;
        return blogCoupons;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)
                myContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if (networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;

        }

        return isAvailable;
    }

    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(myContext.getFragmentManager(), "error_dialog");
    }


    public String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }
}
