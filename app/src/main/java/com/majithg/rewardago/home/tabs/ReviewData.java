package com.majithg.rewardago.home.tabs;

/**
 * Created by megha on 15-03-06.
 */
public class ReviewData {


  public String reviewerImage;
  public String reviewerName;
  public String reviewerComment;
  public String reviewerRate;


  public ReviewData() {
  }

  public ReviewData(String reviewerImage, String reviewerName, String reviewerComment, String reviewerRate) {
    this.reviewerImage = reviewerImage;
    this.reviewerName = reviewerName;
    this.reviewerComment = reviewerComment;
    this.reviewerRate = reviewerRate;
  }

  public String getReviewerImage() {
    return reviewerImage;
  }

  public void setReviewerImage(String reviewerImage) {
    this.reviewerImage = reviewerImage;
  }

  public String getReviewerName() {
    return reviewerName;
  }

  public void setReviewerName(String reviewerName) {
    this.reviewerName = reviewerName;
  }

  public String getReviewerComment() {
    return reviewerComment;
  }

  public void setReviewerComment(String reviewerComment) {
    this.reviewerComment = reviewerComment;
  }

  public String getReviewerRate() {
    return reviewerRate;
  }

  public void setReviewerRate(String reviewerRate) {
    this.reviewerRate = reviewerRate;
  }
}
