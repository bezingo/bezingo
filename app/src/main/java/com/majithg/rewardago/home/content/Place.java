package com.majithg.rewardago.home.content;

import android.content.Context;

/**
 * Created by megha on 15-03-06.
 */
public class Place {

  public int imageResourceId;
  public int id;
  public String name;
  public String name2;
  public String imageName;
  public String imageName2;
  public boolean isFav;
  public String Redeem_status;
  public String qr_code;

  public Place() {
  }


  public int getImageResourceId() {

//    int x = context.getResources().getIdentifier(this.imageName, "drawable", context.getPackageName());
    int y = imageResourceId;

    return y;
  }

  public String getImageName() {
    return imageName;
  }

  public void setImageName(String imageName) {
    this.imageName = imageName;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getName2() {
    return name2;
  }

  public void setName2(String name2) {
    this.name2 = name2;
  }

  public String getImageName2() {
    return imageName2;
  }

  public void setImageName2(String imageName2) {
    this.imageName2 = imageName2;
  }

  public String getRedeem_status() {
    return Redeem_status;
  }

  public void setRedeem_status(String redeem_status) {
    Redeem_status = redeem_status;
  }

  public String getQr_code() {
    return qr_code;
  }

  public void setQr_code(String qr_code) {
    this.qr_code = qr_code;
  }
}
