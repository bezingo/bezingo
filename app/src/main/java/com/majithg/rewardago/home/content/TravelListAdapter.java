package com.majithg.rewardago.home.content;

import android.content.Context;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by megha on 15-03-06.
 */
public class TravelListAdapter extends RecyclerView.Adapter<TravelListAdapter.ViewHolder> {

  Context mContext ;
  OnItemClickListener mItemClickListener;
  List<Place> mlist;

  Context applicationContext = MainActivity.getContextOfApplication();

  int clr;

  public TravelListAdapter(Context context, List<Place> list) {
    this.mContext = context;
    this.mlist = list;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_places, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder, int position) {
//    final Place place = new PlaceData().placeList().get(position);


//      Place currentWeather = new Place();
      Place place = mlist.get(position);

      holder.placeName.setText(place.name);
      holder.placeNameDesc.setText(place.name2); // to remove second subtitle
      //Download image using picasso library
      Picasso.with(mContext).load(place.getImageName())
              .error(R.drawable.placeholder2)
              .placeholder(R.drawable.placeholder2)
              .into(holder.placeImage);
//      holder.placeImage.setImageResource(place.imageResourceId);


      Picasso.with(mContext).load(place.getImageName2())
              .error(R.drawable.placeholder2)
              .placeholder(R.drawable.placeholder2)
              .into(holder.placeImage2);



//    Picasso.with(mContext).load(place.getImageResourceId(mContext)).into(holder.placeImage);
//    Picasso.with(mContext).load(place.getImageResourceId()).into(holder.placeImage);





    // removing color bar color
/*

//    Bitmap photo = BitmapFactory.decodeResource(mContext.getResources(), place.getImageResourceId(mContext));
    Bitmap photo = BitmapFactory.decodeResource(App.getInstance().getResources(), place.getImageResourceId());
    Palette.generateAsync(photo, new Palette.PaletteAsyncListener() {
      public void onGenerated(Palette palette) {
        int mutedLight = palette.getMutedColor(mContext.getResources().getColor(android.R.color.black));
        holder.placeNameHolder.setBackgroundColor(mutedLight);

        clr = mutedLight;

      }
    });
*/


// try it later...
//    Drawable background = null;
//    if (background instanceof ShapeDrawable) {
      // cast to 'ShapeDrawable'
//      ShapeDrawable shapeDrawable = (ShapeDrawable)background;
//      shapeDrawable.getPaint().setColor(mContext.getResources().getColor(R.color.colorToSet));
//      shapeDrawable.getPaint().setColor(clr);
//    holder.addFavouriteImage.setImageDrawable(shapeDrawable);
//    }
    /*else if (background instanceof GradientDrawable) {
      // cast to 'GradientDrawable'
      GradientDrawable gradientDrawable = (GradientDrawable)background;
      gradientDrawable.setColor(getResources().getColor(R.color.colorToSet));
    } else if (background instanceOf ColorDrawable) {
      // alpha value may need to be set again after this call
      Colordrawable colorDrawable = (ColorDrawable)background;
      colorDrawable.setColor(getResources().getColor(R.color.colorToSet));
    }*/
  }
  @Override
  public int getItemCount() {
    return (null != mlist ? mlist.size() : 0);
  }

 /* @Override
  public int getItemCount() {
    return new PlaceData().placeList().size();
  }*/

  public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public LinearLayout placeHolder;
//    public LinearLayout placeNameHolder;
    public LinearLayoutCompat placeNameHolder;
    public TextView placeName;
    public TextView placeNameDesc;
    public ImageView placeImage;
      public ImageView placeImage2;
//    public ImageView addFavouriteImage;

      public Button claimButton;

    public ViewHolder(View itemView) {
      super(itemView);
      placeHolder = (LinearLayout) itemView.findViewById(R.id.mainHolder);
      placeName = (TextView) itemView.findViewById(R.id.placeName);
      placeNameDesc = (TextView) itemView.findViewById(R.id.placeNameDesc);  // to remove empty text space
//      placeNameHolder = (LinearLayout) itemView.findViewById(R.id.placeNameHolder);
      placeNameHolder = (LinearLayoutCompat) itemView.findViewById(R.id.placeNameHolder);
      placeImage = (ImageView) itemView.findViewById(R.id.placeImage11);
        placeImage2 = (ImageView) itemView.findViewById(R.id.placeImage112);
//      addFavouriteImage = (ImageView) itemView.findViewById(R.id.btn_add);
      placeHolder.setOnClickListener(this);

        claimButton = (Button) itemView.findViewById(R.id.claimButton);
        claimButton.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
      if (mItemClickListener != null) {
        mItemClickListener.onItemClick(itemView, getPosition());
      }
    }
  }

  public interface OnItemClickListener {
    void onItemClick(View view, int position);
  }

  public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
    this.mItemClickListener = mItemClickListener;
  }



}
