package com.majithg.rewardago.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.majithg.rewardago.home.content.CouponCategoryActivity;
import com.majithg.rewardago.home.content.CouponDetailsActivity;
import com.majithg.rewardago.home.content.Place;
import com.majithg.rewardago.home.content.TravelListAdapter;
import com.majithg.rewardago.outlets.outlets;
import com.majithg.rewardago.util.AlertDialogFragment;
import com.majithg.rewardago.util.App;
import com.majithg.rewardago.util.FragmentLifecycle;
import com.majithg.rewardago.util.MainViewPagerAdapter;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Home extends Fragment implements FragmentLifecycle
//        ,Updateable
//        ,MainActivity.OnBackPressedListener
{

    private static final String TAG = Home.class.getSimpleName();


    private static final String ARG_PARAM1 = "param1";
    private static final int limit = 50;

    private int messageType;

    private RecyclerView mRecyclerView;
    private StaggeredGridLayoutManager mStaggeredLayoutManager;

    private List<Place> mCurrentWeather;

    TextView l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12,l13,l14,l15;

    private ProgressBar progressBar;
    private SwipeRefreshLayout mSwipeRefreshWidget;

//    public static boolean canAddItem = false;

    private FragmentActivity myContext;

//    private Calendar startTime = Calendar.getInstance();
    private TravelListAdapter mAdapter;

    private String jsonData;

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        App.appmyContext = myContext;
        super.onAttach(activity);
//        Toast.makeText(getActivity(),"onAttach()", Toast.LENGTH_SHORT).show();

    }


    public Home() {
    }

    public static Home newInstance(int param1) {
        Home fragment = new Home();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

   /* @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.v(TAG, "In frag's on save instance state ");
//        outState.putSerializable("starttime", startTime);
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            messageType = getArguments().getInt(ARG_PARAM1);
        } else {
            messageType = 0;
        }

        setHasOptionsMenu(true);

       getForecast();

//       getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

    }



    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.home, container, false);

        progressBar = (ProgressBar) root.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);

        mSwipeRefreshWidget = (SwipeRefreshLayout) root.findViewById(R.id.swipe_refresh_widget_home);
        mSwipeRefreshWidget.setColorScheme(R.color.colorPrimaryDark, R.color.colorPrimaryDark, R.color.colorPrimaryDark, R.color.colorPrimaryDark);


        l1 = (TextView) root.findViewById(R.id.Automotive);
        l2 = (TextView) root.findViewById(R.id.Cafes_and_Lounges);
        l3 = (TextView) root.findViewById(R.id.Desserts);
        l4 = (TextView) root.findViewById(R.id.Educations);
        l5 = (TextView) root.findViewById(R.id.Electronics);
        l6 = (TextView) root.findViewById(R.id.Entertainment_Actvities);
        l7 = (TextView) root.findViewById(R.id.Food_and_Dining);
        l8 = (TextView) root.findViewById(R.id.Health_and_Fitness);
        l9 = (TextView) root.findViewById(R.id.Home_Appliances);
        l10= (TextView) root.findViewById(R.id.Home_Decor);
        l11= (TextView) root.findViewById(R.id.Medical);
        l12= (TextView) root.findViewById(R.id.Mobiles_accessories);
        l13= (TextView) root.findViewById(R.id.Services);
        l14= (TextView) root.findViewById(R.id.Shopping);
        l15= (TextView) root.findViewById(R.id.Travel);






        l1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transitionIntent = new Intent(v.getContext(), CouponCategoryActivity.class);
                transitionIntent.putExtra(CouponCategoryActivity.EXTRA_PARAM_CATEGORY, "Automotive");
                myContext.startActivity(transitionIntent);
            }
        });
        l2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transitionIntent = new Intent(v.getContext(), CouponCategoryActivity.class);
                transitionIntent.putExtra(CouponCategoryActivity.EXTRA_PARAM_CATEGORY, "Cafes_and_Lounges");
                myContext.startActivity(transitionIntent);
            }
        });
        l3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transitionIntent = new Intent(v.getContext(), CouponCategoryActivity.class);
                transitionIntent.putExtra(CouponCategoryActivity.EXTRA_PARAM_CATEGORY, "Desserts");
                myContext.startActivity(transitionIntent);
            }
        });
        l4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transitionIntent = new Intent(v.getContext(), CouponCategoryActivity.class);
                transitionIntent.putExtra(CouponCategoryActivity.EXTRA_PARAM_CATEGORY, "Educations");
                myContext.startActivity(transitionIntent);
            }
        });
        l5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transitionIntent = new Intent(v.getContext(), CouponCategoryActivity.class);
                transitionIntent.putExtra(CouponCategoryActivity.EXTRA_PARAM_CATEGORY, "Electronics");
                myContext.startActivity(transitionIntent);
            }
        });
        l6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transitionIntent = new Intent(v.getContext(), CouponCategoryActivity.class);
                transitionIntent.putExtra(CouponCategoryActivity.EXTRA_PARAM_CATEGORY, "Entertainment_Actvities");
                myContext.startActivity(transitionIntent);
            }
        });
        l7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transitionIntent = new Intent(v.getContext(), CouponCategoryActivity.class);
                transitionIntent.putExtra(CouponCategoryActivity.EXTRA_PARAM_CATEGORY, "Food_and_Dining");
                myContext.startActivity(transitionIntent);
            }
        });
        l8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transitionIntent = new Intent(v.getContext(), CouponCategoryActivity.class);
                transitionIntent.putExtra(CouponCategoryActivity.EXTRA_PARAM_CATEGORY, "Health_and_Fitness");
                myContext.startActivity(transitionIntent);
            }
        });
        l9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transitionIntent = new Intent(v.getContext(), CouponCategoryActivity.class);
                transitionIntent.putExtra(CouponCategoryActivity.EXTRA_PARAM_CATEGORY, "Home_Appliances");
                myContext.startActivity(transitionIntent);
            }
        });
        l10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transitionIntent = new Intent(v.getContext(), CouponCategoryActivity.class);
                transitionIntent.putExtra(CouponCategoryActivity.EXTRA_PARAM_CATEGORY, "Home_Decor");
                myContext.startActivity(transitionIntent);
            }
        });
        l11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transitionIntent = new Intent(v.getContext(), CouponCategoryActivity.class);
                transitionIntent.putExtra(CouponCategoryActivity.EXTRA_PARAM_CATEGORY, "Medical");
                myContext.startActivity(transitionIntent);
            }
        });
        l12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transitionIntent = new Intent(v.getContext(), CouponCategoryActivity.class);
                transitionIntent.putExtra(CouponCategoryActivity.EXTRA_PARAM_CATEGORY, "Mobiles_accessories");
                myContext.startActivity(transitionIntent);
            }
        });
        l13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transitionIntent = new Intent(v.getContext(), CouponCategoryActivity.class);
                transitionIntent.putExtra(CouponCategoryActivity.EXTRA_PARAM_CATEGORY, "Services");
                myContext.startActivity(transitionIntent);
            }
        });

        l14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transitionIntent = new Intent(v.getContext(), CouponCategoryActivity.class);
                transitionIntent.putExtra(CouponCategoryActivity.EXTRA_PARAM_CATEGORY, "Shopping");
                myContext.startActivity(transitionIntent);
            }
        });

        l15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transitionIntent = new Intent(v.getContext(), CouponCategoryActivity.class);
                transitionIntent.putExtra(CouponCategoryActivity.EXTRA_PARAM_CATEGORY, "Travel");
                myContext.startActivity(transitionIntent);
            }
        });


        mRecyclerView = (RecyclerView) root.findViewById(R.id.list);

        mSwipeRefreshWidget.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getForecast();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mSwipeRefreshWidget.setRefreshing(false);
                        progressBar.setVisibility(View.GONE);
                    }
                }, 2000);

//                MainActivity.fAdapter.notifyDataSetChanged(); // ithe potta update toast work aahum...
//                App.vp_message_main.getAdapter().notifyDataSetChanged(); // ithe potta update toast work aahum...


               /* new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        mStaggeredLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
                        mRecyclerView.setLayoutManager(mStaggeredLayoutManager);
                        mRecyclerView.setHasFixedSize(true);
                        mAdapter = new TravelListAdapter(getActivity(), mCurrentWeather);
                        mRecyclerView.setAdapter(mAdapter);
                        mAdapter.setOnItemClickListener(onItemClickListener);
                    }
                }, 2000);*/

                // plz give some times to ... after that load recycler...
            }
        });



//        isListView = true;

        MainActivity.toolbar.getMenu().clear();



        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                mSwipeRefreshWidget.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
            }
        }, 5000);



        return root;
    }

 /*   public void updateView() {
        // do something to update the fragment
//        getForecast();
//        mRecyclerView.setAdapter(App.mAdapter);
//        App.mAdapter.notifyDataSetChanged();
//        mmadapter.notifyDataSetChanged();
    }*/

   /* public void updateDate(String adapter) {
//    public void updateDate(MainViewPagerAdapter adapter) {
//        Log.d(TAG, "updateDate(" + formatDate(date) + ")");
//        mDate = date;
//        mTextView.setText(mDate.toString());

//        mRecyclerView.setAdapter(adapter);
//        adapter.notifyDataSetChanged();
        Toast.makeText(myContext,""+adapter,Toast.LENGTH_SHORT).show();
//        adapter.notifyDataSetChanged();
//        this.mmadapter = adapter;

        mStaggeredLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mStaggeredLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new TravelListAdapter(getActivity(), mCurrentWeather);
        mRecyclerView.setAdapter(mAdapter);
//        mAdapter.setOnItemClickListener(onItemClickListener);
    }*/

/*

    TravelListAdapter.OnItemClickListener onItemClickListener = new TravelListAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(View v, int position) {



//            Intent transitionIntent = new Intent(v.getContext(), DetailActivity.class);
//            Intent transitionIntent = new Intent(v.getContext(), Activity2.class);
//            transitionIntent.putExtra(DetailActivity.EXTRA_PARAM_ID, position);



          */
/*  ImageView placeImage = (ImageView) v.findViewById(R.id.placeImage);
            LinearLayout placeNameHolder = (LinearLayout) v.findViewById(R.id.placeNameHolder);
            View navigationBar = v.findViewById(android.R.id.navigationBarBackground);
            View statusBar = v.findViewById(android.R.id.statusBarBackground);*//*


    */
/*        Pair<View, String> imagePair = Pair.create((View) placeImage, "tImage");
            Pair<View, String> holderPair = Pair.create((View) placeNameHolder, "tNameHolder");
            Pair<View, String> navPair = Pair.create(navigationBar, Window.NAVIGATION_BAR_BACKGROUND_TRANSITION_NAME);
            Pair<View, String> statusPair = Pair.create(statusBar, Window.STATUS_BAR_BACKGROUND_TRANSITION_NAME);

//            Pair<View, String> toolbarPair = Pair.create((View)toolbar, "tActionBar");

            ActivityOptionsCompat options =
               ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(),imagePair, holderPair, navPair, statusPair
//                  , toolbarPair
            );*//*

//            ActivityCompat.startActivity(getActivity(), transitionIntent, options.toBundle());
//            myContext.startActivity(transitionIntent);
//            plz don't call activity.... that is not suite according to ui DESIGN
//            so call fragment...

            MainActivity.toolbar.getMenu().clear();

            Bundle bundle=new Bundle();
//            bundle.putString("message", "From Activity");
//            bundle.putInt("message", position);
            bundle.putInt("message1", mCurrentWeather.get(position).getId());
            bundle.putString("message2", mCurrentWeather.get(position).getName());
            bundle.putString("message3", mCurrentWeather.get(position).getName2());
            bundle.putString("message4", mCurrentWeather.get(position).getImageName());


//            set Fragmentclass Arguments
//            Fragmentclass fragobj=new Fragmentclass();

            HomeInside fone = HomeInside.newInstance(10);
//            MainActivity.f5.setArguments(bundle);

//            Fragment fragment = null;
//            fragment = new HomeInside();
//            fragobj.setArguments(bundle);
//            fragment.setArguments(bundle);

            fone.setArguments(bundle);

*/
/*

            FragmentManager manager = myContext.getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
//            transaction.replace(R.id.inside, fragment);
            transaction.replace(R.id.inside, fone);

            transaction.addToBackStack(null);
            transaction.commit();
//            mRecyclerView.setVisibility(View.GONE);
//            MainActivity.vp_message_main.setCurrentItem(1,false);


*//*



            Intent transitionIntent = new Intent(v.getContext(), CouponDetailsActivity.class);
            transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_POSITION, position);
            transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_ID, mCurrentWeather.get(position).getId());
            transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_TITLE, mCurrentWeather.get(position).getName());
            transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_SUBTITLE, mCurrentWeather.get(position).getName2());
            transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_IMAGE, mCurrentWeather.get(position).getImageName());
            myContext.startActivity(transitionIntent);
        }
    };
*/




    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.fragment_menu_home, menu);
        if(App.HomeCanAddItem) {
            MainActivity.toolbar.getMenu().clear();
            MainActivity.toolbar.getMenu().add(0, 5, Menu.NONE, null).setIcon(R.drawable.ic_arrow_24dp).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        }else {
            MainActivity.toolbar.getMenu().clear();
            MainActivity.toolbar.getMenu().add(0, 3, Menu.NONE, null).setIcon(R.drawable.ic_search_24dp).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            MainActivity.toolbar.getMenu().add(0, 5, Menu.NONE, null).setIcon(R.drawable.ic_store_black_24dp).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(App.HomeCanAddItem) {



        }else {

            //noinspection SimplifiableIfStatement
            if (id == 5) {
//            bottomNavigation.setCurrentItem(0);
//            vp_message.setCurrentItem(0);
//            getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();

            MainActivity.toolbar.getMenu().clear();
                App.HomeCanAddItem = true;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        outlets fone = outlets.newInstance(15);
                        FragmentManager manager = myContext.getSupportFragmentManager();
                        FragmentTransaction transaction = manager.beginTransaction();
                        transaction.replace(R.id.inside, fone, "STORE");
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                }, 50);


//            Toast.makeText(myContext, "store", Toast.LENGTH_SHORT).show();
                return true;
            }
        /*else if (id == android.R.id.home) {
//            onBackPressed();
            sliderLayout.toggleMenu();
            return true;
        }*/
        }

        return super.onOptionsItemSelected(item);
    }



    private void getForecast() {

        String forecastUrl = "http://rewardago.com/cron/json.php";

        if (isNetworkAvailable()){

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    myContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    });

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
                        myContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//                                    updateDisplay();
//                                progressBar.setVisibility(View.GONE);
                                mSwipeRefreshWidget.setRefreshing(false);
                            }
                        });
//                        String jsonData = response.body().string();
                        jsonData = stripHtml(response.body().string());
                       /* myContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(myContext, jsonData,Toast.LENGTH_LONG).show();
                            }
                        });*/
                        if (response.isSuccessful()) {
                            mCurrentWeather = getCurrentDetails(jsonData);
                        } else {
                            alertUserAboutError();
                        }
                    }
                    catch (IOException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }



                }
            });
        }

        else {
            Toast.makeText(myContext, getString(R.string.network_unavailable_message),Toast.LENGTH_LONG).show();
//            mSwipeRefreshWidget.setRefreshing(false);   // i think.. an error may occur here, if we put
//            progressBar.setVisibility(View.GONE);
//            stopRefreshing(); don't use anything here ...
        }

    }

   /* private void toggleRefresh() {
        if (mProgressBar.getVisibility() == View.INVISIBLE) {
            mProgressBar.setVisibility(View.VISIBLE);
            mRefreshImageView.setVisibility(View.INVISIBLE);
        } else {
            mProgressBar.setVisibility(View.INVISIBLE);
            mRefreshImageView.setVisibility(View.VISIBLE);
        }
    }*/

/*    private void updateDisplay() {
        mTemperatureLabel.setText(mCurrentWeather.getTemperature() + "");
        mTimeLabel.setText("At " + mCurrentWeather.getFormattedTime() + " it will be");
        mHumidityValue.setText(mCurrentWeather.getHumidity() + "");
        mPrecipValue.setText(mCurrentWeather.getPrecipChance() + "%");
        mSummaryLabel.setText(mCurrentWeather.getSummary());

        Drawable drawable = getResources().getDrawable(mCurrentWeather.getIconId());
        mIconImageView.setImageDrawable(drawable);
    }*/

    private List<Place> getCurrentDetails(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);


//        String timezone = forecast.getString("timezone");
//        Log.i(TAG, getString(R.string.debug_from_JSON) + timezone);

//        JSONObject currently = forecast.getJSONObject("currently");
        JSONArray currently  = forecast.getJSONArray("Coupon");
//        JSONArray currently  = forecast.getJSONArray("json");


//        ArrayList<HashMap<String, String>> blogCoupons = new ArrayList<HashMap<String, String>>();
        List<Place> blogCoupons = new ArrayList<>();

        for (int i = 0; i < currently.length(); i++) {
//            for (int i = 0; i < 5; i++) {
            Place currentWeather = new Place();

            JSONObject post = currently.getJSONObject(i);

//            String title = post.getString("id");
            currentWeather.setId(post.getInt("id"));
            currentWeather.setName(post.getString("title"));
            currentWeather.setName2(post.getString("Sub_Title"));
//            currentWeather.setImageResourceId(post.getString("image"));
            currentWeather.setImageName(post.getString("image"));
            currentWeather.setImageName2(post.getString("Store_logo"));



           /* title = Html.fromHtml(title).toString();
            String author = post.getString(KEY_AUTHOR);
            author = Html.fromHtml(author).toString();

            HashMap<String, String> blogPost = new HashMap<String, String>();
            blogPost.put(KEY_TITLE, title);
            blogPost.put(KEY_AUTHOR, author);*/


//            blogPosts.add(blogPost);
            blogCoupons.add(currentWeather);
        }

     /*   String[] keys = { KEY_TITLE, KEY_AUTHOR };
        int[] ids = { android.R.id.text1, android.R.id.text2 };
        SimpleAdapter adapter = new SimpleAdapter(this, blogPosts,
                android.R.layout.simple_list_item_2,
                keys, ids);*/

//        listView.setAdapter(adapter);


//        Place currentWeather = new Place();
//        currentWeather.setHumidity(currently.getDouble("humidity"));
//        currentWeather.setTime(currently.getLong("time"));
//        currentWeather.setIcon(currently.getString("icon"));
//        currentWeather.setPrecipChance(currently.getDouble("precipProbability"));
//        currentWeather.setSummary(currently.getString("summary"));
//        currentWeather.setTemperature(currently.getDouble("temperature"));
//        currentWeather.setTimeZone(timezone);

//        Log.d(TAG, currentWeather.getFormattedTime());


        myContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                mStaggeredLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
                mRecyclerView.setLayoutManager(mStaggeredLayoutManager);
                mRecyclerView.setHasFixedSize(true);
                mAdapter = new TravelListAdapter(getActivity(), mCurrentWeather);
                mRecyclerView.setAdapter(mAdapter);
//                mAdapter.setOnItemClickListener(onItemClickListener);

                mAdapter.setOnItemClickListener(new TravelListAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent transitionIntent = new Intent(view.getContext(), CouponDetailsActivity.class);
//                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_POSITION, position);
//                        Toast.makeText(myContext, "PP "+mCurrentWeather.get(position).getId(),Toast.LENGTH_LONG).show();
                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_ENTERING_TPYE, 1);
                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_ID, mCurrentWeather.get(position).getId());
//                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_TITLE, mCurrentWeather.get(position).getName());
//                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_SUBTITLE, mCurrentWeather.get(position).getName2());
//                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_IMAGE, mCurrentWeather.get(position).getImageName());
                        myContext.startActivity(transitionIntent);
                    }
                });



                progressBar.setVisibility(View.GONE);
                mSwipeRefreshWidget.setRefreshing(false);

            }

        });


//        return currentWeather;
        return blogCoupons;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)
                myContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if(networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;

        }

        return isAvailable;
    }

    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(myContext.getFragmentManager(), "error_dialog");
    }



    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }*/

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        Toast.makeText(getActivity(),"onActivityCreated()", Toast.LENGTH_SHORT).show();

    }


    @Override
    public void onStart() {
        super.onStart();
//        Toast.makeText(getActivity(),"onStart()", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
//        Toast.makeText(getActivity(),"onResume()", Toast.LENGTH_SHORT).show();
//        setMainRecyclerView();
//        setCallLogs(curLog);

//        onRefresh();
//        mSwipeRefreshWidget.setRefreshing(true);


//        progressBar.setVisibility(View.VISIBLE);



        getForecast();
//        mSwipeRefreshWidget.performClick(); not working


        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    @Override
    public void onPause() {
        super.onPause();
//        Toast.makeText(getActivity(),"onPause()", Toast.LENGTH_SHORT).show();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    @Override
    public void onStop() {
        super.onStop();
//        Toast.makeText(getActivity(),"onStop()", Toast.LENGTH_SHORT).show();
    }



     @Override
     public void onDestroyView() {
        super.onDestroyView();
//         Toast.makeText(getActivity(),"onDestroyView()", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        Toast.makeText(getActivity(),"onDestroy()", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        Toast.makeText(getActivity(),"onDetach()", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onPauseFragment() {
        Log.i(TAG, "onPauseFragment()");
//        Toast.makeText(getActivity(), "onPauseFragment():" + TAG, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onResumeFragment() {
        Log.i(TAG, "onResumeFragment()");
//        Toast.makeText(getActivity(), "onResumeFragment():" + TAG, Toast.LENGTH_SHORT).show();


    }

  /*  @Override
    public void update() {
        Toast.makeText(getActivity(), "update", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(getActivity(), "onBackPressed() from home", Toast.LENGTH_SHORT).show();
    }*/



    /*@Override
    public void onRefresh() {

        mSwipeRefreshWidget.setRefreshing(true);
        getForecast();
    }*/


     /*public void setMainRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        SwipeRecyclerViewAdapter mAdapter = new SwipeRecyclerViewAdapter(applicationContext, mDataSet);
        ((SwipeRecyclerViewAdapter) mAdapter).setMode(Attributes.Mode.Single);
        mRecyclerView.setAdapter(mAdapter);
    }*/


    /*@Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {

            // launch your AsyncTask here, if the task has not been executed yet
           *//* if(aTask.getStatus().equals(AsyncTask.Status.PENDING)) {
                aTask.execute();
            }*//*
            getForecast();
        }
    }*/

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString().replaceAll("\n", "").trim(); // it's not working properly..
    }



    private void stopRefreshing(){

        myContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshWidget.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
            }
        });

    }
}
