package com.majithg.rewardago.home.content;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.majithg.rewardago.R;
import com.majithg.rewardago.util.AlertDialogFragment;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CouponCategoryActivity extends AppCompatActivity {

    private static final String TAG = CouponCategoryActivity.class.getSimpleName();
    public static final String EXTRA_PARAM_CATEGORY = "category";
//    public static final String EXTRA_PARAM_CATEGORY_ICON = "icon";

    private ProgressBar progressBar;
    private SwipeRefreshLayout mSwipeRefreshWidget;

    private TravelListAdapter mAdapter;
    private String jsonData;

    private RecyclerView mRecyclerView;
    private StaggeredGridLayoutManager mStaggeredLayoutManager;
    private List<Place> mCurrentWeather = new ArrayList<>();

    private TextView emptyViewoffers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon_category);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        getSupportActionBar().setTitle(getIntent().getStringExtra(CouponCategoryActivity.EXTRA_PARAM_CATEGORY));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView textView = (TextView) findViewById(R.id.toolbarText);
        String s = getIntent().getStringExtra(CouponCategoryActivity.EXTRA_PARAM_CATEGORY).replaceAll("_"," ");
        textView.setText(s);

//        getSupportActionBar().setIcon(getIntent().getIntExtra(CouponCategoryActivity.EXTRA_PARAM_CATEGORY_ICON, 0));
//        ImageView imageView = (ImageView) findViewById(R.id.toolbarIcon);
//        imageView.setImageResource(getIntent().getIntExtra(CouponCategoryActivity.EXTRA_PARAM_CATEGORY_ICON, 0));

//        toolbar.setNavigationIcon(R.drawable.back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);

        mSwipeRefreshWidget = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_widget);
        mSwipeRefreshWidget.setColorScheme(R.color.colorPrimaryDark, R.color.colorPrimaryDark, R.color.colorPrimaryDark, R.color.colorPrimaryDark);

        mRecyclerView = (RecyclerView) findViewById(R.id.list);
        emptyViewoffers = (TextView) findViewById(R.id.empty_coupon_cate);

        getForecast();

        mSwipeRefreshWidget.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshWidget.setRefreshing(true);
                getForecast();
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getForecast();
    }

    private void getForecast() {

        String forecastUrl = "http://rewardago.com/cron/json.php?cat="
                +getIntent().getStringExtra(CouponCategoryActivity.EXTRA_PARAM_CATEGORY)
                ;
//        Toast.makeText(this, forecastUrl, Toast.LENGTH_SHORT).show();
//        String forecastUrl = "https://api.myjson.com/bins/4lrjl";
//        String forecastUrl = "http://majithg.0fees.us/loops/"; // not working... so confirm... it's depending on free hosting

        if (isNetworkAvailable()){

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            toggleRefresh();
                            progressBar.setVisibility(View.GONE);
                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    });

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {
                    /*runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toggleRefresh();
                        }
                    });*/
                    try {
//                        String jsonData = response.body().string();
                        jsonData = stripHtml(response.body().string());

                        /*runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), jsonData, Toast.LENGTH_LONG).show();
                            }
                        });*/

                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {
                            mCurrentWeather = getCurrentDetails(jsonData);
                           /* myContext.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
//                                    updateDisplay();
                                    progressBar.setVisibility(View.GONE);
                                    mSwipeRefreshWidget.setRefreshing(false);
                                }
                            });*/

                        } else {
                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        }

        else {
            Toast.makeText(getApplicationContext(), getString(R.string.network_unavailable_message), Toast.LENGTH_LONG).show();

        }

    }


    private List<Place> getCurrentDetails(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);

        JSONArray currently  = forecast.getJSONArray("Coupon");

        List<Place> blogCoupons = new ArrayList<>();

        for (int i = 0; i < currently.length(); i++) {
            Place currentWeather = new Place();
            JSONObject post = currently.getJSONObject(i);
            currentWeather.setId(post.getInt("id"));
            currentWeather.setName(post.getString("title"));
            currentWeather.setName2(post.getString("Sub_Title"));
            currentWeather.setImageName(post.getString("image"));
            currentWeather.setImageName2(post.getString("Store_logo"));
            blogCoupons.add(currentWeather);
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (mCurrentWeather.isEmpty()) {
                    mRecyclerView.setVisibility(View.GONE);
                    emptyViewoffers.setVisibility(View.VISIBLE);
                }
                else {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    emptyViewoffers.setVisibility(View.GONE);
                }

                mStaggeredLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
                mRecyclerView.setLayoutManager(mStaggeredLayoutManager);
                mRecyclerView.setHasFixedSize(true);
                mAdapter = new TravelListAdapter(getApplicationContext(), mCurrentWeather);
                mRecyclerView.setAdapter(mAdapter);
//                mAdapter.setOnItemClickListener(onItemClickListener); // or use directly...

                mAdapter.setOnItemClickListener(new TravelListAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent transitionIntent = new Intent(view.getContext(), CouponDetailsActivity.class);
                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_ENTERING_TPYE, 1);
                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_ID, mCurrentWeather.get(position).getId());
                        startActivity(transitionIntent);
                    }
                });


                progressBar.setVisibility(View.GONE);
                mSwipeRefreshWidget.setRefreshing(false);

            }

        });

        return blogCoupons;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if(networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;

        }

        return isAvailable;
    }

    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(getFragmentManager(), "error_dialog");
    }

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
