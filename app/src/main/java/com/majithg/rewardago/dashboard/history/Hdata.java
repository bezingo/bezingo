package com.majithg.rewardago.dashboard.history;


public class Hdata {
    public String historyTitle;
    public String claimedDate;
    public String RedeempedDate;
    public String outletName;


    public Hdata() {
    }

    public Hdata(String historyTitle, String claimedDate, String redeempedDate, String outletName) {
        this.historyTitle = historyTitle;
        this.claimedDate = claimedDate;
        RedeempedDate = redeempedDate;
        this.outletName = outletName;
    }

    public String getHistoryTitle() {
        return historyTitle;
    }

    public void setHistoryTitle(String historyTitle) {
        this.historyTitle = historyTitle;
    }

    public String getClaimedDate() {
        return claimedDate;
    }

    public void setClaimedDate(String claimedDate) {
        this.claimedDate = claimedDate;
    }

    public String getRedeempedDate() {
        return RedeempedDate;
    }

    public void setRedeempedDate(String redeempedDate) {
        RedeempedDate = redeempedDate;
    }

    public String getOutletName() {
        return outletName;
    }

    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }
}

