package com.majithg.rewardago.dashboard.history;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.majithg.rewardago.R;

import java.util.Collections;
import java.util.List;


public class HRVA extends RecyclerView.Adapter<HVH> {

    List<Hdata> list = Collections.emptyList();
    Context context;

    public HRVA(List<Hdata> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public HVH onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.redemption_row, parent, false);
        HVH hvh = new HVH(v);
        return hvh;
    }

    @Override
    public void onBindViewHolder(HVH holder, int position) {

        holder.outletName.setText(list.get(position).outletName);
        holder.historyTitle.setText(list.get(position).historyTitle);
        holder.claimedDate.setText(list.get(position).claimedDate);
        holder.RedeempedDate.setText(list.get(position).RedeempedDate);


//        animate(holder);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView
    public void insert(int position, Hdata data) {
        list.add(position, data);
        notifyItemInserted(position);
    }
    // Remove a RecyclerView item containing the Data object
    public void remove(Hdata data) {
        int position = list.indexOf(data);
        list.remove(position);
        notifyItemRemoved(position);
    }

    /*public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.anticipate_overshoot_interpolator);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }*/

}
