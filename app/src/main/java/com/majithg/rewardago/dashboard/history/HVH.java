package com.majithg.rewardago.dashboard.history;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.majithg.rewardago.R;


//The adapters View Holder
public class HVH extends RecyclerView.ViewHolder {

    public TextView historyTitle;
    public TextView claimedDate;
    public TextView RedeempedDate;
    public TextView outletName;


    public TextView claimed;
    public TextView redeemed;
    public TextView outlet;



    HVH(View itemView) {
        super(itemView);

        historyTitle = (TextView) itemView.findViewById(R.id.historyTitle);
        claimedDate = (TextView) itemView.findViewById(R.id.claimedDate);
        RedeempedDate = (TextView) itemView.findViewById(R.id.RedeempedDate);
        outletName = (TextView) itemView.findViewById(R.id.outletName);

        claimed = (TextView) itemView.findViewById(R.id.claimed);
        redeemed = (TextView) itemView.findViewById(R.id.redeemed);
        outlet = (TextView) itemView.findViewById(R.id.outlet);

    }

}
