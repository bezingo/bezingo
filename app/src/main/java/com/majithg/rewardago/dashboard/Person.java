package com.majithg.rewardago.dashboard;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDTintHelper;
import com.afollestad.materialdialogs.internal.ThemeSingleton;
import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.majithg.rewardago.activity.LoginActivity;
import com.majithg.rewardago.dashboard.history.RedemptiomHistoryActivity;
import com.majithg.rewardago.dashboard.redemption.MyRedemption;
import com.majithg.rewardago.home.Home;
import com.majithg.rewardago.mycoupons.Offers;
import com.majithg.rewardago.outlets.outlets;
import com.majithg.rewardago.util.AlertDialogFragment;
import com.majithg.rewardago.util.App;
import com.majithg.rewardago.util.CircleImageView;
import com.majithg.rewardago.util.FragmentLifecycle;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Random;


public class Person extends Fragment implements FragmentLifecycle {
    private static final String ARG_PARAM1 = "param1";
    private static final int limit = 50;
    private static final String TAG = Person.class.getSimpleName();

    private int messageType;

    private Button unsubs,redemption,logout,editatprofile;

    public static TextView textMyActiveCoup, textMyRedemp, totalAmt, textName,textNumber,textEaddress;


    private String jsonData;

    private int ints1;
    private int ints2;
    private String s3;
    private String s4;

    private String mCurrentWeather;
    private String currentWeather;

    private FragmentActivity myContext;
    public static PersonViewPager vp_message;
    private PersonPagerAdapter fAdapter;
    private List<Fragment> list_fragment;


    private TextView userid;
    public static TextView fName;
    public static TextView lName;
    public static TextView ContactName;
    public static TextView txtEmail;

//    public static boolean canAddItem = false;


  public static CircleImageView profile_image_person;

   public static String str2 = "Save Profile";
    public static String str1 = "Edit Profiles";
    private EditText passwordInput;
    private View positiveAction;
    private RadioButton rButton;
    private RadioGroup rGroup;
    private ProgressDialog progressDialog;
    private String jsonSubs;
    private String status;
    /*private String[] items = {
            "I am not satisfied with the app",
            "It is not easy to use",
            "It is not helpful for me",
            "I did not find what I wanted",
            "Other"
    };*/

    private Animator mCurrentAnimator;
    private int mShortAnimationDuration;

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }

    public Person() {
    }

    public static Person newInstance(int param1) {
        Person fragment = new Person();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            messageType = getArguments().getInt(ARG_PARAM1);
        } else {
            messageType = 0;
        }

        setHasOptionsMenu(true);
        getForecast();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

//        View root = inflater.inflate(R.layout.person, container, false);
        View root = inflater.inflate(R.layout.person_profile2, container, false);

        profile_image_person = (CircleImageView) root.findViewById(R.id.profile_image_person);
        fName = (TextView) root.findViewById(R.id.fName);
        lName = (TextView) root.findViewById(R.id.lName);
        ContactName = (TextView) root.findViewById(R.id.ContactName);
        txtEmail = (TextView) root.findViewById(R.id.txtEmail);

        profile_image_person.setImageDrawable(App.pImage);
        profile_image_person.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                loadPhoto(profile_image_person, 200,200);
//                zoomImageFromThumb(profile_image_person, R.drawable.placeholder);

//                Bitmap bmap=((BitmapDrawable)profile_image_person.getDrawable()).getBitmap(); // will work
//                profile_image_person.buildDrawingCache();
//                Bitmap bmap = profile_image_person.getDrawingCache();
//                saveImage(bmap);

                /*Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                Uri imgUri = Uri.parse("file://" + saveImage(bmap));
                intent.setDataAndType(imgUri, "image");
                startActivity(intent);*/
            }
        });

        // Fetching user details from SQLite
        HashMap<String, String> user = MainActivity.db.getUserDetails();

        String uid = user.get("uid");
        String name = user.get("fname");
        String name2 = user.get("lname");
        String contact = user.get("contact");
        String email = user.get("email");

//        userid.setText(uid);
        fName.setText(name);
        lName.setText(name2);
        ContactName.setText(contact);
        txtEmail.setText(email);

       /* Picasso.with(myContext).load(App.profilePic)
                .error(R.drawable.placeholder2)
                .placeholder(R.drawable.placeholder2)
                .into(App.profile_image_person);*/



        unsubs = (Button) root.findViewById(R.id.unsubs);
        unsubs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                myContext.startActivity(new Intent(getActivity(), ProfileActivity.class));
//                Toast.makeText(myContext, "Edit Your Profle", Toast.LENGTH_SHORT).show();
showWarningDialog();
            }
        });

        editatprofile = (Button) root.findViewById(R.id.editatprofile);
        editatprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myContext.startActivity(new Intent(getActivity(), ProfileActivity.class));
//                Toast.makeText(myContext, "Edit Your Profle", Toast.LENGTH_SHORT).show();
            }
        });

        textMyActiveCoup = (TextView) root.findViewById(R.id.textMyActiveCoup);
        textMyRedemp = (TextView) root.findViewById(R.id.textMyRedemp);
        totalAmt = (TextView) root.findViewById(R.id.totalAmt);

        textMyActiveCoup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                MainActivity.toolbar.getMenu().clear();
//                MainActivity.toolbar.getMenu().add(0, 1, Menu.NONE, null).setIcon(R.drawable.ic_arrow_24dp).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                Offers.canAddItem = true;
                App.vp_message_main.setCurrentItem(3);
                MainActivity.bottomNavigation.setCurrentItem(3);
//                App.adapter_offers.notifyDataSetChanged();
//                Toast.makeText(myContext, "My Active Coupons", Toast.LENGTH_SHORT).show();

            }
        });
        textMyRedemp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                MainActivity.toolbar.getMenu().clear();
//                Person.vp_message.setCurrentItem(1);
//                App.h_adapter.notifyDataSetChanged();]

                /// TODO plz create and connect with

                MainActivity.toolbar.getMenu().clear();
                App.personCanAddItem = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        MyRedemption fone = MyRedemption.newInstance(20);
                        FragmentManager manager = getFragmentManager();
                        FragmentTransaction transaction = manager.beginTransaction();
                        transaction.replace(R.id.insideP, fone, "MY_REDEMPTION");
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                }, 50);



//                Toast.makeText(myContext, "My Redemption", Toast.LENGTH_SHORT).show();
            }
        });
        totalAmt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                MainActivity.toolbar.getMenu().clear();
                Toast.makeText(myContext, "Total Amount", Toast.LENGTH_SHORT).show();
            }
        });



        redemption = (Button) root.findViewById(R.id.redemptionHistory);
        logout = (Button) root.findViewById(R.id.logout);

        redemption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                MainActivity.toolbar.getMenu().clear();
//                Person.vp_message.setCurrentItem(1);
//                App.h_adapter.notifyDataSetChanged();


                myContext.startActivity(new Intent(getActivity(), RedemptiomHistoryActivity.class));
//                Toast.makeText(myContext, "Redemption History", Toast.LENGTH_SHORT).show();

            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutUser();
            }
        });


        return root;
    }


    public void saveImage(Bitmap finalBitmap) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/rewardago_images");
        myDir.mkdirs();
//        Random generator = new Random();
//        int n = 10000;
//        n = generator.nextInt(n);
//        String fname = "Image-"+ n +".jpg";
        String fname = "Image.jpg";
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

//        Toast.makeText(myContext, "file path: "+myDir.toString(), Toast.LENGTH_SHORT).show();

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
//        Uri imgUri = Uri.parse("file://" + "/storage/emulated/0/rewardago_images/Image.jpg");
        Uri imgUri = Uri.parse("file://" +myDir+fname);
        intent.setDataAndType(imgUri, "image");
        startActivity(intent);
    }



   /* private void zoomImageFromThumb(final View thumbView, int imageResId) {
        // If there's an animation in progress, cancel it immediately and proceed with this one.
        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }

        // Load the high-resolution "zoomed-in" image.
        final ImageView expandedImageView = (ImageView) myContext.findViewById(R.id.expanded_image);
        expandedImageView.setImageResource(imageResId);

        // Calculate the starting and ending bounds for the zoomed-in image. This step
        // involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        // The start bounds are the global visible rectangle of the thumbnail, and the
        // final bounds are the global visible rectangle of the container view. Also
        // set the container view's offset as the origin for the bounds, since that's
        // the origin for the positioning animation properties (X, Y).
        thumbView.getGlobalVisibleRect(startBounds);
        myContext.findViewById(R.id.insideP).getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final bounds using the
        // "center crop" technique. This prevents undesirable stretching during the animation.
        // Also calculate the start scaling factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        // Hide the thumbnail and show the zoomed-in view. When the animation begins,
        // it will position the zoomed-in view in the place of the thumbnail.
        thumbView.setAlpha(0f);
        expandedImageView.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations to the top-left corner of
        // the zoomed-in view (the default is the center of the view).
        expandedImageView.setPivotX(0f);
        expandedImageView.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and scale properties
        // (X, Y, SCALE_X, and SCALE_Y).
        AnimatorSet set = new AnimatorSet();
        set
                .play(ObjectAnimator.ofFloat(expandedImageView, View.X, startBounds.left,
                        finalBounds.left))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.Y, startBounds.top,
                        finalBounds.top))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X, startScale, 1f))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_Y, startScale, 1f));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCurrentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mCurrentAnimator = null;
            }
        });
        set.start();
        mCurrentAnimator = set;

        // Upon clicking the zoomed-in image, it should zoom back down to the original bounds
        // and show the thumbnail instead of the expanded image.
        final float startScaleFinal = startScale;
        expandedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentAnimator != null) {
                    mCurrentAnimator.cancel();
                }

                // Animate the four positioning/sizing properties in parallel, back to their
                // original values.
                AnimatorSet set = new AnimatorSet();
                set
                        .play(ObjectAnimator.ofFloat(expandedImageView, View.X, startBounds.left))
                        .with(ObjectAnimator.ofFloat(expandedImageView, View.Y, startBounds.top))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView, View.SCALE_X, startScaleFinal))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView, View.SCALE_Y, startScaleFinal));
                set.setDuration(mShortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }
                });
                set.start();
                mCurrentAnimator = set;
            }
        });
    }*/

  /*  private void loadPhoto(ImageView imageView, int width, int height) {

        ImageView tempImageView = imageView;


        AlertDialog.Builder imageDialog = new AlertDialog.Builder(myContext);
        LayoutInflater inflater = (LayoutInflater) myContext.getSystemService(myContext.LAYOUT_INFLATER_SERVICE);

        View layout = inflater.inflate(R.layout.custom_fullimage_dialog, (ViewGroup) myContext.findViewById(R.id.layout_root));
//        (ViewGroup) findViewById(R.id.layout_root));
        myContext.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        myContext.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView image = (ImageView) layout.findViewById(R.id.fullimage);
        image.setImageDrawable(tempImageView.getDrawable());
        imageDialog.setView(layout);
        *//*imageDialog.setPositiveButton("OK", new DialogInterface.OnClickListener(){

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }

        });*//*


        imageDialog.create();
        imageDialog.show();
    }*/

    private void logoutUser() {
        MainActivity.session.setLogin(false);
        MainActivity.db.deleteUsers();
        Intent intent = new Intent(myContext, LoginActivity.class);
        startActivity(intent);
        myContext.finish();
        Toast.makeText(myContext, "Successfully Logout", Toast.LENGTH_SHORT).show();
    }


    private void showWarningDialog() {
        MaterialDialog dialog = new MaterialDialog.Builder(myContext)
                .iconRes(R.mipmap.ic_launcher)
                .limitIconToDefaultSize()
                .title("Un-subscribe")
                /*.items(items)
                .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
//                        if(which==-1){
                            Toast.makeText(myContext, which+" "+text.toString(), Toast.LENGTH_SHORT).show();
//                        }else {
//                            mChoice = text.toString().replaceAll(" ","_");
//                            Toast.makeText(myContext,mChoice , Toast.LENGTH_SHORT).show();
//                            if(which==0){
//                                getForecast3(776);
//                            }else if(which==1){
//                                getForecast3(932);
//                            }else if(which==2){
//                                getForecast3(780);
//                            }
//                        }
                        return true;
                    }
                })*/
                .customView(R.layout.dialog_customview, true)
                .positiveText("OK")
                .negativeText("CANCEL")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        rButton = (RadioButton) dialog.getCustomView().findViewById(rGroup.getCheckedRadioButtonId());
//                        Toast.makeText(myContext, rButton.getTag().toString(), Toast.LENGTH_SHORT).show();
//                        Toast.makeText(myContext, passwordInput.getText().toString().trim(), Toast.LENGTH_SHORT).show();

                        removeSubscription(rButton.getTag().toString(),
                                passwordInput.getText().toString().trim() );

                    }
                }).build();

        positiveAction = dialog.getActionButton(DialogAction.POSITIVE);


        rGroup = (RadioGroup) dialog.getCustomView().findViewById(R.id.rb);

        
        //noinspection ConstantConditions
        passwordInput = (EditText) dialog.getCustomView().findViewById(R.id.password);
        passwordInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                positiveAction.setEnabled(s.toString().trim().length() > 8);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

/*
        // Toggling the show password CheckBox will mask or unmask the password input EditText
        CheckBox checkbox = (CheckBox) dialog.getCustomView().findViewById(R.id.showPassword);
        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                passwordInput.setInputType(!isChecked ? InputType.TYPE_TEXT_VARIATION_PASSWORD : InputType.TYPE_CLASS_TEXT);
                passwordInput.setTransformationMethod(!isChecked ? PasswordTransformationMethod.getInstance() : null);
            }
        });

        int widgetColor = ThemeSingleton.get().widgetColor;
        MDTintHelper.setTint(checkbox,widgetColor == 0 ? ContextCompat.getColor(myContext, R.color.colorAccent) : widgetColor);
        MDTintHelper.setTint(passwordInput,widgetColor == 0 ? ContextCompat.getColor(myContext, R.color.colorAccent) : widgetColor);
*/

        dialog.show();
        positiveAction.setEnabled(false); // disabled by default
    }


    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.fragment_menu, menu);
        if(App.personCanAddItem) {
            MainActivity.toolbar.getMenu().clear();
            MainActivity.toolbar.getMenu().add(0, 10, Menu.NONE, null).setIcon(R.drawable.ic_arrow_24dp).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        }else {
            MainActivity.toolbar.getMenu().clear();
            MainActivity.toolbar.getMenu().add(0, 5, Menu.NONE, null).setIcon(R.drawable.ic_arrow_24dp).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

            if (id == 5) {
                MainActivity.toolbar.getMenu().clear();
                MainActivity.bottomNavigation.setCurrentItem(0);
                App.vp_message_main.setCurrentItem(0);
                App.HomeCanAddItem = false;
                return true;
            }

        return super.onOptionsItemSelected(item);
    }

    private void removeFM(){
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }





    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
//        setMainRecyclerView();
//        setCallLogs(curLog);
        getForecast();
        profile_image_person.setImageDrawable(App.pImage);
    }


    @Override
    public void onPauseFragment() {
        Log.i(TAG, "onPauseFragment()");
        // TODO very importants
//        Toast.makeText(getActivity(), "onPauseFragment():" + TAG, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResumeFragment() {
        Log.i(TAG, "onResumeFragment()");
        // TODO very importants
//        Toast.makeText(getActivity(), "onResumeFragment():" + TAG, Toast.LENGTH_SHORT).show();

    }

    private void removeSubscription(String reason, String remarks) {

        String forecastUrl = "http://rewardago.com/cron/subscription.php?action=uns&id="
                +MainActivity.UID+"&r="+reason+"&rm="+remarks;
//                +MainActivity.UID+"?_=" + System.currentTimeMillis();

        progressDialog = new ProgressDialog(myContext);
        progressDialog.setMessage("removing subscription");
        progressDialog.setCancelable(false);
        progressDialog.show();

        if (isNetworkAvailable()){

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    /*myContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            toggleRefresh();
//                            progressBar.setVisibility(View.GONE);
//                            mSwipeRefreshWidget.setRefreshing(false);

                        }
                    });*/
                    progressDialog.dismiss();
                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
//                        String jsonData = response.body().string();
//                        jsonData = response.body().string();
                        jsonSubs = stripHtml(response.body().string());




                       /* myContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(myContext, jsonSubs,Toast.LENGTH_LONG).show();
                            }
                        });*/


//                        Log.v(TAG, jsonSubs);
                        if (response.isSuccessful()) {

                            removingSubscription(jsonSubs);





                        } else {
                            progressDialog.dismiss();
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        }

        else {
            progressDialog.dismiss();
            Toast.makeText(myContext, getString(R.string.network_unavailable_message), Toast.LENGTH_LONG).show();
        }

    }

    private void removingSubscription(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);
        JSONArray jsonArray = forecast.getJSONArray("Uns");
        JSONObject currently = jsonArray.getJSONObject(0);

       status = currently.getString("Status");

        myContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                progressDialog.dismiss();
//                Toast.makeText(myContext, status, Toast.LENGTH_SHORT).show();
                if(status.equals("Success")){
                    logoutUser();
//                    Toast.makeText(myContext, "success", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(myContext, "not success", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    private void getForecast() {

        String forecastUrl = "http://rewardago.com/cron/profile.php?id="
                +MainActivity.UID+"?_=" + System.currentTimeMillis();

        if (isNetworkAvailable()){

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    myContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            toggleRefresh();
//                            progressBar.setVisibility(View.GONE);
//                            mSwipeRefreshWidget.setRefreshing(false);

                        }
                    });

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
//                        String jsonData = response.body().string();
//                        jsonData = response.body().string();
                        jsonData = stripHtml(response.body().string());




                        myContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//                                Toast.makeText(myContext, jsonData,Toast.LENGTH_LONG).show();
                            }
                        });


                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {

                            mCurrentWeather = getCurrentDetails(jsonData);





                        } else {
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        }

        else {
            Toast.makeText(myContext, getString(R.string.network_unavailable_message), Toast.LENGTH_LONG).show();
        }

    }

    private String getCurrentDetails(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);

        JSONObject currently = forecast.getJSONObject("Profile");

       ints1 = currently.getInt("Active_Coupons");
        ints2 = currently.getInt("My_Redemptions");
        s3 = currently.getString("Savings");
        s4 = currently.getString("Image");
        App.profilePic = s4;

        myContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                textMyActiveCoup.setText(""+ints1);
                textMyRedemp.setText(""+ints2);
                totalAmt.setText(s3);
                /*Picasso.with(myContext).load(App.profilePic)
                        .error(R.drawable.placeholder2)
                        .placeholder(R.drawable.placeholder2)
                        .into(App.profile_image_person);*/
//                MainActivity.setImg();

            }

        });

        return currentWeather;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)
                myContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;
        if(networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;
        }
        return isAvailable;
    }
    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(myContext.getFragmentManager(), "error_dialog");
    }
    public String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }


}
