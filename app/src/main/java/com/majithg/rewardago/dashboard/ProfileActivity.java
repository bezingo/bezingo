package com.majithg.rewardago.dashboard;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.majithg.rewardago.RuntimePermissionsActivity;
import com.majithg.rewardago.helper.SQLiteHandler;
import com.majithg.rewardago.util.AlertDialogFragment;
import com.majithg.rewardago.util.App;
import com.majithg.rewardago.util.CircleImageView;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class ProfileActivity extends RuntimePermissionsActivity {

    private static final String TAG = ProfileActivity.class.getSimpleName();
    Button saveProfile;
    CircleImageView profile_image_edit;
    private TextView fName;
    private TextView lName;
    private TextView mNumber;
    private TextView mEaddress;
    private EditText firstName;
    private EditText lastName;
    private EditText editNumber;
    private EditText editEaddress;
    private EditText editPassword;
    private ProgressDialog pDialog;
    private SQLiteHandler db;
    private String jsonData;
    private String mCurrentWeather;
    private String currentWeather;
    private String currentWeather2;
    private String currentWeather3;
    private String currentWeather4;
    private String currentWeather5;
    private String uid;
    private String created_at;
    private Button buttonChoose;
    private Button buttonUpload;

//    private ImageView imageView;

    private EditText editTextName;

    private Bitmap bitmap;

    private int PICK_IMAGE_REQUEST = 1;

    private String UPLOAD_URL = "http://rewardago.com/profilepics/upload.php?id=" + MainActivity.UID;

    private String KEY_IMAGE = "image";
    private String KEY_NAME = "filename";
    private String ee;



    private static final int REQUEST_PERMISSIONS = 20;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_24dp);
        getSupportActionBar().setHomeButtonEnabled(true);

        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        profile_image_edit = (CircleImageView) findViewById(R.id.profile_image_edit);
        profile_image_edit.setImageDrawable(App.pImage);

        profile_image_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileActivity.super.requestAppPermissions(
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        R.string.runtime_permissions_txt
                        , REQUEST_PERMISSIONS);
//                showFileChooser();
            }
        });

        /*Picasso.with(getApplicationContext()).load(App.profilePic)
                .error(R.drawable.placeholder2)
                .placeholder(R.drawable.placeholder2)
                .into(App.profile_image_edit);*/

        fName = (TextView) findViewById(R.id.fName);
        lName = (TextView) findViewById(R.id.lName);
        mNumber = (TextView) findViewById(R.id.mNumber);
        mEaddress = (TextView) findViewById(R.id.mEaddress);

        fName.setText(MainActivity.lName.getText().toString());
        lName.setText(MainActivity.fName.getText().toString());
        mEaddress.setText(MainActivity.txtEmail.getText().toString());
        mNumber.setText(MainActivity.ContactName.getText().toString());


        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        editNumber = (EditText) findViewById(R.id.editNumber);
        editEaddress = (EditText) findViewById(R.id.editEaddress);
        editPassword = (EditText) findViewById(R.id.editPassword);

        saveProfile = (Button) findViewById(R.id.saveProfile);
        saveProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!validateForm()) {
                    return;
                }

                pDialog.setMessage("Updating...");
                showDialog();

                // Fetching user details from SQLite
                HashMap<String, String> user = db.getUserDetails();
                uid = user.get("uid");
                created_at = user.get("created_at");

                String name = firstName.getText().toString().trim();
                String name2 = lastName.getText().toString().trim();
                String contact = editNumber.getText().toString().trim();
                String email = editEaddress.getText().toString().trim();
                String password = editPassword.getText().toString().trim();

                if (!name.isEmpty() && !name2.isEmpty() && !contact.isEmpty() && !email.isEmpty() && !password.isEmpty()) {


                    updatingUserProfile(uid, name, name2, email, contact, password);

                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter your details!", Toast.LENGTH_LONG)
                            .show();
                }

            }
        });






        // Toggling the show password CheckBox will mask or unmask the password input EditText
        CheckBox checkbox = (CheckBox) findViewById(R.id.showPassword);
        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editPassword.setInputType(!isChecked ? InputType.TYPE_TEXT_VARIATION_PASSWORD : InputType.TYPE_CLASS_TEXT);
                editPassword.setTransformationMethod(!isChecked ? PasswordTransformationMethod.getInstance() : null);
            }
        });
    }

    @Override
    public void onPermissionsGranted(int requestCode) {
//        Toast.makeText(this, "Permissions Received.", Toast.LENGTH_LONG).show();
        showFileChooser();
    }


    private void updatingUserProfile(String uid, String name, String name2, String email, String contact, String password) {

        String forecastUrl = "http://rewardago.com/cron/update_profile.php?id=" + uid
                + "&fname=" + name
                + "&lname=" + name2
                + "&mail=" + email
                + "&cons=" + contact
                + "&pass=" + password;

        if (isNetworkAvailable()) {

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            toggleRefresh();
//                            progressBar.setVisibility(View.GONE);
//                            mSwipeRefreshWidget.setRefreshing(false);
                            hideDialog();
                        }
                    });

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
//                        String jsonData = response.body().string();
                        jsonData = stripHtml(response.body().string());




                        /*runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ProfileActivity.this, jsonData,Toast.LENGTH_LONG).show();
                            }
                        });*/


                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {

                            mCurrentWeather = getCurrentDetails(jsonData);


                        } else {
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                            hideDialog();
                        }
                    } catch (IOException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    } catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        } else {
            Toast.makeText(this, getString(R.string.network_unavailable_message), Toast.LENGTH_LONG).show();

        }

    }

    private String getCurrentDetails(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);

        JSONArray currently = forecast.getJSONArray("Login");

        JSONObject post = currently.getJSONObject(0);

//            currentWeather.setId(post.getInt("offer_id"));
        currentWeather = post.getString("fname");
        currentWeather2 = post.getString("lname");
        currentWeather3 = post.getString("user_email");
        currentWeather4 = post.getString("user_contact");
        currentWeather5 = post.getString("password");


        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                                    updateDisplay();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms


//                        progressBar.setVisibility(View.GONE);
//                        mSwipeRefreshWidget.setRefreshing(false);
                        hideDialog();

                    }
                }, 3000);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                    }
                }, 4000);


                /*if (mCurrentWeather.isEmpty()) {
                    recyclerView.setVisibility(View.GONE);
                    emptyView.setVisibility(View.VISIBLE);
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    emptyView.setVisibility(View.GONE);
                }*/

               /* recyclerView.setLayoutManager(new LinearLayoutManager(RedemptiomHistoryActivity.this));
                h_adapter = new HRVA(mCurrentWeather, getApplicationContext());
//                recyclerView.setAdapter(App.h_adapter);
                recyclerView.setAdapter(h_adapter);
                recyclerView.addItemDecoration(new MyItemDecoration());*/


                fName.setText(currentWeather);
                lName.setText(currentWeather2);
                mEaddress.setText(currentWeather3);
                mNumber.setText(currentWeather4);

                Person.lName.setText(currentWeather);
                Person.fName.setText(currentWeather2);
                Person.ContactName.setText(currentWeather4);
                Person.txtEmail.setText(currentWeather3);

                MainActivity.lName.setText(currentWeather);
                MainActivity.fName.setText(currentWeather2);
                MainActivity.ContactName.setText(currentWeather4);
                MainActivity.txtEmail.setText(currentWeather3);

//                RedemptiomHistoryActivity.fName.setText(currentWeather);
//                RedemptiomHistoryActivity.lName.setText(currentWeather2);
//                RedemptiomHistoryActivity.ContactName.setText(currentWeather4);
//                RedemptiomHistoryActivity.txtEmail.setText(currentWeather3);

                db.deleteUsers();

                // Inserting row in users table
                db.addUser(currentWeather, currentWeather2, currentWeather4, currentWeather3, uid, created_at);

//                App.adapter_offers.notifyDataSetChanged();

            }

        });


        return "successfully updated";
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if (networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;

        }

        return isAvailable;
    }

    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(getFragmentManager(), "error_dialog");
    }

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private boolean validateForm() {
        boolean valid = true;

        String mfname = firstName.getText().toString();
        String mlname = lastName.getText().toString();
        String email = editEaddress.getText().toString();
        String number = editNumber.getText().toString();
        String password = editPassword.getText().toString();

        if (mfname.isEmpty() || mfname.length() < 3) {
            firstName.setError("at least 3 characters");
            valid = false;
        } else {
            firstName.setError(null);
        }

        if (mlname.isEmpty() || mlname.length() < 3) {
            lastName.setError("at least 3 characters");
            valid = false;
        } else {
            lastName.setError(null);
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editEaddress.setError("enter a valid email address");
            valid = false;
        } else {
            editEaddress.setError(null);
        }


        if (number.isEmpty() || number.length() < 10 || number.length() > 10) {
            editNumber.setError("enter a valid phone number (10 digits)");
            valid = false;
        } else {
            editNumber.setError(null);
        }


        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            editPassword.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            editPassword.setError(null);
        }

        return valid;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                //Setting the Bitmap to ImageView
//                imageView.setImageBitmap(bitmap);
                profile_image_edit.setImageBitmap(bitmap);

                /*Picasso.with(getApplicationContext()).load(App.profilePic)
                        .error(R.drawable.placeholder2)
                        .placeholder(R.drawable.placeholder2)
                        .into(profile_image_edit);*/
                uploadImage();


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void uploadImage() {
        //Showing the progress dialog
        final ProgressDialog loading = ProgressDialog.show(this, "Uploading...", "Please wait...", false, false);

        StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.POST, UPLOAD_URL,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //Disimissing the progress dialog
                                loading.dismiss();
                                //Showing toast message of the response
//                        Toast.makeText(ProfileActivity.this, s , Toast.LENGTH_SHORT).show();

                                App.profile_image_main.setImageBitmap(bitmap);
                                App.pImage = App.profile_image_main.getDrawable();
                            }
                        });

                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        ee = volleyError.getMessage();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //Dismissing the progress dialog
                                loading.dismiss();
                                //Showing toast
                                if(ee != ee) {
                                    Toast.makeText(ProfileActivity.this, "onErrorResponse(): " + ee, Toast.LENGTH_LONG).show();
                                }else {
                                    Toast.makeText(ProfileActivity.this, "uploaded", Toast.LENGTH_LONG).show();
                                }
                            }
                        });

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                String image = getStringImage(bitmap);

                //Getting Image Name
//                String name = editTextName.getText().toString().trim();
//                String name = MainActivity.UID.toString().trim();

                //Creating parameters
                Map<String, String> params = new Hashtable<String, String>();

                //Adding parameters
                params.put(KEY_IMAGE, image);
                params.put(KEY_NAME, MainActivity.UID);

                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }


}
