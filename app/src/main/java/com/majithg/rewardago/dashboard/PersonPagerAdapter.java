package com.majithg.rewardago.dashboard;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

public class PersonPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> list_fragment;
    private List<String> list_Title;


    public PersonPagerAdapter(FragmentManager fm, List<Fragment> list_fragment, List<String> list_Title) {
        super(fm);
        this.list_fragment = list_fragment;
        this.list_Title = list_Title;
    }

    public PersonPagerAdapter(FragmentManager fm, List<Fragment> list_fragment) {
        super(fm);
        this.list_fragment = list_fragment;
        this.list_Title = list_Title;
    }

    @Override
    public Fragment getItem(int position) {

       /* Bundle b = new Bundle();
        b.putString("ARG_PARAM1","param1");
        b.putString("ARG_PARAM2","param2");
        b.putString("ARG_PARAM3","param3");

        list_fragment.get(position).setArguments(b);*/

        return list_fragment.get(position);
    }

    @Override
    public int getCount() {
        return list_fragment.size();
    }


    @Override
    public CharSequence getPageTitle(int position) {
//        return list_Title.get(position % list_Title.size());
        return null;
    }


}
