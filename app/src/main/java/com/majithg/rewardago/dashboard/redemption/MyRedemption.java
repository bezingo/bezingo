package com.majithg.rewardago.dashboard.redemption;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.majithg.rewardago.dashboard.Person;
import com.majithg.rewardago.home.Home;
import com.majithg.rewardago.home.content.CouponDetailsActivity;
import com.majithg.rewardago.home.content.Place;
import com.majithg.rewardago.mycoupons.ActListAdapter;
import com.majithg.rewardago.util.AlertDialogFragment;
import com.majithg.rewardago.util.App;
import com.majithg.rewardago.util.FragmentLifecycle;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * ZLB
 */
public class MyRedemption extends Fragment implements FragmentLifecycle
//        ,Updateable
{

    private static final String TAG = MyRedemption.class.getSimpleName();
    Context applicationContext = MainActivity.getContextOfApplication();

    private static final String ARG_PARAM1 = "param1";
    private static final int limit = 50;

    private int messageType;

    private RecyclerView mRecyclerView;
    private StaggeredGridLayoutManager mStaggeredLayoutManager;

//    private boolean isListView;



    private List<Place> mCurrentWeather;
//    Place currentWeather;


    private ProgressBar progressBar;
private SwipeRefreshLayout mSwipeRefreshWidget;

    private FragmentActivity myContext;

//    MainViewPagerAdapter mmadapter;
//    private Calendar startTime = Calendar.getInstance();
//    private StoreListAdapter mAdapter;
    private ActListAdapter actListAdapter;

    TextView empty_view_myRed;

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
//        Toast.makeText(getActivity(),"onAttach()", Toast.LENGTH_SHORT).show();

    }


    public MyRedemption() {
    }

    public static MyRedemption newInstance(int param1) {
        MyRedemption fragment = new MyRedemption();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.v(TAG, "In frag's on save instance state ");
//        outState.putSerializable("starttime", startTime);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Toast.makeText(getActivity(),"onCreate()", Toast.LENGTH_SHORT).show();

        if (getArguments() != null) {
            messageType = getArguments().getInt(ARG_PARAM1);
        } else {
            messageType = 0;
        }

        setHasOptionsMenu(true);

       getForecast();


    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

//        Toast.makeText(getActivity(),"onCreateView()", Toast.LENGTH_SHORT).show();

        View root = inflater.inflate(R.layout.my_redemption, container, false);

        progressBar = (ProgressBar) root.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);

        mSwipeRefreshWidget = (SwipeRefreshLayout) root.findViewById(R.id.swipe_refresh_widget);
        mSwipeRefreshWidget.setColorScheme(R.color.colorPrimaryDark, R.color.colorPrimaryDark, R.color.colorPrimaryDark, R.color.colorPrimaryDark);


        mRecyclerView = (RecyclerView) root.findViewById(R.id.redemptionlist);
        empty_view_myRed = (TextView) root.findViewById(R.id.empty_view_myRed);



        mSwipeRefreshWidget.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getForecast();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mSwipeRefreshWidget.setRefreshing(false);
                    }
                }, 2000);

            }
        });

//        MainActivity.toolbar.getMenu().clear();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                mSwipeRefreshWidget.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
            }
        }, 5000);



        return root;
    }




   /* public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.fragment_menu_home, menu);
//        inflater.inflate(R.menu.fragment_menu, menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == 10) {
            App.personCanAddItem = false;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    reemove();
                }
            }, 50);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void reemove(){
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }



    private void getForecast() {

        String forecastUrl = "http://rewardago.com/cron/my_redemption.php?id="
                +MainActivity.UID+"?_=" + System.currentTimeMillis();

        if (isNetworkAvailable()){

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    myContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            toggleRefresh();
                            progressBar.setVisibility(View.GONE);
                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    });

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {
                    /*runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toggleRefresh();
                        }
                    });*/
                    try {
                        String jsonData = response.body().string();
//                        jsonData = stripHtml(response.body().string());

                        /*myContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(myContext, jsonData,Toast.LENGTH_LONG).show();
                            }
                        });*/

//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {
                            mCurrentWeather = getCurrentDetails(jsonData);
                           /* myContext.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
//                                    updateDisplay();
                                    progressBar.setVisibility(View.GONE);
                                    mSwipeRefreshWidget.setRefreshing(false);
                                }
                            });*/

                        } else {
                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        }

        else {
            Toast.makeText(myContext, getString(R.string.network_unavailable_message),Toast.LENGTH_LONG).show();
//            mSwipeRefreshWidget.setRefreshing(false);   // i think.. an error may occur here, if we put
//            progressBar.setVisibility(View.GONE);
        }

    }


    private List<Place> getCurrentDetails(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);

        JSONArray currently  = forecast.getJSONArray("Redemption");

        List<Place> blogCoupons = new ArrayList<>();

        for (int i = 0; i < currently.length(); i++) {

            Place currentWeather = new Place();

            JSONObject post = currently.getJSONObject(i);

//            String title = post.getString("id");
            currentWeather.setId(post.getInt("offer_id"));
            currentWeather.setName(post.getString("title"));
            currentWeather.setName2(post.getString("Sub_Title"));
            currentWeather.setImageName(post.getString("Image"));
            currentWeather.setImageName2(post.getString("store_logo"));

            currentWeather.setQr_code(post.getString("qr_code"));

            blogCoupons.add(currentWeather);
        }


//        Log.d(TAG, currentWeather.getFormattedTime());


        myContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //TODO plz enable later
                /*if (mCurrentWeather.isEmpty()) {
                    mRecyclerView.setVisibility(View.GONE);
                    empty_view_myRed.setVisibility(View.VISIBLE);
                }
                else {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    empty_view_myRed.setVisibility(View.GONE);
                }*/


                mStaggeredLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
                mRecyclerView.setLayoutManager(mStaggeredLayoutManager);
                mRecyclerView.setHasFixedSize(true);
                actListAdapter = new ActListAdapter(getActivity(), mCurrentWeather);
                mRecyclerView.setAdapter(actListAdapter);
//                mAdapter.setOnItemClickListener(onItemClickListener);

                actListAdapter.setOnItemClickListener(new ActListAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent transitionIntent = new Intent(view.getContext(), CouponDetailsActivity.class);
//                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_POSITION, position);
//                        Toast.makeText(myContext, "PP "+mCurrentWeather.get(position).getId(),Toast.LENGTH_LONG).show();
                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_ENTERING_TPYE, 5);
                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_ID, mCurrentWeather.get(position).getId());
//                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_TITLE, mCurrentWeather.get(position).getName());
                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_QR_CODE, mCurrentWeather.get(position).getQr_code());
//                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_SUBTITLE, mCurrentWeather.get(position).getName2());
//                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_IMAGE, mCurrentWeather.get(position).getImageName());
                        myContext.startActivity(transitionIntent);
//                        reemove();
                    }
                });



                progressBar.setVisibility(View.GONE);
                mSwipeRefreshWidget.setRefreshing(false);

            }

        });


        return blogCoupons;
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)
                myContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if(networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;

        }

        return isAvailable;
    }

    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(myContext.getFragmentManager(), "error_dialog");
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        Toast.makeText(getActivity(),"onActivityCreated()", Toast.LENGTH_SHORT).show();

    }


    @Override
    public void onStart() {
        super.onStart();
//        Toast.makeText(getActivity(),"onStart()", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
//        Toast.makeText(getActivity(),"onResume()", Toast.LENGTH_SHORT).show();
        getForecast();
    }



    @Override
    public void onPauseFragment() {
        Log.i(TAG, "onPauseFragment()");
//        Toast.makeText(getActivity(), "onPauseFragment():" + TAG, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onResumeFragment() {
        Log.i(TAG, "onResumeFragment()");
//        Toast.makeText(getActivity(), "onResumeFragment():" + TAG, Toast.LENGTH_SHORT).show();


    }

   /* @Override
    public void update() {
        Toast.makeText(getActivity(), "update", Toast.LENGTH_SHORT).show();
    }*/

}
