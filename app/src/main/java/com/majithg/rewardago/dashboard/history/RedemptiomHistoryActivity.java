package com.majithg.rewardago.dashboard.history;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.majithg.rewardago.dashboard.ProfileActivity;
import com.majithg.rewardago.util.AlertDialogFragment;
import com.majithg.rewardago.util.App;
import com.majithg.rewardago.util.CircleImageView;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RedemptiomHistoryActivity extends AppCompatActivity {

    //    private static final String UID = "13";
//    private static final String UID = MainActivity.db.getUserDetails().get("uid");
    private static final String TAG = RedemptiomHistoryActivity.class.getSimpleName();

    private String jsonData;

    private int messageType;

    private TextView textName,textNumber,textEaddress;

//    public static ExportLogsAdapter exportLogsAdapter;

    private Button editatredemption;

    private RecyclerView recyclerView;
    private TextView emptyView;

    private ProgressBar progressBar;
    private SwipeRefreshLayout mSwipeRefreshWidget;

    private List<Hdata> mCurrentWeather;
    private HRVA h_adapter;

    private TextView userid;
    public static TextView fName;
    public static TextView lName;
    public static TextView ContactName;
    public static TextView txtEmail;
    private Button btnLogout;

    CircleImageView profile_image_rh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redemptiom_history);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_24dp);
        getSupportActionBar().setHomeButtonEnabled(true);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        getForecast();

        profile_image_rh = (CircleImageView) findViewById(R.id.profile_image_rh);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);

        mSwipeRefreshWidget = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_widget);
        mSwipeRefreshWidget.setColorScheme(R.color.colorPrimaryDark, R.color.colorPrimaryDark, R.color.colorPrimaryDark, R.color.colorPrimaryDark);


        editatredemption = (Button) findViewById(R.id.editatredemption);

        editatredemption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                MainActivity.toolbar.getMenu().clear();
//                Person.vp_message.setCurrentItem(2);
//                App.pageNumper = 1;
                startActivity(new Intent(RedemptiomHistoryActivity.this, ProfileActivity.class));
            }
        });


        fName = (TextView) findViewById(R.id.lName);
        lName = (TextView) findViewById(R.id.fName);
        ContactName = (TextView) findViewById(R.id.textNumber);
        txtEmail = (TextView) findViewById(R.id.textEaddress);

        // Fetching user details from SQLite
        HashMap<String, String> user = MainActivity.db.getUserDetails();

        String uid = user.get("uid");
        String name = user.get("fname");
        String name2 = user.get("lname");
        String contact = user.get("contact");
        String email = user.get("email");

//        userid.setText(uid);
        fName.setText(name);
        lName.setText(name2);
        ContactName.setText(contact);
        txtEmail.setText(email);

        /*Picasso.with(getApplicationContext()).load(App.profilePic)
                .error(R.drawable.placeholder2)
                .placeholder(R.drawable.placeholder2)
                .into(profile_image_rh);*/

        profile_image_rh.setImageDrawable(App.pImage);

        recyclerView = (RecyclerView) findViewById(R.id.redemptionlist);

        emptyView = (TextView) findViewById(R.id.empty_history);


        mSwipeRefreshWidget.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshWidget.setRefreshing(true);
                getForecast();
            }
        });


//        recyclerView.setAdapter(h_adapter);
//        recyclerView.addItemDecoration(new MyItemDecoration());
//        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
//        setMainRecyclerView();
//        setCallLogs(curLog);

        getForecast();
    }



    private void getForecast() {

        String forecastUrl = "http://rewardago.com/cron/redemption_history.php?id="
                +MainActivity.UID+"?_=" + System.currentTimeMillis();

        if (isNetworkAvailable()){

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                   runOnUiThread(new Runnable() {
                       @Override
                       public void run() {
//                            toggleRefresh();
                           progressBar.setVisibility(View.GONE);
                           mSwipeRefreshWidget.setRefreshing(false);

                       }
                   });

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
//                        String jsonData = response.body().string();
                        jsonData = stripHtml(response.body().string());




                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//                                Toast.makeText(myContext, jsonData,Toast.LENGTH_LONG).show();
                            }
                        });


                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {

                            mCurrentWeather = getCurrentDetails(jsonData);





                        } else {
                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        }

        else {
            Toast.makeText(this, getString(R.string.network_unavailable_message), Toast.LENGTH_LONG).show();

        }

    }

    private List<Hdata> getCurrentDetails(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);


//        String timezone = forecast.getString("timezone");
//        Log.i(TAG, getString(R.string.debug_from_JSON) + timezone);

//        JSONObject currently = forecast.getJSONObject("currently");
        JSONArray currently  = forecast.getJSONArray("History");
//        JSONArray currently  = forecast.getJSONArray("json");


//        ArrayList<HashMap<String, String>> blogCoupons = new ArrayList<HashMap<String, String>>();
        List<Hdata> blogCoupons = new ArrayList<>();

        for (int i = 0; i < currently.length(); i++) {
//
            Hdata currentWeather = new Hdata();

            JSONObject post = currently.getJSONObject(i);

//            currentWeather.setId(post.getInt("offer_id"));
            currentWeather.setOutletName(post.getString("store_name"));
            currentWeather.setHistoryTitle(post.getString("title"));
            currentWeather.setClaimedDate(post.getString("claimed_date"));
            currentWeather.setRedeempedDate(post.getString("redeemed_date"));


            blogCoupons.add(currentWeather);
        }



        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                                    updateDisplay();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms


                        progressBar.setVisibility(View.GONE);
                        mSwipeRefreshWidget.setRefreshing(false);

                    }
                }, 3000);


                if (mCurrentWeather.isEmpty()) {
                    recyclerView.setVisibility(View.GONE);
                    emptyView.setVisibility(View.VISIBLE);
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    emptyView.setVisibility(View.GONE);
                }

                recyclerView.setLayoutManager(new LinearLayoutManager(RedemptiomHistoryActivity.this));
                h_adapter = new HRVA(mCurrentWeather, getApplicationContext());
//                recyclerView.setAdapter(App.h_adapter);
                recyclerView.setAdapter(h_adapter);
                recyclerView.addItemDecoration(new MyItemDecoration());


//                App.adapter_offers.notifyDataSetChanged();

            }

        });

        return blogCoupons;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if(networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;

        }

        return isAvailable;
    }

    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(getFragmentManager(), "error_dialog");
    }

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }
}
