package com.majithg.rewardago.discover;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.model.LatLng;
import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.majithg.rewardago.home.Home;
//import com.majithg.rewardago.home.HomeInside;
import com.majithg.rewardago.home.content.CouponDetailsActivity;
import com.majithg.rewardago.search.SearchAdapter;
import com.majithg.rewardago.search.Utils;
import com.majithg.rewardago.util.AlertDialogFragment;
import com.majithg.rewardago.util.App;
import com.majithg.rewardago.util.FragmentLifecycle;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * ZLB
 */
public class Search extends Fragment implements TabLayout.OnTabSelectedListener,FragmentLifecycle {
    private static final String ARG_PARAM1 = "param1";
    private static final int limit = 50;

    private int messageType;

    private int gpsoff;

//    public static ExportLogsAdapter exportLogsAdapter;

    private FragmentActivity myContext;
    private List<Fragment> list_fragment;
    private List<String> list_title;
    private TabLayout layout_tab;
    private SearchViewPager vp_message;
    private SearchViewPagerAdapter fAdapter;
    private String TAG = Search.class.getSimpleName();

    Button button1,button2,button3;

    private  String[] items = {"All",
            "Automotive",
            "Cafes and Lounges",
            "Computer Store",
            "Clothing Outlets",
            "Desserts",
            "Education",
            "Electrical",
            "Entertainment and Activities",
            "Food and Dining",
            "Healthcare and Fitness",
            "Home Appliances",
            "Home Decor",
//                    "Hotels",
//                    "Jewelry",
//                    "Laundry",
            "Medical",
            "Mobile Accessories",
//                    "Resturants_and_Cafes",
            "Services",
            "Shopping",
            "Travel and Leisure",
            "Villas"
    };

    private String[] items2 = {
                    "Abstract",
            "Daya Learners",
            "Laundromat"};
    private String jsonData;
    private String jsonData2;
    private String jsonData3;
    private String mChoice;
    private ArrayList<String> mCountries;
    private ArrayList<String> mIds;
    private String jsonSearchData;
    private Dialog toolbarSearchDialog;
    private String jsonData4;

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }

    public Search() {
    }

    public static Search newInstance(int param1) {
        Search fragment = new Search();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            messageType = getArguments().getInt(ARG_PARAM1);
        } else {
            messageType = 0;
        }

        setHasOptionsMenu(true);
        getSearchData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.search, container, false);


        vp_message = (SearchViewPager) root.findViewById(R.id.vp_search);
        layout_tab = (TabLayout) root.findViewById(R.id.tab_search);

        button1 = (Button) root.findViewById(R.id.button1);
        button2 = (Button) root.findViewById(R.id.button2);
        button3 = (Button) root.findViewById(R.id.button3);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mCountries!=null){
                    loadToolBarSearch();
//                    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                }
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterByCategory();
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterByOutlet();
            }
        });

        initControls();
/*
//        mDataSet = new ArrayList<CallHistory>();
        mDataSet = (ArrayList<CallHistory>) getData();

//        curLog = CallLogHelper.getAllCallLogs(applicationContext.getContentResolver());
//        setCallLogs(curLog);

        mRecyclerView = (RecyclerView) root.findViewById(R.id.listings_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        SwipeRecyclerViewAdapter mAdapter = new SwipeRecyclerViewAdapter(applicationContext, mDataSet);
        ((SwipeRecyclerViewAdapter) mAdapter).setMode(Attributes.Mode.Single);
        mRecyclerView.setAdapter(mAdapter);
//        setMainRecyclerView();
*/
       /* exportLogsAdapter = new ExportLogsAdapter(applicationContext);
        ListView rv = (ListView) root.findViewById(R.id.rv);
        rv.setAdapter(exportLogsAdapter);
        exportLogsAdapter.notifyDataSetChanged();*/

/*        final Button btn1 = (Button)root.findViewById(R.id.button1);
        Button btn2 = (Button)root.findViewById(R.id.button2);

        View.OnClickListener listener = new View.OnClickListener() {
            public void onClick(View view) {
                Fragment fragment = null;
                if(view == btn1){
                    fragment = new HomeInside();
                } else {
                    fragment = new FragmentTwo();
                }
                FragmentManager manager = myContext.getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.output, fragment);
                transaction.commit();
            }
        };


        btn1.setOnClickListener(listener);

        btn2.setOnClickListener(listener);*/



//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return root;
    }


    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.back_arrow) {
//            bottomNavigation.setCurrentItem(0);
//            vp_message.setCurrentItem(0);
            MainActivity.toolbar.getMenu().clear();
            MainActivity.bottomNavigation.setCurrentItem(0);
            App.vp_message_main.setCurrentItem(0);

            App.HomeCanAddItem = false;

            return true;
        }
        /*else if (id == android.R.id.home) {
//            onBackPressed();
            sliderLayout.toggleMenu();
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }


    private void initControls() {

        vp_message.setPagingEnabled(false);

        TabMapView f0 = TabMapView.newInstance(0);
        TabListView f1 = TabListView.newInstance(1);


        list_fragment = new ArrayList<>();
        list_fragment.add(f0);
        list_fragment.add(f1);

        vp_message.setOffscreenPageLimit(list_fragment.size());


        list_title = new ArrayList<>();
        list_title.add("MapView");
        list_title.add("ListView");


        layout_tab.setTabMode(TabLayout.MODE_FIXED);

        layout_tab.addTab(layout_tab.newTab().setText(list_title.get(0)));
        layout_tab.addTab(layout_tab.newTab().setText(list_title.get(1)));

        layout_tab.setTabGravity(TabLayout.GRAVITY_FILL);


        fAdapter = new SearchViewPagerAdapter(getChildFragmentManager(), list_fragment, list_title);
        vp_message.setAdapter(fAdapter);
        layout_tab.setupWithViewPager(vp_message);
        layout_tab.setOnTabSelectedListener(this);
    }


    private void enableGPS(){

        try {
            gpsoff = Settings.Secure.getInt(myContext.getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        if(gpsoff==0){

            Toast.makeText(myContext, "Please Enable GPS", Toast.LENGTH_SHORT).show();

//            new AlertDialog.Builder(myContext,R.style.AppCompatAlertDialogStyle).setIcon(R.drawable.alert).setTitle("Exit")
       /* new AlertDialog.Builder(myContext)
                .setMessage("Please Enable GPS")
                .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent onGPS = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        onGPS.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        onGPS.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(onGPS);
                    }
                }).setNegativeButton("No", null).show();*/

        }

    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        switch (tab.getPosition()) {
            case 0:
                enableGPS();
                vp_message.setCurrentItem(0);
                /*toolbar.setTitle("CSV");
                toolbar.setSubtitle(""+3);*/
                break;
            case 1:
                vp_message.setCurrentItem(1);
               /* toolbar.setTitle("XML");
                toolbar.setSubtitle(""+0);*/
                break;

        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }






    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }


    @Override
    public void onResume() {
        super.onResume();
//        setMainRecyclerView();
//        setCallLogs(curLog);
//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    @Override
    public void onPause() {
        super.onPause();
//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    @Override
    public void onPauseFragment() {
        Log.i(TAG, "onPauseFragment()");
//        Toast.makeText(getActivity(), "onPauseFragment():" + TAG, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResumeFragment() {
        Log.i(TAG, "onResumeFragment()");
//        Toast.makeText(getActivity(), "onResumeFragment():" + TAG, Toast.LENGTH_SHORT).show();
    }


    private void filterByCategory(){
        new MaterialDialog.Builder(myContext)
                .title("Filter by Category")
                .items(items)
//                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() { // -1 pottaa.. no item selected nu meaning
                // apdinaa default selected array is -1... so if we select 0 then it will be the first choice..
                            .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        /**
                         * If you use alwaysCallSingleChoiceCallback(), which is discussed below,
                         * returning false here won't allow the newly selected radio button to actually be selected.
                         **/

                        App.mGoogleMap.clear();
                        App.markerList.clear();



//                        if(text.toString().equals("All")){ // ithuvum work pannum
                            if(which==0){
//                            Toast.makeText(myContext, "All categories", Toast.LENGTH_SHORT).show();
                            getForecast();
                        }else {
                            String s = text.toString().replaceAll(" ","_");
//                            Toast.makeText(myContext, s, Toast.LENGTH_SHORT).show();
                            getForecast2(s);

                        }


                        return true;
                    }
                })
                .positiveText("ok")
                .show();
    }



    private void filterByOutlet(){
        new MaterialDialog.Builder(myContext)
                .title("Filter by Outlet")
                .items(items2)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        /**
                         * If you use alwaysCallSingleChoiceCallback(), which is discussed below,
                         * returning false here won't allow the newly selected radio button to actually be selected.
                         **/

//                        mGoogleMap.clear();
                        App.mGoogleMap.clear();
                        App.markerList.clear();

                            /*if(text.toString().equals("All")){
                                Toast.makeText(MainActivity.this, "All categories", Toast.LENGTH_SHORT).show();
                                getForecast();
                            }else {
                                Toast.makeText(MainActivity.this,text.toString() , Toast.LENGTH_SHORT).show();
                                getForecast2(text.toString());
                            }*/

                        if(which==-1){
                            Toast.makeText(myContext, "plz choose one", Toast.LENGTH_SHORT).show();

                        }else {
                            mChoice = text.toString().replaceAll(" ","_");
                            Toast.makeText(myContext,mChoice , Toast.LENGTH_SHORT).show();

                            if(which==0){
                                getForecast3(776);
                            }else if(which==1){
                                getForecast3(932);
                            }else if(which==2){
                                getForecast3(780);
                            }


                        }


                        return true;
                    }
                })
                .positiveText("ok")
                .show();
    }


    //////////////////////////////
    /////////////////////////////
    ///////////////////////////////


    private void getForecast() {

//        progressDialog.show();

        String forecastUrl = "http://rewardago.com/cron/outlets.php";

        if (isNetworkAvailable()){

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    /*myContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });*/

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
//                        String jsonData = response.body().string();
                        jsonData = response.body().string();
//                        jsonData = stripHtml(response.body().string());

                        /*runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, jsonData,Toast.LENGTH_LONG).show();
                            }
                        });*/

//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {
//                            getCurrentDetails(jsonData).dismiss();
                            getCurrentDetails(jsonData);

                        } else {
//                            progressDialog.dismiss();
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        }

        else {
            Toast.makeText(myContext, getString(R.string.network_unavailable_message),Toast.LENGTH_LONG).show();
//            mSwipeRefreshWidget.setRefreshing(false);   // i think.. an error may occur here, if we put
//            progressBar.setVisibility(View.GONE);
        }

    }


    //    private ProgressDialog getCurrentDetails(String jsonData) throws JSONException {
    private void getCurrentDetails(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);
        JSONArray currently  = forecast.getJSONArray("Store");

        App.blogCoupons.clear();
        for (int i = 0; i < currently.length(); i++) {
            MyLocation currentWeather = new MyLocation();
            JSONObject post = currently.getJSONObject(i);
            currentWeather.set_id(post.getString("id"));
            currentWeather.setTitle(post.getString("name"));
            currentWeather.setImg(post.getString("image"));

            if (post.optString("Latitude").isEmpty() || post.optString("Longitude").isEmpty()) {

            }else {
                //            double Latitude = Double.parseDouble(post.getString("Latitude"));
                double Latitude = Double.parseDouble(post.optString("Latitude"));
//            double Longitude = Double.parseDouble(post.getString("Longitude"));
                double Longitude = Double.parseDouble(post.optString("Longitude"));
                currentWeather.setLatLng(new LatLng(Latitude,Longitude));
                App.blogCoupons.add(currentWeather);
            }


        }

        myContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                progressDialog.dismiss();

                App.getInstance().LocationsLoaded(App.blogCoupons);

//                String[] stringArray = stringArrayList.toArray(new String[0]);
//                String string = Arrays.toString(stringArray);

            }
        });

//        return progressDialog;
    }



    //////////////////////////////////////////////
    //////////////////////////////////////////////
    ////////////////////////////////////////////


    private void getForecast2(String s) {

//        progressDialog.show();

        String forecastUrl = "http://rewardago.com/cron/outlets.php?cat="+s;

        if (isNetworkAvailable()){

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                   /* myContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });*/

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
//                        String jsonData = response.body().string();
                        jsonData2 = response.body().string();
//                        jsonData = stripHtml(response.body().string());

                        /*runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, jsonData,Toast.LENGTH_LONG).show();
                            }
                        });*/

//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {
                            getCurrentDetails2(jsonData2);

                        } else {
//                            progressDialog.dismiss();
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        }

        else {
            Toast.makeText(myContext, getString(R.string.network_unavailable_message),Toast.LENGTH_LONG).show();
//            mSwipeRefreshWidget.setRefreshing(false);   // i think.. an error may occur here, if we put
//            progressBar.setVisibility(View.GONE);
        }

    }


    private void getCurrentDetails2(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);
        JSONArray currently  = forecast.getJSONArray("Outlet");
        App.blogCoupons.clear();
        for (int i = 0; i < currently.length(); i++) {
            MyLocation currentWeather = new MyLocation();
            JSONObject post = currently.getJSONObject(i);
            currentWeather.set_id(post.getString("id"));
            currentWeather.setTitle(post.getString("name"));
            currentWeather.setImg(post.getString("image"));

            if (post.optString("Latitude").isEmpty() || post.optString("Longitude").isEmpty()) {

            }else {
                //            double Latitude = Double.parseDouble(post.getString("Latitude"));
                double Latitude = Double.parseDouble(post.optString("Latitude"));
//            double Longitude = Double.parseDouble(post.getString("Longitude"));
                double Longitude = Double.parseDouble(post.optString("Longitude"));
                currentWeather.setLatLng(new LatLng(Latitude,Longitude));
                App.blogCoupons.add(currentWeather);
            }


        }

        myContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                progressDialog.dismiss();
                App.getInstance().LocationsLoaded(App.blogCoupons);

//                String[] stringArray = stringArrayList.toArray(new String[0]);
//                String string = Arrays.toString(stringArray);

            }
        });

//        return progressDialog;
    }


    //////////////////////////////////////////
    //////////////////////////////////////
    ////////////////////////////////////////



    private void getForecast3(int s) {

//        progressDialog.show();

        String forecastUrl = "http://rewardago.com/cron/outlets.php?sid="+s;

        if (isNetworkAvailable()){

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                   /* myContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });*/

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
//                        String jsonData = response.body().string();
                        jsonData3 = response.body().string();
//                        jsonData = stripHtml(response.body().string());

                        /*runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, jsonData,Toast.LENGTH_LONG).show();
                            }
                        });*/

//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {
                            getCurrentDetails3(jsonData3);

                        } else {
//                            progressDialog.dismiss();
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        }

        else {
            Toast.makeText(myContext, getString(R.string.network_unavailable_message),Toast.LENGTH_LONG).show();
//            mSwipeRefreshWidget.setRefreshing(false);   // i think.. an error may occur here, if we put
//            progressBar.setVisibility(View.GONE);
        }

    }


    private void getCurrentDetails3(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);
        JSONArray currently  = forecast.getJSONArray("Outlet");
        App.blogCoupons.clear();
        for (int i = 0; i < currently.length(); i++) {
            MyLocation currentWeather = new MyLocation();
            JSONObject post = currently.getJSONObject(i);
            currentWeather.set_id(post.getString("branch_id"));
            currentWeather.setTitle(post.getString("name"));
            currentWeather.setImg(post.getString("store_image"));

            if (post.optString("Latitude").isEmpty() || post.optString("Longitude").isEmpty()) {

            }else {
                //            double Latitude = Double.parseDouble(post.getString("Latitude"));
                double Latitude = Double.parseDouble(post.optString("Latitude"));
//            double Longitude = Double.parseDouble(post.getString("Longitude"));
                double Longitude = Double.parseDouble(post.optString("Longitude"));
                currentWeather.setLatLng(new LatLng(Latitude,Longitude));
                App.blogCoupons.add(currentWeather);
            }


        }

        myContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                progressDialog.dismiss();
                App.getInstance().LocationsLoaded(App.blogCoupons);

//                String[] stringArray = stringArrayList.toArray(new String[0]);
//                String string = Arrays.toString(stringArray);

            }
        });

//        return progressDialog;
    }



    /////////////////////////////////////////
    ///////////////////////////////////////
    //////////////////////////////////////////

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)
                myContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if(networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;

        }

        return isAvailable;
    }

    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(myContext.getFragmentManager(), "error_dialog");
    }


    private void mySelection(String string) {
//        Toast.makeText(myContext,string,Toast.LENGTH_SHORT).show();

//        toolbarSearchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        toolbarSearchDialog.dismiss();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        App.mGoogleMap.clear();
        App.markerList.clear();
        getForecast4(string);

    }

    private void loadToolBarSearch() {


//        ArrayList<String> countryStored = SharedPreference.loadList(MainActivity.this, Utils.PREFS_NAME, Utils.KEY_COUNTRIES);
//        ArrayList<String> countryStored = mCountries; // all history
        ArrayList<String> countryStored = new ArrayList<>(); // empty history
        ArrayList<String> idStored = new ArrayList<>();

        View view = myContext.getLayoutInflater().inflate(R.layout.view_toolbar_search, null);

        LinearLayout parentToolbarSearch = (LinearLayout) view.findViewById(R.id.parent_toolbar_search);

        ImageView imgToolBack = (ImageView) view.findViewById(R.id.img_tool_back);

        final EditText edtToolSearch = (EditText) view.findViewById(R.id.edt_tool_search);

        ImageView imgToolMic = (ImageView) view.findViewById(R.id.img_tool_mic);

        final ListView listSearch = (ListView) view.findViewById(R.id.list_search);

        final TextView txtEmpty = (TextView) view.findViewById(R.id.txt_empty);

        Utils.setListViewHeightBasedOnChildren(listSearch);

        edtToolSearch.setHint("Search your shops");

        toolbarSearchDialog = new Dialog(Search.this.myContext, R.style.MaterialSearch);
        toolbarSearchDialog.setContentView(view);
        toolbarSearchDialog.setCancelable(false);
        toolbarSearchDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        toolbarSearchDialog.getWindow().setGravity(Gravity.BOTTOM);
        toolbarSearchDialog.show();

        toolbarSearchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
//        toolbarSearchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);



        countryStored = (countryStored != null && countryStored.size() > 0) ? countryStored : new ArrayList<String>();
        idStored = (idStored != null && idStored.size() > 0) ? idStored : new ArrayList<String>();

//        final SearchAdapter searchAdapter = new SearchAdapter(MainActivity.this, countryStored, false);
        final SearchAdapter searchAdapter = new SearchAdapter(myContext, countryStored, false, idStored);

        listSearch.setVisibility(View.VISIBLE);
        listSearch.setAdapter(searchAdapter);


        listSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                String country = String.valueOf(adapterView.getItemAtPosition(position));
//                SharedPreference.addList(MainActivity.this, Utils.PREFS_NAME, Utils.KEY_COUNTRIES, country);
                edtToolSearch.setText(country);
                listSearch.setVisibility(View.GONE);


                mySelection(country);
            }
        });
        edtToolSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

//                String[] country = MainActivity.this.getResources().getStringArray(R.array.countries_array);
//                mCountries = new ArrayList<String>(Arrays.asList(country));
                listSearch.setVisibility(View.VISIBLE);
//                searchAdapter.updateList(mCountries, true);
                searchAdapter.updateList(mCountries, true,mIds);


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ArrayList<String> filterList = new ArrayList<String>();
                ArrayList<String> filterListIds = new ArrayList<String>();
                boolean isNodata = false;
                if (s.length() > 0) {
                    for (int i = 0; i < mCountries.size(); i++) {


//                        if (mCountries.get(i).toLowerCase().startsWith(s.toString().trim().toLowerCase())) {
                        if (mCountries.get(i).toLowerCase().contains(s.toString().trim().toLowerCase())) {

                            filterList.add(mCountries.get(i));
                            filterListIds.add(mIds.get(i));

                            listSearch.setVisibility(View.VISIBLE);
//                            searchAdapter.updateList(filterList, true);
                            searchAdapter.updateList(filterList, true,filterListIds);
                            isNodata = true;
                        }
                    }
                    if (!isNodata) {
                        listSearch.setVisibility(View.GONE);
                        txtEmpty.setVisibility(View.VISIBLE);
                        txtEmpty.setText("No data found");
                    }
                } else {
                    listSearch.setVisibility(View.GONE);
                    txtEmpty.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        imgToolBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toolbarSearchDialog.dismiss();
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
            }
        });

        imgToolMic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                edtToolSearch.setText("");

            }
        });


    }

    private void getSearchData() {

//        progressDialog.show();

        String forecastUrl = "http://rewardago.com/cron/outlets.php";

        if (isNetworkAvailable()){

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    /*myContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });*/

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
//                        String jsonData = response.body().string();
                        jsonSearchData = response.body().string();
//                        jsonData = stripHtml(response.body().string());

                        /*myContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(myContext, jsonSearchData,Toast.LENGTH_LONG).show();
                            }
                        });*/

//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {
//                            getCurrentDetails(jsonData).dismiss();
                            gotSearchData(jsonSearchData);

                        } else {
//                            progressDialog.dismiss();
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
//                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
//                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        }

        else {
            Toast.makeText(myContext, getString(R.string.network_unavailable_message),Toast.LENGTH_LONG).show();
//            mSwipeRefreshWidget.setRefreshing(false);   // i think.. an error may occur here, if we put
//            progressBar.setVisibility(View.GONE);
        }

    }

    private void gotSearchData(String jsonData) throws JSONException {

        JSONObject forecast = new JSONObject(jsonData);
//        JSONObject jsonObject = new JSONObject(result);
//                    JSONArray jArray = new JSONArray(result);
//        JSONArray jArray = forecast.getJSONArray("json");
        JSONArray jArray = forecast.getJSONArray("Store");

        mCountries = new ArrayList<String>();
        mIds = new ArrayList<String>();

        // Extract data from json and store into ArrayList
        for (int i = 0; i < jArray.length(); i++) {
            JSONObject json_data = jArray.getJSONObject(i);
//            dataList.add(json_data.getString("fish_name"));
            mCountries.add(json_data.getString("name"));
            mIds.add(json_data.getString("id"));
        }



/*        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                progressDialog.dismiss();

//                strArrData = countryStored.toArray(new String[countryStored.size()]);
//                loadToolBarSearch();

            }
        });*/

//        return progressDialog;
    }



    private void getForecast4(String s) {

//        progressDialog.show();

        String forecastUrl = "http://rewardago.com/cron/outlets.php?mid="+s;

        if (isNetworkAvailable()){

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                   /* myContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });*/

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
//                        String jsonData = response.body().string();
                        jsonData4 = response.body().string();
//                        jsonData = stripHtml(response.body().string());

                        /*runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, jsonData,Toast.LENGTH_LONG).show();
                            }
                        });*/

//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {
                            getCurrentDetails4(jsonData4);

                        } else {
//                            progressDialog.dismiss();
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        }

        else {
            Toast.makeText(myContext, getString(R.string.network_unavailable_message),Toast.LENGTH_LONG).show();
//            mSwipeRefreshWidget.setRefreshing(false);   // i think.. an error may occur here, if we put
//            progressBar.setVisibility(View.GONE);
        }

    }


    private void getCurrentDetails4(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);
        JSONArray currently  = forecast.getJSONArray("Store");
        App.blogCoupons.clear();
//        for (int i = 0; i < currently.length(); i++) {
            MyLocation currentWeather = new MyLocation();
            JSONObject post = currently.getJSONObject(0);
            currentWeather.set_id(post.getString("id"));
            currentWeather.setTitle(post.getString("name"));
            currentWeather.setImg(post.getString("image"));

            if (post.optString("Latitude").isEmpty() || post.optString("Longitude").isEmpty()) {

            }else {
                //            double Latitude = Double.parseDouble(post.getString("Latitude"));
                double Latitude = Double.parseDouble(post.optString("Latitude"));
//            double Longitude = Double.parseDouble(post.getString("Longitude"));
                double Longitude = Double.parseDouble(post.optString("Longitude"));
                currentWeather.setLatLng(new LatLng(Latitude,Longitude));
                App.blogCoupons.add(currentWeather);
            }


//        }

        myContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                progressDialog.dismiss();
                App.getInstance().LocationsLoaded(App.blogCoupons);

//                String[] stringArray = stringArrayList.toArray(new String[0]);
//                String string = Arrays.toString(stringArray);

            }
        });

//        return progressDialog;
    }

}
