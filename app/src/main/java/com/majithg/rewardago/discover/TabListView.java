package com.majithg.rewardago.discover;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.majithg.rewardago.favourite.FavListAdapter;
import com.majithg.rewardago.home.content.Place;
//import com.majithg.rewardago.home.content.PlaceData;
//import com.majithg.rewardago.discover.listview.TabListAdapter;
import com.majithg.rewardago.util.AlertDialogFragment;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * ZLB
 */
public class TabListView extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final int limit = 50;

    Context applicationContext = MainActivity.getContextOfApplication();

    private int messageType;



    private RecyclerView mRecyclerView;
    private StaggeredGridLayoutManager mStaggeredLayoutManager;

    private FragmentActivity myContext;
    private String jsonData;
    private List<Place> mCurrentWeather = new ArrayList<>();

    private TextView emptyView;

    private ProgressBar progressBar;
    private SwipeRefreshLayout mSwipeRefreshWidget;
    private FavListAdapter favListAdapter;

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }

    public TabListView() {
    }

    public static TabListView newInstance(int param1) {
        TabListView fragment = new TabListView();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            messageType = getArguments().getInt(ARG_PARAM1);
        } else {
            messageType = 0;
        }

        getForecast();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.tab_listview, container, false);

        progressBar = (ProgressBar) root.findViewById(R.id.progress_bar_list);
        mSwipeRefreshWidget = (SwipeRefreshLayout) root.findViewById(R.id.swipe_refresh_widget_list);


        progressBar.setVisibility(View.VISIBLE);

        mSwipeRefreshWidget.setColorScheme(R.color.colorPrimaryDark, R.color.colorPrimaryDark,
                R.color.colorPrimaryDark, R.color.colorPrimaryDark);


        mRecyclerView = (RecyclerView) root.findViewById(R.id.recyclerview_list);
        emptyView = (TextView) root.findViewById(R.id.empty_view_list);


        mSwipeRefreshWidget.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getForecast();

                // this is also working...
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        mSwipeRefreshWidget.setRefreshing(false);


                    }
                }, 2000);



            }
        });


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                mSwipeRefreshWidget.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
            }
        }, 5000);



        return root;
    }


    @Override
    public void onResume() {
        super.onResume();
        getForecast();
    }




    private void getForecast() {




        String forecastUrl = "http://rewardago.com/cron/listview.php?id="
                +MainActivity.UID+"?_=" + System.currentTimeMillis();




        if (isNetworkAvailable()){







            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();
            Call call = client.newCall(request);
            call.enqueue(new Callback() {



                @Override
                public void onFailure(Request request, IOException e) {
                    stopRefreshing();
                    alertUserAboutError();
                }
                @Override
                public void onResponse(Response response) throws IOException {
                    try {
                        jsonData = stripHtml(response.body().string());
                        if (response.isSuccessful()) {
                            mCurrentWeather = getCurrentDetails(jsonData);
                            stopRefreshing();
                        } else {
                            stopRefreshing();
                            alertUserAboutError();
                        }
                    }
                    catch (IOException e) {
                        stopRefreshing();
//                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
                        stopRefreshing();
//                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }



            });






        }

        else {
            Toast.makeText(myContext, getString(R.string.network_unavailable_message),Toast.LENGTH_LONG).show();
//            mSwipeRefreshWidget.setRefreshing(false);   // i think.. an error may occur here, if we put
//            progressBar.setVisibility(View.GONE);
//            toggleRefresh();
//            stopRefreshing(); don't use anything here ...
        }

    }


    private void stopRefreshing(){

        myContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshWidget.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
            }
        });

    }


    private List<Place> getCurrentDetails(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);

        JSONArray currently  = forecast.getJSONArray("Coupon");

        List<Place> blogCoupons = new ArrayList<>();

        for (int i = 0; i < currently.length(); i++) {

            Place currentWeather = new Place();

            JSONObject post = currently.getJSONObject(i);

//            String title = post.getString("id");
            currentWeather.setId(post.getInt("id"));
            currentWeather.setName(post.getString("title"));
            currentWeather.setName2(post.getString("Sub_Title"));
//            currentWeather.setImageResourceId(post.getString("image"));
            currentWeather.setImageName(post.getString("image"));
            currentWeather.setImageName2(post.getString("Store_logo"));

            blogCoupons.add(currentWeather);
        }


//        Log.d(TAG, currentWeather.getFormattedTime());


        myContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {



                //TODO plz enable later
                if (mCurrentWeather.isEmpty()) {
                    mRecyclerView.setVisibility(View.GONE);
                    emptyView.setVisibility(View.VISIBLE);

                }
                else {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    emptyView.setVisibility(View.GONE);
                }


                mStaggeredLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
                mRecyclerView.setLayoutManager(mStaggeredLayoutManager);
                mRecyclerView.setHasFixedSize(true);
                favListAdapter = new FavListAdapter(getActivity(), mCurrentWeather);
                mRecyclerView.setAdapter(favListAdapter);


            }

        });


        return blogCoupons;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)
                myContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if(networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;

        }

        return isAvailable;
    }

    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(myContext.getFragmentManager(), "error_dialog");
    }

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }

}
