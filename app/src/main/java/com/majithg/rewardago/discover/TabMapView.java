package com.majithg.rewardago.discover;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.majithg.rewardago.util.AlertDialogFragment;
import com.majithg.rewardago.util.App;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * ZLB
 */
public class TabMapView extends Fragment
        implements OnMapReadyCallback
        ,GoogleApiClient.ConnectionCallbacks
        , GoogleApiClient.OnConnectionFailedListener
        , LocationListener
{
    private static final String ARG_PARAM1 = "param1";
    private static final int limit = 50;
    private int messageType;

//    private GoogleMap googleMap;
//    private SupportMapFragment supportMapFragment;
//    private MapView mapView;

    private int gpsoff;

    private FragmentActivity myContext;
    MapView mMapView;
    SupportMapFragment mapFragment;
//    private GoogleMap mGoogleMap;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    private String jsonData;
//    ProgressDialog progressDialog;
    private String TAG = MainActivity.class.getSimpleName();
//    List<MyLocation> blogCoupons = new ArrayList<>();
    private int inttt;
    JSONObject global;
//    private HashMap<String, String> images= new HashMap<>();
//    private HashMap<String, LatLng> mmm= new HashMap<>();
//    private List<Marker> markerList = new ArrayList<>();
    private String jsonData2;


    private  String[] items = {"All",
            "Automotive",
            "Cafes and Lounges",
            "Computer Store",
            "Clothing Outlets",
            "Desserts",
            "Education",
            "Electrical",
            "Entertainment and Activities",
            "Food and Dining",
            "Healthcare and Fitness",
            "Home Appliances",
            "Home Decor",
//                    "Hotels",
//                    "Jewelry",
//                    "Laundry",
            "Medical",
            "Mobile Accessories",
//                    "Resturants_and_Cafes",
            "Services",
            "Shopping",
            "Travel and Leisure",
            "Villas"
    };

    private String[] items2 = {
//                    "All",
            "Clothing Outlets",
            "Computer Store"};


    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }

    public TabMapView() {
    }

    public static TabMapView newInstance(int param1) {
        TabMapView fragment = new TabMapView();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            messageType = getArguments().getInt(ARG_PARAM1);
        } else {
            messageType = 0;
        }


      App.layoutInflater =  myContext.getLayoutInflater();
    }

   /* @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mapFragment = new SupportMapFragment();
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.tab_mapview, container, false);

        /*supportMapFragment = new SupportMapFragment() {
            @Override
            public void onActivityCreated(Bundle savedInstanceState) {
                super.onActivityCreated(savedInstanceState);
                googleMap = supportMapFragment.getMap();
                if (googleMap != null) {
                    setupMap();
                }
            }
        };
        getChildFragmentManager().beginTransaction().add(R.id.map, supportMapFragment).commit();*/


//        enableGPS();


//        progressDialog = new ProgressDialog(myContext);
//        progressDialog.setIndeterminate(true);
//        progressDialog.setCancelable(false);
//        progressDialog.setMessage("fatching data ...");

        /*if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {   // android version >= marshmallow
            checkLocationPermission();
        }

        mapFragment = (SupportMapFragment) myContext.getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);*/


        /*mMapView = (MapView) root.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume();// needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mGoogleMap = mMapView.getMap();*/
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapView);
//        mGoogleMap = (mapFragment).getMap();
        App.mGoogleMap = (mapFragment).getMap();
        mapFragment.getMapAsync(this);

        return root;
    }


    // also working but no good
    /*@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView = (MapView) view.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
    }*/

   /* private void setupMap(){
        //         display zoom map
        googleMap.setMyLocationEnabled(true);
        googleMap.setOnMapClickListener(this);
        googleMap.setOnMapLongClickListener(this);
        googleMap.setOnMarkerClickListener(this);
    }*/

private void enableGPS(){

    try {
        gpsoff = Settings.Secure.getInt(myContext.getContentResolver(), Settings.Secure.LOCATION_MODE);
    } catch (Settings.SettingNotFoundException e) {
        e.printStackTrace();
    }
    if(gpsoff==0){

        Toast.makeText(myContext, "Please Enable GPS", Toast.LENGTH_SHORT).show();

//            new AlertDialog.Builder(myContext,R.style.AppCompatAlertDialogStyle).setIcon(R.drawable.alert).setTitle("Exit")
       /* new AlertDialog.Builder(myContext)
                .setMessage("Please Enable GPS")
                .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent onGPS = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        onGPS.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        onGPS.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(onGPS);
                    }
                }).setNegativeButton("No", null).show();*/

    }

}

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(myContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
//        setMainRecyclerView();
//        setCallLogs(curLog);
//        mMapView.onResume();
//        mapFragment.onResume();
//        App.mGoogleMap = (mapFragment).getMap();
//        mapFragment.getMapAsync(this);
    }

    @Override
    public void onPause() {
        super.onPause();
//        mMapView.onPause();
//        mapFragment.onPause();
        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mMapView.onDestroy();
//        mapFragment.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
//        mapFragment.onLowMemory();
//        mMapView.onLowMemory();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapView);

//        FragmentManager fm = getActivity().getSupportFragmentManager();
//        Fragment fragment = (fm.findFragmentById(R.id.mapView));
//        FragmentTransaction ft = fm.beginTransaction();
//        ft.remove(fragment);
//        ft.commit();

//        mapFragment.onDestroyView();

        FragmentManager fm = getChildFragmentManager();
        Fragment fragment = (fm.findFragmentById(R.id.mapView));
        FragmentTransaction ft = fm.beginTransaction();
        ft.remove(fragment);
//        ft.commit();
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (ContextCompat.checkSelfPermission(myContext,Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(myContext,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(myContext,Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(myContext, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(myContext, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        if (googleMap != null) {

//            mGoogleMap = googleMap;
            App.mGoogleMap = googleMap;
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            //Initialize Google Play Services
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(myContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    buildGoogleApiClient();
                    googleMap.setMyLocationEnabled(true);
//                googleMap.getUiSettings().setCompassEnabled(true);
                }
            } else {
                buildGoogleApiClient();
                googleMap.setMyLocationEnabled(true);
//            googleMap.getUiSettings().setCompassEnabled(true);
            }


            getForecast();

        }
    }

    /**
     * Callback for the result from requesting permissions. This method
     * is invoked for every call on {@link #requestPermissions(String[], int)}.
     * <p>
     * <strong>Note:</strong> It is possible that the permissions request interaction
     * with the user is interrupted. In this case you will receive empty permissions
     * and results arrays which should be treated as a cancellation.
     * </p>
     *
     * @param requestCode  The request code passed in {@link #requestPermissions(String[], int)}.
     * @param permissions  The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *                     which is either {@link PackageManager#PERMISSION_GRANTED}
     *                     or {@link PackageManager#PERMISSION_DENIED}. Never null.
     * @see #requestPermissions(String[], int)
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(myContext,Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
//                        mGoogleMap.setMyLocationEnabled(true);
                        App.mGoogleMap.setMyLocationEnabled(true);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(myContext, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }

    }



    /////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////


    private void getForecast() {

//        progressDialog.show();

        String forecastUrl = "http://rewardago.com/cron/outlets.php";

        if (isNetworkAvailable()){

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                   /* myContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });*/

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
//                        String jsonData = response.body().string();
                        jsonData = response.body().string();
//                        jsonData = stripHtml(response.body().string());

                        /*runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, jsonData,Toast.LENGTH_LONG).show();
                            }
                        });*/

//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {
//                            getCurrentDetails(jsonData).dismiss();
                            getCurrentDetails(jsonData);

                        } else {
//                            progressDialog.dismiss();
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        }

        else {
            Toast.makeText(myContext, "network_unavailable_message",Toast.LENGTH_LONG).show();
//            mSwipeRefreshWidget.setRefreshing(false);   // i think.. an error may occur here, if we put
//            progressBar.setVisibility(View.GONE);
        }

    }


//    private ProgressDialog getCurrentDetails(String jsonData) throws JSONException {
        private void getCurrentDetails(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);
        JSONArray currently  = forecast.getJSONArray("Store");

        App.blogCoupons.clear();
        for (int i = 0; i < currently.length(); i++) {
            MyLocation currentWeather = new MyLocation();
            JSONObject post = currently.getJSONObject(i);

            currentWeather.set_id(post.getString("id"));
            currentWeather.setTitle(post.getString("name"));
            currentWeather.setImg(post.getString("image"));

            if (post.optString("Latitude").isEmpty() || post.optString("Longitude").isEmpty()) {

            }else {
                //            double Latitude = Double.parseDouble(post.getString("Latitude"));
                double Latitude = Double.parseDouble(post.optString("Latitude"));
//            double Longitude = Double.parseDouble(post.getString("Longitude"));
                double Longitude = Double.parseDouble(post.optString("Longitude"));
                currentWeather.setLatLng(new LatLng(Latitude,Longitude));
                App.blogCoupons.add(currentWeather);
            }


        }

        myContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                progressDialog.dismiss();

                App.getInstance().LocationsLoaded(App.blogCoupons);

//                String[] stringArray = stringArrayList.toArray(new String[0]);
//                String string = Arrays.toString(stringArray);

            }
        });

//        return progressDialog;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)
                myContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if(networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;

        }

        return isAvailable;
    }

    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(myContext.getFragmentManager(), "error_dialog");
    }






    //////////////////////////////////////////////////
    //////////////////////////////////////////////////


    /*private void getForecast2(String s) {

//        progressDialog.show();

        String forecastUrl = "http://rewardago.com/cron/outlets.php?cat="+s;

        if (isNetworkAvailable()){

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    myContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
//                        String jsonData = response.body().string();
                        jsonData2 = response.body().string();
//                        jsonData = stripHtml(response.body().string());

                        *//*runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, jsonData,Toast.LENGTH_LONG).show();
                            }
                        });*//*

//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {
                            getCurrentDetails2(jsonData2).dismiss();

                        } else {
                            progressDialog.dismiss();
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        }

        else {
            Toast.makeText(myContext, "network_unavailable_message",Toast.LENGTH_LONG).show();
//            mSwipeRefreshWidget.setRefreshing(false);   // i think.. an error may occur here, if we put
//            progressBar.setVisibility(View.GONE);
        }

    }


    private ProgressDialog getCurrentDetails2(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);
        JSONArray currently  = forecast.getJSONArray("Outlet");
        blogCoupons.clear();
        for (int i = 0; i < currently.length(); i++) {
            MyLocation currentWeather = new MyLocation();
            JSONObject post = currently.getJSONObject(i);
            currentWeather.set_id(post.getString("id"));
            currentWeather.setTitle(post.getString("name"));
            currentWeather.setImg(post.getString("image"));

            if (post.optString("Latitude").isEmpty() || post.optString("Longitude").isEmpty()) {

            }else {
                //            double Latitude = Double.parseDouble(post.getString("Latitude"));
                double Latitude = Double.parseDouble(post.optString("Latitude"));
//            double Longitude = Double.parseDouble(post.getString("Longitude"));
                double Longitude = Double.parseDouble(post.optString("Longitude"));
                currentWeather.setLatLng(new LatLng(Latitude,Longitude));
                blogCoupons.add(currentWeather);
            }


        }

        myContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
                App.getInstance().LocationsLoaded(App.blogCoupons);

//                String[] stringArray = stringArrayList.toArray(new String[0]);
//                String string = Arrays.toString(stringArray);

            }
        });

        return progressDialog;
    }*/



    /*private void LocationsLoaded(List<MyLocation> locations){

        for (MyLocation myLoc : locations){
//            Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                    Marker marker = App.mGoogleMap.addMarker(new MarkerOptions()
                    .position(myLoc.getLatLng())
                    .title(myLoc.getTitle())
//                    .snippet(myLoc.snippet)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA)));

            images.put(marker.getId().toString(), myLoc.getImg());
            mmm.put(marker.getId().toString(), myLoc.getLatLng());

            markerList.add(marker);
        }

        if(!locations.isEmpty()) {
//            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(locations.get(0).getLatLng(), 13));
            App.mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(locations.get(0).getLatLng(), 13));
        }else{
            Toast.makeText(myContext, "Nothing to display", Toast.LENGTH_LONG).show();
        }
//        mGoogleMap.getUiSettings().setMapToolbarEnabled(true);
        App.mGoogleMap.getUiSettings().setMapToolbarEnabled(true);
//        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        App.mGoogleMap.getUiSettings().setZoomControlsEnabled(true);

//        mGoogleMap.setPadding(0,0,6,96);

//        progressDialog.dismiss();


        *//*mGoogleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });*//*

        PopupAdapter infowin = new PopupAdapter(myContext.getApplicationContext(),myContext.getLayoutInflater(),images);
//        mGoogleMap.setInfoWindowAdapter(infowin);
        App.mGoogleMap.setInfoWindowAdapter(infowin);


//        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
        App.mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

//                marker.showInfoWindow();
//                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mmm.get(marker.getId()), 10));
//                return true; // true potta taan work pannum
                return false;
            }
        });

//        mGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
        App.mGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {

            }
        });

    }*/








}
