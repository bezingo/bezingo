package com.majithg.rewardago.util;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.majithg.rewardago.dashboard.Person;
import com.majithg.rewardago.discover.Search;
import com.majithg.rewardago.favourite.Favourite;
import com.majithg.rewardago.home.Home;
import com.majithg.rewardago.mycoupons.Offers;

import java.util.List;

public class MainViewPagerAdapter extends FragmentPagerAdapter {
//    don't use FragmentStatePagerAdapter here... if you did, when we navigate 5th to 1st, app will stopped..
//    because during the lifecycle, fragments will be onDetach()...
//    but in FragmentPagerAdapter, fragments will be only onDestryView().. so don't want to recreate...

    private static final String TAG = MainViewPagerAdapter.class.getSimpleName();
    private List<Fragment> list_fragment;
    private List<String> list_Title;
    private Context mContext;


    public MainViewPagerAdapter(FragmentManager fm) {
        super(fm);
//        this.mContext = context;
    }


   /* public MainViewPagerAdapter(FragmentManager fm, List<Fragment> list_fragment, List<String> list_Title) {
        super(fm);
        this.list_fragment = list_fragment;
        this.list_Title = list_Title;
    }*/

    public MainViewPagerAdapter( FragmentManager fm, List<Fragment> list_fragment) {
        super(fm);
//        this.mContext = context;
        this.list_fragment = list_fragment;
        this.list_Title = list_Title;
    }

   /* @Override
    public Object instantiateItem(ViewGroup container, int position) {
//        List<Fragment> fragmentsList = mFragmentManager.getFragments();
//        list_fragment
//        if (fragmentsList != null && position <= (fragmentsList.size() - 1)) {
            if (list_fragment != null && position <= (list_fragment.size() - 1)) {
//            SampleFragment sampleFragment = (SampleFragment) fragmentsList.get(position);
                SampleFragment sampleFragment = (SampleFragment) list_fragment.get(position);
//            Utils.DummyItem dummyItem = mDummyItems.get(position);
                Utils.DummyItem dummyItem = mDummyItems.get(position);
            //If the current data of the fragment changed, set the new data
            if (!dummyItem.equals(sampleFragment.getDummyItem())) {
                sampleFragment.setDummyItem(dummyItem);
//                Log.i(TAG, "********instantiateItem position:" + position + " FragmentDataChanged");
            }
        } else {
            //No fragment instance available for this index, create a new fragment by calling getItem() and show the data.
//            Log.i(TAG, "********instantiateItem position:" + position + " NewFragmentCreated");
        }

        return super.instantiateItem(container, position);
    }
*/
    @Override
    public Fragment getItem(int position) {
        return list_fragment.get(position);
    }
   /* @Override
    public Fragment getItem(int position) {
        Log.d(TAG, "getItem(" + position + ")");
        if (position == 0) {
            return Page0Fragment.newInstance(mDate);
        } else if (position == 1) {
            return Page1Fragment.newInstance(mContent);
        } else if (position == 2) {
            return Page2Fragment.newInstance(mChecked);
        } else if (position == 3) {
            // return Page3Fragment.newInstance();
            return getFragment(3);
        } else if (position == 4) {
            return ContainerFragment.newInstance(0, mDate, mContent);
        }

        return null;
    }*/

    @Override
    public int getCount() {
//        return list_Title.size();
        return list_fragment.size();
    }


    @Override
    public CharSequence getPageTitle(int position) {
//        return list_Title.get(position % list_Title.size());
        return null;
    }

    /*@Override
    public int getItemPosition(Object object) {
        // POSITION_NONE makes it possible to reload the PagerAdapter
//        return POSITION_NONE;
        if (object instanceof Home) {
            ((Home)object).updateView();
        }
        return  super . getItemPosition (object);
    }*/

    @Override
    // To update fragment in ViewPager, we should override getItemPosition() method,
    // in this method, we call the fragment's public updating method.
    public int getItemPosition(Object object) {
        Log.d(TAG, "getItemPosition(" + object.getClass().getSimpleName() + ")");
        if (object instanceof Home) {
//            ((Home) object).updateDate("Hi kutty");
           /* Home f0 = (Home) object;
            if (f0 != null) {
                f0.update();
            }*/

        } else if (object instanceof Search) {
//            ((Search) object).updateContent(mContent);
        } else if (object instanceof Favourite) {
//            ((Favourite) object).updateCheckedStatus(mChecked);
        } else if (object instanceof Offers) {
//            ((Offers) object).updateData(mFragmentToShow, mDate, mContent);
        } else if (object instanceof Person) {
//            ((Person) object).updateData(mFragmentToShow, mDate, mContent);
        }

        return super.getItemPosition(object);
    }


   /* @Override
    public Object instantiateItem(ViewGroup container, int position) {

        if (list_fragment != null && position <= (list_fragment.size() - 1)) {

            Fragment mFragment = (Fragment) list_fragment.get(position);
//            Utils.DummyItem dummyItem = mDummyItems.get(position);
            //If the current data of the fragment changed, set the new data
            if (mFragment.equals(Home.newInstance(0))) {
                sampleFragment.setDummyItem(dummyItem);
                Log.i(TAG, "********instantiateItem position:" + position + " FragmentDataChanged");
            }


        } else {
            //No fragment instance available for this index, create a new fragment by calling getItem() and show the data.
            Log.i(TAG, "********instantiateItem position:" + position + " NewFragmentCreated");
        }

        return super.instantiateItem(container, position);
    }*/
}
