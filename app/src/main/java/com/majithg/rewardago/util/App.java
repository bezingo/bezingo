package com.majithg.rewardago.util;

import android.app.Application;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.SystemClock;

import android.support.v4.app.FragmentActivity;
//import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.majithg.rewardago.dailog.DialogCliam;
import com.majithg.rewardago.dailog.DialogRedeem;
import com.majithg.rewardago.discover.MyLocation;
import com.majithg.rewardago.discover.PopupAdapter;
import com.majithg.rewardago.home.content.CouponDetailsActivity;
import com.majithg.rewardago.home.content.Place;
import com.majithg.rewardago.outlets.OutletCoupons;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import android.app.FragmentManager;

//import android.support.v4.app.FragmentActivity;
//import android.support.v4.app.FragmentTransaction;


public class App extends Application {


    public static boolean personCanAddItem = false;
    public static boolean HomeCanAddItem = false;

    public static Drawable pImage;

//    public static CircleImageView profile_image_rh;
//    public static CircleImageView profile_image_person;
    public static CircleImageView profile_image_main;
//    public static CircleImageView profile_image_edit;

    public static Button claim, share;

    public static GoogleMap mGoogleMap;

    public static final String TAG = App.class.getSimpleName();

    public static String profilePic;

    public static ArrayList<String> codes;
    public static int menu_id;
//    public static Data2 data2;
//    public static List<Data2> data2Array;

    private RequestQueue mRequestQueue;

    private static App mInstance;                    // plz keep private... don't change to public

    public static List<Place> mCurrentWeather;

    Toolbar toolbar;


//    public static Recycler_View_Adapter2 adapter;

//    public static List<Data2> data;

    public static int pageNumper;

//    public static List<Place> mlist;

//    public static TravelListAdapter mAdapter;

    public static MainViewPager vp_message_main;

    public static HashMap<String, String> nnmms= new HashMap<>();
    public static HashMap<String, Integer> ids= new HashMap<>();
    public static HashMap<String, String> images= new HashMap<>();
    public static HashMap<String, LatLng> mmm= new HashMap<>();
    public static List<Marker> markerList = new ArrayList<>();

    public static LayoutInflater layoutInflater;

    public static List<MyLocation> blogCoupons = new ArrayList<>();

  /*  public  static String EXTRA_PARAM_COUPON_TYPE;
    public  static String EXTRA_PARAM_COUPON_QR_CODE;
    public  static boolean EXTRA_PARAM_COUPON_HIDE_QR_CODE = false;

//    public  static String EXTRA_PARAM_COUPON_CLAIMED;
//    public  static String EXTRA_PARAM_COUPON_REDEEMED;
    public  static String EXTRA_PARAM_COUPON_EXPIRY ;

    public  static String EXTRA_PARAM_TITLE;
    public  static String EXTRA_PARAM_SUBTITLE;
    public  static String EXTRA_PARAM_IMAGE;
    public  static String EXTRA_PARAM_STORE_LOGO;
    public  static String EXTRA_PARAM_STORE_CONTACT;
    public  static String EXTRA_PARAM_STORE_ADDRESS;*/


    public  static FragmentActivity appmyContext;  // value of appmyContext will be added at Home fragment
//    public  static Context appmyContext = MainActivity.contextOfApplication;
//    public  static AppCompatActivity appmyContext = MainActivity.appCompatActivity;

    public  static DialogCliam appdialogCliam;
//    ProgressDialog progressDialog;
    private String claimedRespone;
    private boolean errorClaiming = true;
//    private int nn=0;


    public  static FragmentManager manager;
    private String remeededRespone;
    private boolean errorRedeeming = true;
    private int coupon_type;
    private int redeemed_status;

    public final static int QRcodeWidth = 500 ;
    private Bitmap bitmap ;
    private String qr_code;


    public App() {

    }

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;



//        progressDialog = new ProgressDialog(this);
//        dialogCliam = new DialogCliam();

        codes  = new ArrayList<>();
//        data2Array = new ArrayList<>();

//        data = new ArrayList<>();
//        adapter = new Recycler_View_Adapter2(data, getApplicationContext());

        // Don't do this! This is just so cold launches take some time
        SystemClock.sleep(TimeUnit.SECONDS.toMillis(2));
    }

    public static synchronized App getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(com.android.volley.Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(com.android.volley.Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public static boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)
                getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if(networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;
        }

        return isAvailable;
    }

    public void alertUserAboutError() {

        FragmentManager fm = appmyContext.getFragmentManager();
        AlertDialogFragment dialog = new AlertDialogFragment();

//        FragmentTransaction transaction = fm.beginTransaction();
//        transaction.add(dialog,"error_dialog");
//        transaction.commitAllowingStateLoss();

        dialog.show(fm, "error_dialog");
//        dialog.show();
    }



    public void claimCoupon(int cid) {

//        int nn;

//        progressDialog.setMessage("claiming...");
//        progressDialog.show();

        String forecastUrl = "http://rewardago.com/cron/claim.php?id="
                + MainActivity.UID + "&oid=" + cid;

        if (isNetworkAvailable()) {

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
                        claimedRespone = response.body().string();
//                        claimedRespone = stripHtml(response.body().string());



                        // working
                        /*appmyContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(appmyContext, claimedRespone, Toast.LENGTH_LONG).show();
                            }
                        });*/


//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {

//                            progressDialog.dismiss();
                      claimingCoupon(claimedRespone);


                        } else {
//                            progressDialog.dismiss();
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
//                    } catch (IOException e) {
//                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    } catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        } else {
//            progressDialog.dismiss();
            Toast.makeText(this, getString(R.string.network_unavailable_message), Toast.LENGTH_LONG).show();
//            return null;
        }



    }

    private void claimingCoupon(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);
        JSONArray jsonArray = forecast.getJSONArray("Claim");
        JSONObject jsonObject = jsonArray.getJSONObject(0);
        errorClaiming = jsonObject.getBoolean("error");

        appmyContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!errorClaiming) {
                    errorClaiming = true;
//                    isEditTextVisible = 0;
//                    mAddButton.setImageResource(R.drawable.ic_favorite_border_white_24dp);
//                    dialogCliam.show(appmyContext.getFragmentManager(), "dialog");
//                    Toast.makeText(appmyContext, "successfully claimed", Toast.LENGTH_SHORT).show();
                    App.claim.setText("Show Coupon");
                    App.claim.setBackgroundColor(getResources().getColor(R.color.showCoupon));
                    testing();

                } else {
                    Toast.makeText(appmyContext, "something went wrong", Toast.LENGTH_SHORT).show();
//                    return testing();
                }


            }
        });

//        return progressDialog;
//        return testing();
//        return nn;
    }

    public void redeemCoupon(int cid) {

//        int nn;

//        progressDialog.setMessage("claiming...");
//        progressDialog.show();

        String forecastUrl = "http://rewardago.com/cron/redemption.php?id="
                + MainActivity.UID + "&oid=" + cid;

        if (isNetworkAvailable()) {

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
                        remeededRespone = response.body().string();
//                        remeededRespone = stripHtml(response.body().string());



                        // working
                        /*appmyContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(appmyContext, remeededRespone, Toast.LENGTH_LONG).show();
                            }
                        });*/


//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {

//                            progressDialog.dismiss();
                            redeemingCoupon(remeededRespone);


                        } else {
//                            progressDialog.dismiss();
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
//                    } catch (IOException e) {
//                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    } catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        } else {
//            progressDialog.dismiss();
            Toast.makeText(this, getString(R.string.network_unavailable_message), Toast.LENGTH_LONG).show();
//            return null;
        }



    }

    private void redeemingCoupon(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);
        JSONArray jsonArray = forecast.getJSONArray("Redeem");
        JSONObject jsonObject = jsonArray.getJSONObject(0);
        errorRedeeming = jsonObject.getBoolean("error");

        if (!errorRedeeming) {
            qr_code = jsonObject.getString("qr_code");
            coupon_type = Integer.parseInt(jsonObject.getString("coupon_type"));
            redeemed_status = Integer.parseInt(jsonObject.getString("redeemed_status"));

        }


        appmyContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!errorRedeeming) {

                    errorRedeeming = true;

                    showRedeemedDialog(qr_code);

                    if(coupon_type == 2){


                        if(redeemed_status == 0){
//                            Toast.makeText(appmyContext, "successfully redeemed", Toast.LENGTH_SHORT).show();
                            App.claim.setText("Redeem Again");
                            App.claim.setBackgroundColor(getResources().getColor(R.color.redeemAgain));
                        }else{
//                            Toast.makeText(appmyContext, "you redeemed again", Toast.LENGTH_SHORT).show();
                            App.claim.setText("Redeem Again");
                            App.claim.setBackgroundColor(getResources().getColor(R.color.redeemAgain));
                        }


                    } else {
//                        Toast.makeText(appmyContext, "successfully redeemed", Toast.LENGTH_SHORT).show();
                        App.claim.setText("Redeemed");
                        App.claim.setBackgroundColor(getResources().getColor(R.color.redeemed));
//                        App.claim.setEnabled(false);
                    }

//                    showRedeemedDialog();

//                } else if(){

                }else {
                    Toast.makeText(appmyContext, "something went wrong", Toast.LENGTH_SHORT).show();
//                    return testing();
                }


            }
        });

//        return progressDialog;
//        return testing();
//        return nn;
    }

    public void showRedeemedDialog(String code) {
//        Toast.makeText(this,"generating QR code",Toast.LENGTH_SHORT).show();
        Bundle args = new Bundle();
//        if(!EXTRA_PARAM_COUPON_HIDE_QR_CODE) {
            if(!CouponDetailsActivity.EXTRA_PARAM_COUPON_HIDE_QR_CODE) {

            try {
                bitmap = TextToImageEncode(code);
                //Convert to byte array
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                args.putByteArray("key_qr_code", byteArray);
                args.putString("key_qr_text", code);
                DialogRedeem dialogRedeem = new DialogRedeem();
                dialogRedeem.setArguments(args);
                dialogRedeem.show(manager, "sslaam");
//                    imageView.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

        }else {
            args.putString("key_qr_text", code);
            DialogRedeem dialogRedeem = new DialogRedeem();
            dialogRedeem.setArguments(args);
            dialogRedeem.show(manager, "salaamm");
        }

    }


    ////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
    public void LocationsLoaded(List<MyLocation> locations){

        for (MyLocation myLoc : locations){
//            Marker marker = mGoogleMap.addMarker(new MarkerOptions()
            Marker marker = App.mGoogleMap.addMarker(new MarkerOptions()
                    .position(myLoc.getLatLng())
                    .title(myLoc.getTitle())
//                    .snippet(myLoc.snippet)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA)));

            nnmms.put(marker.getId().toString(), myLoc.getTitle());
            ids.put(marker.getId().toString(), Integer.parseInt(myLoc.get_id()));
            images.put(marker.getId().toString(), myLoc.getImg());
            mmm.put(marker.getId().toString(), myLoc.getLatLng());

            markerList.add(marker);
        }

        if(!locations.isEmpty()) {
//            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(locations.get(0).getLatLng(), 13));
            App.mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(locations.get(0).getLatLng(), 13));
          /* if(markerList.size()==1) {
               markerList.get(0).showInfoWindow();
           }*/

        }else{
            Toast.makeText(getInstance(), "Nothing to display", Toast.LENGTH_LONG).show();
        }
//        mGoogleMap.getUiSettings().setMapToolbarEnabled(true);
        App.mGoogleMap.getUiSettings().setMapToolbarEnabled(true);
//        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        App.mGoogleMap.getUiSettings().setZoomControlsEnabled(true);

//        mGoogleMap.setPadding(0,0,6,96);

//        progressDialog.dismiss();


        /*mGoogleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });*/

        PopupAdapter infowin = new PopupAdapter(getInstance().getApplicationContext(),layoutInflater,images);
//        mGoogleMap.setInfoWindowAdapter(infowin);
        App.mGoogleMap.setInfoWindowAdapter(infowin);


//        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
        App.mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

//                marker.showInfoWindow();
//                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mmm.get(marker.getId()), 10));
//                return true; // true potta taan work pannum
                return false;
            }
        });

//        mGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
        App.mGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
//                Toast.makeText(getInstance(), ""+ids.get(marker.getId()),Toast.LENGTH_SHORT).show();
                Intent transitionIntent = new Intent(appmyContext, OutletCoupons.class);
                transitionIntent.putExtra(OutletCoupons.EXTRA_PARAM_STORE_ID, ""+ids.get(marker.getId()));
                transitionIntent.putExtra(OutletCoupons.EXTRA_PARAM_STORE_NAME, nnmms.get(marker.getId()));
                appmyContext.startActivity(transitionIntent);
            }
        });

    }


public void testing(){
//    CouponDetailsActivity.inflateDialogClaim(manager); // also working
    DialogCliam dialogCliam = new DialogCliam();
    dialogCliam.show(manager,"slaam");
}


    Bitmap TextToImageEncode(String Value) throws WriterException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    Value,
                    BarcodeFormat.DATA_MATRIX.QR_CODE,
                    QRcodeWidth, QRcodeWidth, null
            );

        } catch (IllegalArgumentException Illegalargumentexception) {

            return null;
        }
        int bitMatrixWidth = bitMatrix.getWidth();

        int bitMatrixHeight = bitMatrix.getHeight();

        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth; x++) {

                pixels[offset + x] = bitMatrix.get(x, y) ?
                        getResources().getColor(R.color.QRCodeBlackColor):getResources().getColor(R.color.QRCodeWhiteColor);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }

}