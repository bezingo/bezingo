package com.majithg.rewardago.util;

/**
 * Created by majithg on 27-Jul-16.
 */
public interface FragmentLifecycle {

    public void onPauseFragment();
    public void onResumeFragment();
}
