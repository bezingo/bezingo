
package com.majithg.rewardago.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
//import com.facebook.FacebookSdk;
import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.majithg.rewardago.helper.SQLiteHandler;
import com.majithg.rewardago.helper.SessionManager;
import com.majithg.rewardago.util.AlertDialogFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

public class LoginActivity extends Activity {
    private static final String TAG = RegisterActivity.class.getSimpleName();
    private Button btnLogin;
//    private Button btnLinkToRegister;
    private TextView btnLinkToRegister;
    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;

    private TextView forgot;
    private boolean error = true;
    private String duid;
    private String dname;
    private String dname2;
    private String duser_contact;
    private String demail;
    private String dcreated_at;
    private ProgressDialog progressDialog;
    private String jsonData = null;
//    private String pass_code = "";
    private boolean errorStatus = true;
    private String jsonData2;
    private String jsonLoggin;
    private String responseUID;
    private String jsonException;
    private String errorMsg;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_login);

//        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main2);

        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLinkToRegister = (TextView) findViewById(R.id.btnLinkToRegisterScreen);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // Session manager
        session = new SessionManager(getApplicationContext());

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
//            Intent intent = new Intent(LoginActivity.this, MainActivity1.class);
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }

        // Login button Click Event
        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {

                if (!validateForm()) {
                    return;
                }

//                String email = inputEmail.getText().toString().trim();
//                String password = inputPassword.getText().toString().trim();
                String email = inputEmail.getText().toString();
                String password = inputPassword.getText().toString();

                // Check for empty data in the form
                if (!email.isEmpty() && !password.isEmpty()) {
                    // login user
//                    checkLogin(email, password);
                    logIn(email, password);
//                    logIn(inputEmail.getText().toString(), inputPassword.getText().toString());
                } else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please enter the credentials!", Toast.LENGTH_LONG)
                            .show();
                }
            }

        });

        // Link to Register Screen
        btnLinkToRegister.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),
                        RegisterActivity.class);
                startActivity(i);
                finish();
            }
        });

        forgot = (TextView) findViewById(R.id.forgot);
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgotPassword.class);
                startActivity(intent);
            }
        });

    }

    /**
     * function to verify login details in mysql db
     * */
  /*  private void checkLogin(final String email, final String password) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        pDialog.setMessage("Logging in ...");
        showDialog();

        StringRequest strReq = new StringRequest(Method.POST,
                AppConfig.URL_LOGIN,
                new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.d(TAG, "Login Response: " + response.toString());
//                Toast.makeText(getApplicationContext(), "Login Response: "+response.toString(), Toast.LENGTH_LONG).show();
                hideDialog();

                try {


                    jsonData2 = response.toString();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(LoginActivity.this, "Login Response: "+jsonData2,Toast.LENGTH_LONG).show();
                        }
                    });


                    JSONObject jObj = new JSONObject(response);
                    JSONArray jArr = jObj.getJSONArray("Login");
                    JSONObject ary0 = jArr.getJSONObject(0);

//                    boolean error = jObj.getBoolean("error");
                    error = ary0.getBoolean("error");

//                    Toast.makeText(getApplicationContext(), "error status: "+error, Toast.LENGTH_SHORT).show();

                    // Check for error node in json
                    if (!error) {
                            error = true;

                        if(ary0.getInt("subscription") == 0){
                            pass_code = ary0.getString("user_code");
                            Toast.makeText(getApplicationContext(), pass_code, Toast.LENGTH_SHORT).show();

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

//                                    if(!pass_code.isEmpty()){
//                                        showPassCodeDialog();
//                                    }else {
//                                        Toast.makeText(getApplicationContext(), "please try to login", Toast.LENGTH_SHORT).show();
//                                    }
                                    showPassCodeDialog();
                                }
                            });


                        }else if(ary0.getInt("subscription") == 1) {

                            duid = ary0.getString("uid");
                            dname = ary0.getString("fname");
                            dname2 = ary0.getString("lname");
                            demail = ary0.getString("user_email");
                            duser_contact = ary0.getString("user_contact");
                            dcreated_at = ary0.getString("added_date");

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    // user successfully logged in
                                    // Create login session
                                    session.setLogin(true);
                                    // Now store the user in SQLite

                                    // Inserting row in users table
                                    db.addUser(dname, dname2, duser_contact, demail, duid, dcreated_at);
                                    // Launch main activity
//                        Intent intent = new Intent(LoginActivity.this, MainActivity1.class);
                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();

                                }
                            });


                        }else {
                            Toast.makeText(getApplicationContext(), "something went wrong", Toast.LENGTH_SHORT).show();
                        }


                    } else {
                        // Error in login. Get the error message
                        String errorMsg = ary0.getString("error_msg");
//                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getApplicationContext(), "Invalid mail or password", Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    // JSON error
//                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "JSONException: "+ e.getMessage()+"\nproblem with json parsing", Toast.LENGTH_SHORT).show();
                    Toast.makeText(getApplicationContext(), "Invalid mail or password", Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "onErrorResponse: "+error.getMessage(), Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(), "please try again", Toast.LENGTH_SHORT).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", password);

                return params;
            }

        };

        strReq.setShouldCache(false);

        // Adding request to request queue
        App.getInstance().addToRequestQueue(strReq, tag_string_req);
    }*/

    private boolean validateForm() {
        boolean valid = true;

        String email = inputEmail.getText().toString();
        String password = inputPassword.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            inputEmail.setError("enter a valid email address");
            valid = false;
        } else {
            inputEmail.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            inputPassword.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            inputPassword.setError(null);
        }

        return valid;
    }

    private void showPassCodeDialog() {
        new MaterialDialog.Builder(LoginActivity.this)
                .title("Verify your Number")
                .content("Please enter the code that you received")
//                .inputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS)
                .inputType(InputType.TYPE_CLASS_NUMBER)
                .input("12345", null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        // Do something
//                                    Toast.makeText(getApplicationContext(), input.toString(), Toast.LENGTH_SHORT).show();
//                        if (input.toString().contentEquals(pass_code)){
//                            getForecast(duid, pass_code);


/*                            //TODO allow to rigister...
//                            Toast.makeText(RegisterActivity.this, "status: "+input.toString().contentEquals(pass_code)
//                                    ,Toast.LENGTH_LONG).show();
//                            registerUser(Rname, Rname2, Remail, Rcontact, Rpassword);
                            Toast.makeText(getApplicationContext(), "User successfully registered. Try login now!", Toast.LENGTH_SHORT).show();
                            // Launch login activity
                            Intent intent = new Intent(LoginActivity.this,LoginActivity.class);
                            startActivity(intent);
                            finish();*/

//                        }else {
//                            Toast.makeText(LoginActivity.this, "Invalid code!",Toast.LENGTH_LONG).show();
//                        }

                        if(!input.toString().isEmpty()) {
                            getForecast(responseUID, input.toString());
//                            Toast.makeText(LoginActivity.this, responseUID+" "+input.toString(),Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(LoginActivity.this, "please enter code",Toast.LENGTH_SHORT).show();
                        }
                    }
                }).show();
    }

    private void getForecast(String uid, String user_code) {
        progressDialog = new ProgressDialog(this);
//        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Verifying your mobile number ...");
        progressDialog.show();

        String forecastUrl = "http://rewardago.com/cron/login.php?id="+uid
                +"&code="+user_code;
//                +"?_=" + System.currentTimeMillis();


        if (isNetworkAvailable()){

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {
                    progressDialog.dismiss();
                    alertUserAboutError();
                }

                @Override
                public void onResponse(com.squareup.okhttp.Response response) throws IOException {

                    try {
                        progressDialog.dismiss();
//                        String jsonData = response.body().string();
                        jsonData = response.body().string();
//                        jsonData = stripHtml(response.body().string());

                       /* runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(LoginActivity.this, "Verify Response: "+jsonData,Toast.LENGTH_LONG).show();
                            }
                        });*/

//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {
                            getCurrentDetails(jsonData);


                        } else {
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
//                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                        progressDialog.dismiss();
                    }
                    catch (JSONException e) {
//                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                        progressDialog.dismiss();
                      jsonException = e.getMessage();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(LoginActivity.this, "JSONException: "+jsonException,Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }
            });
        }

        else {
            Toast.makeText(this, getString(R.string.network_unavailable_message),Toast.LENGTH_LONG).show();
//            mSwipeRefreshWidget.setRefreshing(false);   // i think.. an error may occur here, if we put
//            progressBar.setVisibility(View.GONE);
            progressDialog.dismiss();
        }

    }

    private void getCurrentDetails(String jsonData) throws JSONException {

        JSONObject forecast = new JSONObject(jsonData);
        JSONArray currently  = forecast.getJSONArray("Login");
        JSONObject post = currently.getJSONObject(0);

        errorStatus = post.getBoolean("error");
        if(!errorStatus){
            // Now store the user in SQLite
//                        String uid = jObj.getString("uid");
            duid = post.getString("user_id");
            dname = post.getString("fname");
            dname2 = post.getString("lname");
            demail = post.getString("user_email");
            duser_contact = post.getString("user_contact");
            dcreated_at = post.getString("added_date");
        }


        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                if(!errorStatus){
                    errorStatus = true;

//                    Toast.makeText(LoginActivity.this,dname+"\n"+dname2+"\n"+duser_contact+"\n"+demail+"\n"+duid+"\n"+dcreated_at,Toast.LENGTH_LONG).show();

                    // user successfully logged in
                    // Create login session
                    session.setLogin(true);
                    // Inserting row in users table
                    db.addUser(dname, dname2, duser_contact, demail, duid, dcreated_at);
                    // Launch main activity
//                        Intent intent = new Intent(LoginActivity.this, MainActivity1.class);
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }else {
                    Toast.makeText(LoginActivity.this, "something went wrong",Toast.LENGTH_LONG).show();
                }


            }

        });

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if (networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;

        }

        return isAvailable;
    }

    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(getFragmentManager(), "error_dialog");
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

   /* @Override
    protected void onStop() {
        super.onStop();
        pass_code="";
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        pass_code="";
    }

    @Override
    protected void onStart() {
        super.onStart();
        pass_code = "";
    }*/


    private void logIn(String eeemail, String pppassword) {

//        Toast.makeText(getApplicationContext(), eeemail+" "+pppassword, Toast.LENGTH_SHORT).show();

        progressDialog = new ProgressDialog(this);
//        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("please wait...");
        progressDialog.show();

        String forecastUrl = "http://rewardago.com/cron/login.php?email="+eeemail+"&pass="+pppassword;
//                +"?_=" + System.currentTimeMillis();


        if (isNetworkAvailable()){

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {
                    progressDialog.dismiss();
                    alertUserAboutError();
                }

                @Override
                public void onResponse(com.squareup.okhttp.Response response) throws IOException {

                    try {
                        progressDialog.dismiss();
//                        String jsonData = response.body().string();
                        jsonLoggin = response.body().string();
//                        jsonData = stripHtml(response.body().string());

                        /*runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(LoginActivity.this, "Login Response: "+jsonLoggin,Toast.LENGTH_LONG).show();
                            }
                        });*/

//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {
                            getlogging(jsonLoggin);


                        } else {
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                        progressDialog.dismiss();
                    }
                    catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                        progressDialog.dismiss();
                    }
                }
            });
        }

        else {
            Toast.makeText(this, getString(R.string.network_unavailable_message),Toast.LENGTH_LONG).show();
//            mSwipeRefreshWidget.setRefreshing(false);   // i think.. an error may occur here, if we put
//            progressBar.setVisibility(View.GONE);
            progressDialog.dismiss();
        }

    }

    private void getlogging(String jsonData) throws JSONException {

        JSONObject jObj = new JSONObject(jsonData);
        JSONArray jArr = jObj.getJSONArray("Login");
        JSONObject ary0 = jArr.getJSONObject(0);

//                    boolean error = jObj.getBoolean("error");
        error = ary0.getBoolean("error");


        // Check for error node in json
        if (!error) {
            error = true;


            if(ary0.getInt("subscription") == 0){
//                pass_code = ary0.getString("user_code");
                responseUID = ary0.getString("uid");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

//                        Toast.makeText(getApplicationContext(), responseUID, Toast.LENGTH_SHORT).show();

//                        Toast.makeText(getApplicationContext(), pass_code, Toast.LENGTH_SHORT).show();

//                                    if(!pass_code.isEmpty()){
//                                        showPassCodeDialog();
//                                    }else {
//                                        Toast.makeText(getApplicationContext(), "please try to login", Toast.LENGTH_SHORT).show();
//                                    }
                        showPassCodeDialog();
                    }
                });


            }else if(ary0.getInt("subscription") == 1) {
                duid = ary0.getString("uid");
                dname = ary0.getString("fname");
                dname2 = ary0.getString("lname");
                demail = ary0.getString("user_email");
                duser_contact = ary0.getString("user_contact");
                dcreated_at = ary0.getString("added_date");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // user successfully logged in
                        // Create login session
                        session.setLogin(true);
                        // Now store the user in SQLite
                        // Inserting row in users table
                        db.addUser(dname, dname2, duser_contact, demail, duid, dcreated_at);
                        // Launch main activity
//                        Intent intent = new Intent(LoginActivity.this, MainActivity1.class);
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });

            }else {
                Toast.makeText(getApplicationContext(), "something went wrong", Toast.LENGTH_SHORT).show();
            }


        } else {

            // Error in login. Get the error message
            errorMsg = ary0.getString("error_msg");
//            Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getApplicationContext(), "Invalid mail or password", Toast.LENGTH_LONG).show();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();
//                    Toast.makeText(getApplicationContext(), "Invalid mail or password", Toast.LENGTH_SHORT).show();
                }
            });
        }



    }









}
