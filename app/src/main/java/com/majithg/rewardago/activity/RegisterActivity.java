
package com.majithg.rewardago.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.majithg.rewardago.helper.SQLiteHandler;
import com.majithg.rewardago.helper.SessionManager;
import com.majithg.rewardago.util.App;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

//import info.androidhive.loginandregistration.R;
//import info.androidhive.loginandregistration.app.AppConfig;
//import info.androidhive.loginandregistration.app.AppController;
//import info.androidhive.loginandregistration.helper.SQLiteHandler;
//import info.androidhive.loginandregistration.helper.SessionManager;

public class RegisterActivity extends Activity {
    private static final String TAG = RegisterActivity.class.getSimpleName();
    private Button btnRegister;
    //    private Button btnLinkToLogin;
    private TextView btnLinkToLogin;
    private EditText lname;
    private EditText user_contact;
    private EditText fname;
    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;
    private ProgressDialog progressDialog;
    private String jsonData;
    private boolean errorStatus = true;
//    private String pass_code = "";
    private String Rname;
    private String Rname2;
    private String Rcontact;
    private String Remail;
    private String Rpassword;
    private String jsonData2;
    private boolean error = true;
    private String duid;
    private String dname;
    private String dlname;
    private String dcontact;
    private String demail;
    private String dcreated_at;

    // Server user login url
//	public static String URL_LOGIN = "http://192.168.0.102/android_login_api/login.php";
    public static String URL_LOGIN = "http://rewardago.com/cron/login.php"
            +"?_=" + System.currentTimeMillis();

    // Server user register url
//	public static String URL_REGISTER = "http://192.168.0.102/android_login_api/register.php";
    public static String URL_REGISTER = "http://rewardago.com/cron/reg.php"
            +"?_=" + System.currentTimeMillis();
    private String errorMsg;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_register);
        setContentView(R.layout.activity_create_account);

        fname = (EditText) findViewById(R.id.fname);
        lname = (EditText) findViewById(R.id.lname);
        user_contact = (EditText) findViewById(R.id.user_contact);
        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnLinkToLogin = (TextView) findViewById(R.id.btnLinkToLoginScreen);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // Session manager
        session = new SessionManager(getApplicationContext());

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
//            Intent intent = new Intent(RegisterActivity.this, MainActivity1.class);
            Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        // Register Button Click event
        btnRegister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                if (!validateForm()) {
                    return;
                }

                Rname = fname.getText().toString().trim();
                Rname2 = lname.getText().toString().trim();
                Rcontact = user_contact.getText().toString().trim();
                Remail = inputEmail.getText().toString().trim();
                Rpassword = inputPassword.getText().toString().trim();

                if (!Rname.isEmpty() && !Rname2.isEmpty() && !Rcontact.isEmpty() && !Remail.isEmpty() && !Rpassword.isEmpty()) {

//                    int chk = Integer.parseInt(first.toString().trim());
                    /*if( Rcontact.startsWith("077") || Rcontact.startsWith("076") ){
                        //TODO DIALOG
//                        send request to verification code
//                        pass_code ="12345";
                        if(pass_code.isEmpty()){
                            getForecast(Rcontact);
                        }else {
                            showPassCodeDialog();
                        }

//

                    }else {
//                        Toast.makeText(getApplicationContext(), "from else...", Toast.LENGTH_SHORT).show();
                        registerUser(Rname, Rname2, Remail, Rcontact, Rpassword);
                    }*/
//                    pass_code ="12345";
                    /*if(!pass_code.isEmpty()){
                        showPassCodeDialog();
                        return;
                    }*/
                    areYouAgree();
//                    registerUser(Rname, Rname2, Remail, Rcontact, Rpassword);
//
                } else {
                    Toast.makeText(getApplicationContext(), "Please enter your details!", Toast.LENGTH_LONG).show();
                }
            }
        });

        // Link to Login Screen
        btnLinkToLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
                finish();
            }
        });

    }

    private void areYouAgree() {

        new MaterialDialog.Builder(this)
                .title(R.string.title)
                .customView(R.layout.custom_view, true)
//                .content(R.string.loremIpsum)
//                .backgroundColor(getResources().getColor(R.color.primary))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                        Toast.makeText(RegisterActivity.this, "AGREE",Toast.LENGTH_SHORT).show();
                        registerUser(Rname, Rname2, Remail, Rcontact, Rpassword);
                    }
                })
//                .onNegative(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                        Toast.makeText(RegisterActivity.this, "DISAGREE",Toast.LENGTH_SHORT).show();
//                    }
//                })
                .positiveText("AGREE")
                .negativeText("DISAGREE")
                .show();

    }

    /**
     * Function to store user in MySQL database will post params(tag, name,
     * email, password) to register url
     */
    private void registerUser(final String fname,
                              final String lname,
                              final String email,
                              final String contact,
                              final String password) {
        // Tag used to cancel the request
        String tag_string_req = "req_register";

        pDialog.setMessage("Registering ...");
        showDialog();

//        StringRequest strReq = new StringRequest(Method.POST, AppConfig.URL_REGISTER, new Response.Listener<String>() {
        StringRequest strReq = new StringRequest(Method.POST, URL_REGISTER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                Log.d(TAG, "Register Response: " + response.toString());
//                Toast.makeText(getApplicationContext(), "Register Response: "+response.toString(), Toast.LENGTH_SHORT).show();

                hideDialog();

                try {

                    jsonData2 = response.toString();

                    /*runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(RegisterActivity.this, "Register Response: "+jsonData2,Toast.LENGTH_LONG).show();
                        }
                    });*/

                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("User");
                    JSONObject jObj = jsonArray.getJSONObject(0);

                    error = jObj.getBoolean("error");
//                    Toast.makeText(getApplicationContext(), ""+error, Toast.LENGTH_SHORT).show();
                    if (!error) {
                        // User successfully stored in MySQL
                        // Now store the user in sqlite
                        duid = jObj.getString("user_id");

//                        JSONObject user = jObj.getJSONObject("User");
                        dname = jObj.getString("fname");
                        dlname = jObj.getString("lname");
                        dcontact = jObj.getString("user_contact");
                        demail = jObj.getString("user_email");
                        dcreated_at = jObj.getString("added_date");


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                // Inserting row in users table
                                db.addUser(dname, dlname, dcontact, demail, duid, dcreated_at);


                               /* if( Rcontact.startsWith("077") || Rcontact.startsWith("076") ){
                                    getForecast(duid);
                                }else{
                                    Toast.makeText(getApplicationContext(), "User successfully registered. Try login now!", Toast.LENGTH_SHORT).show();
                                    // Launch login activity
                                    Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }*/
                                Toast.makeText(getApplicationContext(), "User successfully registered. Try login now!", Toast.LENGTH_SHORT).show();
                                // Launch login activity
                                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                startActivity(intent);
                                finish();

                            }
                        });


                    } else {
//                        pass_code = "";
                        // Error occurred in registration. Get the error
                        // message
                       errorMsg = jObj.getString("error_msg");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                            }
                        });


                    }




                } catch (JSONException e) {
                    e.printStackTrace();
//                    Toast.makeText(getApplicationContext(), "something went wrong!", Toast.LENGTH_SHORT).show();
                    Toast.makeText(getApplicationContext(), "JSONException: "+e.getMessage()+"\nproblem with json parsing", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "Registration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "onErrorResponse: "+error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();


            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("fname", fname);
                params.put("lname", lname);
                params.put("email", email);
                params.put("contact", contact);
                params.put("password", password);

                return params;
            }

        };

        // Adding request to request queue
        App.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private boolean validateForm() {
        boolean valid = true;

        String mfname = fname.getText().toString();
        String mlname = lname.getText().toString();
        String email = inputEmail.getText().toString();
        String number = user_contact.getText().toString();
        String password = inputPassword.getText().toString();

        if (mfname.isEmpty() || mfname.length() < 3) {
            fname.setError("at least 3 characters");
            valid = false;
        } else {
            fname.setError(null);
        }

        if (mlname.isEmpty() || mlname.length() < 3) {
            lname.setError("at least 3 characters");
            valid = false;
        } else {
            lname.setError(null);
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            inputEmail.setError("enter a valid email address");
            valid = false;
        } else {
            inputEmail.setError(null);
        }


        if (number.isEmpty() || number.length() < 10 || number.length() > 10) {
            user_contact.setError("enter a valid phone number (10 digits)");
            valid = false;
        } else {
            user_contact.setError(null);
        }


        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            inputPassword.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            inputPassword.setError(null);
        }

        return valid;
    }


/*    @Override
    protected void onStop() {
        super.onStop();
        pass_code="";
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        pass_code="";
    }

    @Override
    protected void onStart() {
        super.onStart();
        pass_code="";
    }*/
}
