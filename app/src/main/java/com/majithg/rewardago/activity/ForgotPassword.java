package com.majithg.rewardago.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.majithg.rewardago.R;
import com.majithg.rewardago.home.content.Place;
import com.majithg.rewardago.home.content.TravelListAdapter;
import com.majithg.rewardago.util.AlertDialogFragment;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//import info.androidhive.loginandregistration.R;

public class ForgotPassword extends AppCompatActivity {


    private static final String TAG = ForgotPassword.class.getSimpleName();
//    @Bind(R.id.input_number)
    EditText _numberText;

//    @Bind(R.id.input_send)
    Button _sendButton;
//    private List<Place> mCurrentWeather = new ArrayList<>();
    private String jsonData;
    private String userId;
    ProgressDialog progressDialog;
    private boolean errorStatus = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        _numberText = (EditText) findViewById(R.id.input_number);
        _sendButton = (Button) findViewById(R.id.input_send);

//        ButterKnife.bind(this);

        _sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Send();
            }
        });
    }

    public void Send() {
        Log.d(TAG, "Send");

        if (!validate()) {
            onSignupFailed();
            return;
        }

//        _sendButton.setEnabled(false);

       progressDialog = new ProgressDialog(ForgotPassword.this
//                ,R.style.AppTheme_Dark_Dialog
        );
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Sending Code...");
        progressDialog.show();

//        String number = _numberText.getText().toString();

        // TODO: Implement your own signup logic here.

       /* new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onSignupSuccess or onSignupFailed
                        // depending on success
                        onSignupSuccess();
                        // onSignupFailed();
                        progressDialog.dismiss();
                    }
                }, 3000);*/
        onSignupSuccess();
    }

    public void onSignupSuccess() {
//        _sendButton.setEnabled(true);

        getForecast(_numberText.getText().toString());

//        setResult(RESULT_OK, null);
//        startActivity(new Intent(this, Reset.class));
//        finish();

    }



    public void onSignupFailed() {
//        Toast.makeText(getBaseContext(), "Sending failed", Toast.LENGTH_LONG).show();

        _sendButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String number = _numberText.getText().toString();

        if (number.isEmpty() || number.length() < 10 || number.length() > 10) {
            _numberText.setError("enter a valid phone number (10 digits)");
            valid = false;
        } else {
            _numberText.setError(null);
        }

        return valid;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity1.class);
        startActivity(intent);
        finish();
    }







    private void getForecast(String mobile) {

        String forecastUrl = "http://rewardago.com/cron/chg_pass.php?mob="+mobile;


        if (isNetworkAvailable()){

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {
                    progressDialog.dismiss();
                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
                        progressDialog.dismiss();
//                        String jsonData = response.body().string();
                        jsonData = response.body().string();
//                        jsonData = stripHtml(response.body().string());

//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                Toast.makeText(ForgotPassword.this, jsonData,Toast.LENGTH_LONG).show();
//                            }
//                        });

//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {
                            getCurrentDetails(jsonData);


                        } else {
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                        progressDialog.dismiss();
                    }
                    catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                        progressDialog.dismiss();
                    }
                }
            });
        }

        else {
            Toast.makeText(ForgotPassword.this, getString(R.string.network_unavailable_message),Toast.LENGTH_LONG).show();
//            mSwipeRefreshWidget.setRefreshing(false);   // i think.. an error may occur here, if we put
//            progressBar.setVisibility(View.GONE);
            progressDialog.dismiss();
        }

    }

    private void getCurrentDetails(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);

//        JSONArray currently  = forecast.getJSONArray("Code");

//        ArrayList<HashMap<String, String>> blogCoupons = new ArrayList<HashMap<String, String>>();

//        JSONObject post = currently.getJSONObject(0);

        errorStatus = forecast.getBoolean("error");
        if(!errorStatus){
            userId = forecast.getString("uid");
//        String code = forecast.getString("pass_code");
        }


        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if(!errorStatus){

                    errorStatus = true;
                    Intent intent = new Intent(ForgotPassword.this, Reset.class);
                    intent.putExtra("uid", userId);
                    setResult(RESULT_OK, intent);
                    startActivity(intent);

                    finish();
                }else {
                    Toast.makeText(ForgotPassword.this, "something went wrong!",Toast.LENGTH_LONG).show();
                }


            }

        });

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if(networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;
        }

        return isAvailable;
    }

    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(getFragmentManager(), "error_dialog");
    }
}
