package com.majithg.rewardago.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.majithg.rewardago.R;
import com.majithg.rewardago.util.AlertDialogFragment;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

//import info.androidhive.loginandregistration.R;

//import butterknife.Bind;
//import butterknife.ButterKnife;

public class Reset extends AppCompatActivity {


//    @Bind(R.id.input_confirmation_code)
    EditText _codeText;

//    @Bind(R.id.input_password)
    EditText _passwordText;

//    @Bind(R.id.input_confirm_password)
    EditText _confirmpasswordText;

//    @Bind(R.id.btn_signin)
    Button _signinButton;

    private String TAG = Reset.class.getSimpleName();

    ProgressDialog progressDialog;
    private String jsonData;
    private boolean errorStatus = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset);

        //        ButterKnife.bind(this);

//        final ProgressDialog progressDialog = new ProgressDialog(Reset.this,R.style.AppTheme_Dark_Dialog);
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");

        _codeText = (EditText) findViewById(R.id.input_confirmation_code);
        _passwordText = (EditText) findViewById(R.id.input_password);
        _confirmpasswordText = (EditText) findViewById(R.id.input_confirm_password);
        _signinButton = (Button) findViewById(R.id.btn_signin);

        _signinButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }

//        _signinButton.setEnabled(false);


        progressDialog.show();

        String code = _codeText.getText().toString();
        String password = _passwordText.getText().toString();
        String confirmpassword = _confirmpasswordText.getText().toString();

        Bundle bundle = getIntent().getExtras();
        String uid = bundle.getString("uid");


        // TODO: Implement your own authentication logic here.

       /* new Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
//                        onLoginSuccess();
                        // onLoginFailed();
//                        progressDialog.dismiss();
                    }
                }, 3000);*/

        getForecast(uid,code,password,confirmpassword);

    }

    /*@Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }*/

    public void onLoginSuccess() {
        _signinButton.setEnabled(true);
        // TODO do it later
        startActivity(new Intent(this, MainActivity1.class));
        finish();
    }

    public void onLoginFailed() {
//        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _signinButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String code = _codeText.getText().toString();
        String password = _passwordText.getText().toString();
        String confirmpassword = _confirmpasswordText.getText().toString();

        if (code.isEmpty() ) {
            _codeText.setError("enter a valid code");
            valid = false;
        } else {
            _codeText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        if (confirmpassword.isEmpty() || !confirmpassword.matches(password)) {
            _confirmpasswordText.setError("Confirm password is not the same as password");
            valid = false;
        } else {
            _confirmpasswordText.setError(null);
        }



        return valid;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, ForgotPassword.class);
        startActivity(intent);
        finish();
    }




    private void getForecast(String uid, String pass_code, String newpass, String retype_pass ) {

        String forecastUrl = "http://rewardago.com/cron/chg_pass.php?id="+uid
                +"&cod="+pass_code+"&pass="+newpass+"&rpass="+retype_pass;


        if (isNetworkAvailable()){

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            toggleRefresh();
//                            progressBar.setVisibility(View.GONE);
//                            mSwipeRefreshWidget.setRefreshing(false);
                            progressDialog.dismiss();
                        }
                    });

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
                        progressDialog.dismiss();
//                        String jsonData = response.body().string();
                        jsonData = response.body().string();
//                        jsonData = stripHtml(response.body().string());

                       /* runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(Reset.this, jsonData,Toast.LENGTH_LONG).show();
                            }
                        });*/

//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {
                            getCurrentDetails(jsonData);


                        } else {
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                        progressDialog.dismiss();
                    }
                    catch (JSONException e) {
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                        progressDialog.dismiss();
                    }
                }
            });
        }

        else {
            Toast.makeText(Reset.this, getString(R.string.network_unavailable_message),Toast.LENGTH_LONG).show();
//            mSwipeRefreshWidget.setRefreshing(false);   // i think.. an error may occur here, if we put
//            progressBar.setVisibility(View.GONE);
            progressDialog.dismiss();
        }

    }

    private void getCurrentDetails(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);
//        JSONArray currently  = forecast.getJSONArray("ChangePass");
//        JSONObject post = currently.getJSONObject(0);
        errorStatus = forecast.getBoolean("error");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // On complete call either onLoginSuccess or onLoginFailed
if(!errorStatus){
    errorStatus = true;
    _signinButton.setEnabled(false);
    onLoginSuccess();
}else {
//    _signinButton.setEnabled(true);
    Toast.makeText(Reset.this, "Invalid Code!",Toast.LENGTH_LONG).show();
}


            }
        });

//        return progressDialog;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if(networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;

        }

        return isAvailable;
    }

    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(getFragmentManager(), "error_dialog");
    }
}
