package com.majithg.rewardago;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.google.android.gms.maps.model.Marker;
import com.majithg.rewardago.activity.LoginActivity;
import com.majithg.rewardago.dashboard.Person;
import com.majithg.rewardago.discover.Search;
import com.majithg.rewardago.favourite.Favourite;
import com.majithg.rewardago.helper.SQLiteHandler;
import com.majithg.rewardago.helper.SessionManager;
import com.majithg.rewardago.home.Home;
import com.majithg.rewardago.home.content.CouponDetailsActivity;
import com.majithg.rewardago.mycoupons.Offers;
import com.majithg.rewardago.outlets.outlets;
import com.majithg.rewardago.search.SearchAdapter;
//import com.majithg.rewardago.search.SharedPreference;
import com.majithg.rewardago.search.Utils;
import com.majithg.rewardago.util.AlertDialogFragment;
import com.majithg.rewardago.util.App;
import com.majithg.rewardago.util.CircleImageView;
import com.majithg.rewardago.util.FragmentLifecycle;
import com.majithg.rewardago.util.MainViewPager;
import com.majithg.rewardago.util.MainViewPagerAdapter;
//import com.majithg.rewardago.util.OnBackPressedListener;
import com.majithg.rewardago.util.SliderLayout;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//import android.app.FragmentManager;



public class MainActivity extends AppCompatActivity
//        implements OnBackPressedListener
//        implements
//        TabLayout.OnTabSelectedListener
{

    public static String UID;

    public static Context contextOfApplication;
    public static AppCompatActivity appCompatActivity;

    public static Toolbar toolbar;
    public static MainViewPagerAdapter fAdapter;
    public static AHBottomNavigation bottomNavigation;
//    public static dep_HomeHome f1;
//    public static MainViewPager vp_message_main;
    public static TextView userid;
    public static TextView fName;
    public static TextView lName;
    public static TextView ContactName;
    public static TextView txtEmail;

//    TabLayout layout_tab;
    public static Button btnLogout;
    public static SQLiteHandler db;
    public static SessionManager session;
//   public static CircleImageView profile_image;
    SliderLayout sliderLayout;
    MenuItem menuItem;


    private ArrayList<String> mIds;
    private ArrayList<String> mCountries;
    private Dialog toolbarSearchDialog;

//    Map<ArrayList<String>,String> map = new HashMap();


    private android.app.FragmentManager fragmentManager;


     MainViewPager.OnPageChangeListener pageChangeListener = new MainViewPager.OnPageChangeListener() {

        int currentPosition = 0;

        @Override
        public void onPageSelected(int newPosition) {

            FragmentLifecycle fragmentToShow = (FragmentLifecycle) fAdapter.getItem(newPosition);
            fragmentToShow.onResumeFragment();

            FragmentLifecycle fragmentToHide = (FragmentLifecycle)fAdapter.getItem(currentPosition);
            fragmentToHide.onPauseFragment();

            currentPosition = newPosition;
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) { }

        public void onPageScrollStateChanged(int arg0) { }
    };
    private int gpsoff;
    private List<Fragment> list_fragment;
    private ListView lvMenu;
    private String[] lvMenuItems;
    private String currentWeather;
    private String mCurrentWeather;
    private outlets fone;
    private Target target;
    private String jsonData;

    public static Context getContextOfApplication() {
        return contextOfApplication;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);

        fragmentManager = getFragmentManager();
        App.manager = fragmentManager;

        sliderLayout = (SliderLayout)this.getLayoutInflater().inflate(R.layout.activity_main, null);
        setContentView(sliderLayout);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
        getSupportActionBar().setHomeButtonEnabled(true);

        fName = (TextView) findViewById(R.id.fName);
        lName = (TextView) findViewById(R.id.lName);
        ContactName = (TextView) findViewById(R.id.ContactName);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
        App.profile_image_main = (CircleImageView) findViewById(R.id.profile_image_main);

        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }


        // Fetching user details from SQLite
        HashMap<String, String> user = db.getUserDetails();

        this.UID = user.get("uid");
        String name = user.get("fname");
        String name2 = user.get("lname");
        String contact = user.get("contact");
        String email = user.get("email");

        // Displaying the user details on the screen
//        userid.setText(uid);
        fName.setText(name);
        lName.setText(name2);
        ContactName.setText(contact);
        txtEmail.setText(email);
//        setImg();
        getForecast();



//        lvMenuItems = getResources().getStringArray(R.array.menu_items); // don't use this way
        lvMenuItems = new String[]{"HOME", "DISCOVER", "OUTLETS", "FAVOURITE", "MY COUPONS", "DASHBOARD", "LOGOUT"};
        lvMenu = (ListView) findViewById(R.id.activity_main_menu_listview);
        lvMenu.setAdapter(new ArrayAdapter<String>(this, R.layout.item, lvMenuItems));


        App.vp_message_main = (MainViewPager) findViewById(R.id.vp_message_main);
        App.vp_message_main.setPagingEnabled(false);
//        vp_message.setPagingEnabled(false);

        Home f0 = Home.newInstance(0);
//        f1 = dep_HomeHome.newInstance(1);
        Search f1 = Search.newInstance(1);
        Favourite f2 = Favourite.newInstance(2);
        Offers f3 = Offers.newInstance(3);
        Person f4 = Person.newInstance(4);


        list_fragment = new ArrayList<>();
        list_fragment.add(f0);
        list_fragment.add(f1);
        list_fragment.add(f2);
        list_fragment.add(f3);
        list_fragment.add(f4);
//        list_fragment.add(f5);


//        fAdapter = new MainViewPagerAdapter(this.getSupportFragmentManager(), list_fragment);
        fAdapter = new MainViewPagerAdapter(getSupportFragmentManager(), list_fragment);
        App.vp_message_main.setAdapter(fAdapter);
//        App.vp_message_main.setCurrentItem(0);
        App.vp_message_main.setOnPageChangeListener(pageChangeListener);



        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
        bottomNavigation.setDefaultBackgroundColor(getResources().getColor(R.color.navcolor));

//        AHBottomNavigationItem item1 = new AHBottomNavigationItem("Green", R.drawable.ic_map_24dp, Color.parseColor(colors[0]));
//        AHBottomNavigationItem item2 = new AHBottomNavigationItem("Red", R.drawable.ic_local_restaurant_24dp, Color.parseColor(colors[1]));
//        AHBottomNavigationItem item3 = new AHBottomNavigationItem("Blue", R.drawable.ic_store_mall_directory_24dp, Color.parseColor(colors[2]));

        AHBottomNavigationItem item1 = new AHBottomNavigationItem(null,R.drawable.ic_home_grey_400_24dp);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(null,R.drawable.ic_search_grey_400_24dp);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(null,R.drawable.ic_favorite_grey_400_24dp);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(null,R.drawable.ic_local_offer_grey_400_24dp);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem(null,R.drawable.ic_person_grey_400_24dp);

        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
        bottomNavigation.addItem(item4);
        bottomNavigation.addItem(item5);


        bottomNavigation.setSelected(true);
        bottomNavigation.setCurrentItem(0);


        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                // Do something cool here...

//                fragment.updateColor(Color.parseColor(colors[position]));

//                onTabSelected(0,true);

                switch (position) {
                    case 0:
                        App.vp_message_main.setCurrentItem(0);
                        if(App.HomeCanAddItem==true){
                            App.HomeCanAddItem = false;
                            FragmentManager manager = getSupportFragmentManager();
                            FragmentTransaction transaction = manager.beginTransaction();
//                            transaction.remove(manager.findFragmentByTag("STORE"));
                            transaction.hide(manager.findFragmentByTag("STORE"));
//                            transaction.commit();
                            transaction.commitAllowingStateLoss();
                        }
                        break; //optional
                    case 1:
                        toolbar.getMenu().clear();
                        toolbar.getMenu().add(0, 1, Menu.NONE, null).setIcon(R.drawable.ic_arrow_24dp).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                        App.vp_message_main.setCurrentItem(1);
                        enableGPS();
                        break; //optional
                    case 2:

                        toolbar.getMenu().clear();
                        toolbar.getMenu().add(0, 1, Menu.NONE, null).setIcon(R.drawable.ic_arrow_24dp).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                        App.vp_message_main.setCurrentItem(2);
//                        App.adapter.notifyDataSetChanged();
                        break; //optional
                    case 3:

                        App.vp_message_main.setCurrentItem(3);
//                        App.adapter_offers.notifyDataSetChanged();
                        break; //optional
                    case 4:

                        App.vp_message_main.setCurrentItem(4);
                        if(App.personCanAddItem==true) {
                            App.personCanAddItem = false;
                            FragmentManager manager = getSupportFragmentManager();
                            FragmentTransaction transaction = manager.beginTransaction();
                            if ((manager.findFragmentByTag("MY_REDEMPTION")) != null) {
                                if ((manager.findFragmentByTag("MY_REDEMPTION")).getUserVisibleHint()) {
                                    transaction.remove(manager.findFragmentByTag("MY_REDEMPTION"));
//                                    transaction.hide(manager.findFragmentByTag("MY_REDEMPTION"));
//                            transaction.commit();
                                    transaction.commitAllowingStateLoss();
                                }
                            }
                        }
                        break; //optional
                }

                return true;
            }

            @Override
            public boolean onTabUnSelected(int position, boolean wasSelected) {
                return false;
            }

        });


        lvMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String selectedItem = lvMenuItems[position];

                if (selectedItem.compareTo("HOME") == 0) {
                    App.vp_message_main.setCurrentItem(0);
                    bottomNavigation.setCurrentItem(0);
                    if(App.HomeCanAddItem==true){
                        App.HomeCanAddItem = false;
                        FragmentManager manager = getSupportFragmentManager();
                        FragmentTransaction transaction = manager.beginTransaction();
                        transaction.remove(manager.findFragmentByTag("STORE"));
//                        transaction.commit();
                        transaction.commitAllowingStateLoss();
                    }


                } else if (selectedItem.compareTo("DISCOVER") == 0) {
                    enableGPS();
                    App.vp_message_main.setCurrentItem(1);
                    bottomNavigation.setCurrentItem(1);

                } else if (selectedItem.compareTo("OUTLETS") == 0) {
                    bottomNavigation.setCurrentItem(0);
                    App.HomeCanAddItem = true;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            fone = outlets.newInstance(15);
                            FragmentManager manager = getSupportFragmentManager();
                            FragmentTransaction transaction = manager.beginTransaction();
                            transaction.replace(R.id.inside, fone, "STORE");
                            transaction.addToBackStack(fone.getClass().getSimpleName());
//                            transaction.commit();
                            transaction.commitAllowingStateLoss();
                        }
                    }, 50);

                } else if (selectedItem.compareTo("FAVOURITE") == 0) {
                    App.vp_message_main.setCurrentItem(2);
                    bottomNavigation.setCurrentItem(2);

                } else if (selectedItem.compareTo("MY COUPONS") == 0) {
                    App.vp_message_main.setCurrentItem(3);
                    bottomNavigation.setCurrentItem(3);

                } else if (selectedItem.compareTo("DASHBOARD") == 0) {
                    App.vp_message_main.setCurrentItem(4);
                    bottomNavigation.setCurrentItem(4);
                    /*if(App.personCanAddItem==true){
                        App.personCanAddItem = false;
                        FragmentManager manager = getSupportFragmentManager();
                        FragmentTransaction transaction = manager.beginTransaction();
//                        transaction.remove(manager.findFragmentByTag("MY_REDEMPTION"));
                        transaction.hide(manager.findFragmentByTag("MY_REDEMPTION"));
//                        transaction.commit();
                        transaction.commitAllowingStateLoss();
                    }*/

                } else if (selectedItem.compareTo("LOGOUT") == 0) {
                   logoutUser();
                }

                sliderLayout.toggleMenu();
            }
        });

        getSearchData();
    }

    private void logoutUser() {
        session.setLogin(false);

        db.deleteUsers();

        // Launching the login activity
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        App.menu_id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == 1) {
            bottomNavigation.setCurrentItem(0);
            App.vp_message_main.setCurrentItem(0);
            return true;
        }else if (id == android.R.id.home) {
//            onBackPressed();
            sliderLayout.toggleMenu();
            return true;
        }else if (id == 3) {
//            Toast.makeText(MainActivity.this,"Search",Toast.LENGTH_SHORT).show();
            if(mCountries!=null){
//                App.menu_id = 3;
                loadToolBarSearch();

            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if (sliderLayout.isMenuShown()){
            sliderLayout.toggleMenu();
        }

        /*if (getSupportFragmentManager().getBackStackEntryCount() == 1){
            finish();
        }
        else {
            super.onBackPressed();
        }*/

        /*if(App.menu_id == 3){
            toolbarSearchDialog.dismiss();
        }*/

        if (App.menu_id ==5){


        } else  if( !(App.vp_message_main.getCurrentItem() == 0) ){
            bottomNavigation.setCurrentItem(0);
            App.vp_message_main.setCurrentItem(0);
        } else {
//            new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle).setIcon(R.drawable.alert).setTitle("Exit")
            new AlertDialog.Builder(this).setTitle("Exit")
                    .setMessage("Are you sure you want to exit?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            finish();
                        }
                    }).setNegativeButton("No", null).show();
        }

//        MenuItem actionRestart = (MenuItem) findViewById(R.id.back_arrow);
//        onOptionsItemSelected(actionRestart);

    }





    private void enableGPS(){

        try {
            gpsoff = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        if(gpsoff==0){

            Toast.makeText(this, "Please Enable GPS", Toast.LENGTH_SHORT).show();

        }

    }

    private void getForecast() {

        String forecastUrl = "http://rewardago.com/cron/profile.php?id="
                +MainActivity.UID+"?_=" + System.currentTimeMillis();

        if (isNetworkAvailable()){

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            toggleRefresh();
//                            progressBar.setVisibility(View.GONE);
//                            mSwipeRefreshWidget.setRefreshing(false);

                        }
                    });

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
                        String jsonData = response.body().string();
//                        jsonData = response.body().string();
//                        jsonData = stripHtml(response.body().string());



                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//                                Toast.makeText(myContext, jsonData,Toast.LENGTH_LONG).show();
                            }
                        });


//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {

                            mCurrentWeather = getCurrentDetails(jsonData);





                        } else {
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
//                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
//                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        }

        else {
            Toast.makeText(this, getString(R.string.network_unavailable_message), Toast.LENGTH_LONG).show();

        }

    }

    private String getCurrentDetails(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);

        JSONObject currently = forecast.getJSONObject("Profile");

        App.profilePic = currently.getString("Image");
//            blogCoupons.add(currentWeather);
//        }


        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                                    updateDisplay();

               /* new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms


//                        progressBar.setVisibility(View.GONE);
//                        mSwipeRefreshWidget.setRefreshing(false);

                    }
                }, 3000);*/


//                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//                h_adapter = new HRVA(mCurrentWeather, getActivity());
//                recyclerView.setAdapter(App.h_adapter);
//                recyclerView.setAdapter(h_adapter);
//                recyclerView.addItemDecoration(new MyItemDecoration());

//                details.setText(currentWeather);
//                textMyActiveCoup.setText(""+ints1);
//                textMyRedemp.setText(""+ints2);
//                totalAmt.setText(s3);
               /* Picasso.with(getApplicationContext()).load( App.profilePic)
                        .error(R.drawable.placeholder2)
                        .placeholder(R.drawable.placeholder2)
                        .into(profile_image);
                MainActivity.setImg();*/

//                App.adapter_offers.notifyDataSetChanged();

                target = new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        App.profile_image_main.setImageBitmap(bitmap);
                        App.pImage = App.profile_image_main.getDrawable();
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {}

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {}
                };


                        Picasso.with(App.getInstance()).load(App.profilePic)
                                .skipMemoryCache()
                                .memoryPolicy(MemoryPolicy.NO_CACHE)
                                .networkPolicy(NetworkPolicy.NO_CACHE)
                                .error(R.drawable.placeholder2)
                                .placeholder(R.drawable.placeholder2)
                                .into(target);




                



            }

        });

        return currentWeather;
    }


   /* static  class MarkerCallback implements com.squareup.picasso.Callback {
        Drawable marker=null;

        MarkerCallback(Drawable marker) {
            this.marker=marker;
        }

        @Override
        public void onError() {
//            Log.e(getClass().getSimpleName(), "Error loading thumbnail!");
        }

        @Override
        public void onSuccess() {
            if (marker != null) {
                marker.showInfoWindow();
            }
        }
    }*/

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if(networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;

        }

        return isAvailable;
    }

    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(getFragmentManager(), "error_dialog");
    }

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        Picasso.with(this).cancelRequest(target);
    }

    ///////////////////////////////////////////
    /////////////////////////////////////////
    //FOR SEARCHING////////////////////////

    private void mySelection(String string) {
//        Toast.makeText(MainActivity.this,string,Toast.LENGTH_SHORT).show();
        toolbarSearchDialog.dismiss();
//        MainActivity.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        Intent transitionIntent = new Intent(this, CouponDetailsActivity.class);
        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_ENTERING_TPYE, 1);
        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_ID, Integer.parseInt(string));
        startActivity(transitionIntent);
    }

    public void loadToolBarSearch() {


//        ArrayList<String> countryStored = SharedPreference.loadList(MainActivity.this, Utils.PREFS_NAME, Utils.KEY_COUNTRIES);
//        ArrayList<String> countryStored = mCountries; // all history
        ArrayList<String> countryStored = new ArrayList<>(); // empty history
        ArrayList<String> idStored = new ArrayList<>();

        View view = MainActivity.this.getLayoutInflater().inflate(R.layout.view_toolbar_search, null);

        LinearLayout parentToolbarSearch = (LinearLayout) view.findViewById(R.id.parent_toolbar_search);

        ImageView imgToolBack = (ImageView) view.findViewById(R.id.img_tool_back);

        final EditText edtToolSearch = (EditText) view.findViewById(R.id.edt_tool_search);

        ImageView imgToolMic = (ImageView) view.findViewById(R.id.img_tool_mic);

        final ListView listSearch = (ListView) view.findViewById(R.id.list_search);

        final TextView txtEmpty = (TextView) view.findViewById(R.id.txt_empty);

        Utils.setListViewHeightBasedOnChildren(listSearch);

        edtToolSearch.setHint("Search your coupons");

        toolbarSearchDialog = new Dialog(MainActivity.this, R.style.MaterialSearch);
        toolbarSearchDialog.setContentView(view);
        toolbarSearchDialog.setCancelable(false);
        toolbarSearchDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        toolbarSearchDialog.getWindow().setGravity(Gravity.BOTTOM);
        toolbarSearchDialog.show();

        toolbarSearchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        countryStored = (countryStored != null && countryStored.size() > 0) ? countryStored : new ArrayList<String>();
        idStored = (idStored != null && idStored.size() > 0) ? idStored : new ArrayList<String>();

//        final SearchAdapter searchAdapter = new SearchAdapter(MainActivity.this, countryStored, false);
        final SearchAdapter searchAdapter = new SearchAdapter(MainActivity.this, countryStored, false, idStored);

        listSearch.setVisibility(View.VISIBLE);
        listSearch.setAdapter(searchAdapter);


        listSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                String country = String.valueOf(adapterView.getItemAtPosition(position));
//                SharedPreference.addList(MainActivity.this, Utils.PREFS_NAME, Utils.KEY_COUNTRIES, country);
                edtToolSearch.setText(country);
                listSearch.setVisibility(View.GONE);


                mySelection(country);
            }
        });
        edtToolSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

//                String[] country = MainActivity.this.getResources().getStringArray(R.array.countries_array);
//                mCountries = new ArrayList<String>(Arrays.asList(country));
                listSearch.setVisibility(View.VISIBLE);
//                searchAdapter.updateList(mCountries, true);
                searchAdapter.updateList(mCountries, true,mIds);


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ArrayList<String> filterList = new ArrayList<String>();
                ArrayList<String> filterListIds = new ArrayList<String>();
                boolean isNodata = false;
                if (s.length() > 0) {
                    for (int i = 0; i < mCountries.size(); i++) {


//                        if (mCountries.get(i).toLowerCase().startsWith(s.toString().trim().toLowerCase())) {
                        if (mCountries.get(i).toLowerCase().contains(s.toString().trim().toLowerCase())) {

                            filterList.add(mCountries.get(i));
                            filterListIds.add(mIds.get(i));

                            listSearch.setVisibility(View.VISIBLE);
//                            searchAdapter.updateList(filterList, true);
                            searchAdapter.updateList(filterList, true,filterListIds);
                            isNodata = true;
                        }
                    }
                    if (!isNodata) {
                        listSearch.setVisibility(View.GONE);
                        txtEmpty.setVisibility(View.VISIBLE);
                        txtEmpty.setText("No data found");
                    }
                } else {
                    listSearch.setVisibility(View.GONE);
                    txtEmpty.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        imgToolBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toolbarSearchDialog.dismiss();
//                MainActivity.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
            }
        });

        imgToolMic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                edtToolSearch.setText("");

            }
        });


    }

    private void getSearchData() {

//        progressDialog.show();

//        String forecastUrl = "https://api.myjson.com/bins/28d14";
        String forecastUrl = "http://rewardago.com/cron/json.php";

        if (isNetworkAvailable()){

//            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {

                    /*myContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });*/

                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    try {
//                        String jsonData = response.body().string();
                        jsonData = response.body().string();
//                        jsonData = stripHtml(response.body().string());

                        /*runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, jsonData,Toast.LENGTH_LONG).show();
                            }
                        });*/

//                        Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {
//                            getCurrentDetails(jsonData).dismiss();
                            gotSearchData(jsonData);

                        } else {
//                            progressDialog.dismiss();
//                            progressBar.setVisibility(View.GONE);
                            alertUserAboutError();
//                            mSwipeRefreshWidget.setRefreshing(false);
                        }
                    }
                    catch (IOException e) {
//                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
//                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }
            });
        }

        else {
            Toast.makeText(this, getString(R.string.network_unavailable_message),Toast.LENGTH_LONG).show();
//            mSwipeRefreshWidget.setRefreshing(false);   // i think.. an error may occur here, if we put
//            progressBar.setVisibility(View.GONE);
        }

    }

    private void gotSearchData(String jsonData) throws JSONException {

        JSONObject forecast = new JSONObject(jsonData);
//        JSONObject jsonObject = new JSONObject(result);
//                    JSONArray jArray = new JSONArray(result);
//        JSONArray jArray = forecast.getJSONArray("json");
        JSONArray jArray = forecast.getJSONArray("Coupon");

        mCountries = new ArrayList<String>();
        mIds = new ArrayList<String>();

        // Extract data from json and store into ArrayList
        for (int i = 0; i < jArray.length(); i++) {
            JSONObject json_data = jArray.getJSONObject(i);
//            dataList.add(json_data.getString("fish_name"));
            mCountries.add(json_data.getString("Sub_Title"));
            mIds.add(json_data.getString("id"));
        }



/*        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                progressDialog.dismiss();

//                strArrData = countryStored.toArray(new String[countryStored.size()]);
//                loadToolBarSearch();

            }
        });*/

//        return progressDialog;
    }


    @Override
    protected void onResume() {
        super.onResume();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        Picasso.with(this).cancelRequest(target);
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }
}
