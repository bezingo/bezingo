package com.majithg.rewardago.favourite;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.majithg.rewardago.home.content.CouponDetailsActivity;
import com.majithg.rewardago.home.content.Place;
import com.majithg.rewardago.util.App;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by megha on 15-03-06.
 */
public class FavListAdapter extends RecyclerView.Adapter<FavListAdapter.ViewHolder> {

  Context mContext ;
  OnItemClickListener mItemClickListener;
  List<Place> mlist;

  Context applicationContext = MainActivity.getContextOfApplication();

  int clr;

//    Place place;

  public FavListAdapter(Context context, List<Place> list) {
    this.mContext = context;
    this.mlist = list;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_places, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder, int position) {
//    final Place place = new PlaceData().placeList().get(position);


//      Place currentWeather = new Place();
      Place place = mlist.get(position);
//      place = mlist.get(position);

      holder.placeName.setText(place.name);
      holder.placeNameDesc.setText(place.name2); // to remove second subtitle
      //Download image using picasso library
      Picasso.with(mContext).load(place.getImageName())
              .error(R.drawable.placeholder2)
              .placeholder(R.drawable.placeholder2)
              .into(holder.placeImage);
//      holder.placeImage.setImageResource(place.imageResourceId);


      Picasso.with(mContext).load(place.getImageName2())
              .error(R.drawable.placeholder2)
              .placeholder(R.drawable.placeholder2)
              .into(holder.placeImage2);



//    Picasso.with(mContext).load(place.getImageResourceId(mContext)).into(holder.placeImage);
//    Picasso.with(mContext).load(place.getImageResourceId()).into(holder.placeImage);





    // removing color bar color
/*

//    Bitmap photo = BitmapFactory.decodeResource(mContext.getResources(), place.getImageResourceId(mContext));
    Bitmap photo = BitmapFactory.decodeResource(App.getInstance().getResources(), place.getImageResourceId());
    Palette.generateAsync(photo, new Palette.PaletteAsyncListener() {
      public void onGenerated(Palette palette) {
        int mutedLight = palette.getMutedColor(mContext.getResources().getColor(android.R.color.black));
        holder.placeNameHolder.setBackgroundColor(mutedLight);

        clr = mutedLight;

      }
    });
*/


// try it later...
//    Drawable background = null;
//    if (background instanceof ShapeDrawable) {
      // cast to 'ShapeDrawable'
//      ShapeDrawable shapeDrawable = (ShapeDrawable)background;
//      shapeDrawable.getPaint().setColor(mContext.getResources().getColor(R.color.colorToSet));
//      shapeDrawable.getPaint().setColor(clr);
//    holder.addFavouriteImage.setImageDrawable(shapeDrawable);
//    }
    /*else if (background instanceof GradientDrawable) {
      // cast to 'GradientDrawable'
      GradientDrawable gradientDrawable = (GradientDrawable)background;
      gradientDrawable.setColor(getResources().getColor(R.color.colorToSet));
    } else if (background instanceOf ColorDrawable) {
      // alpha value may need to be set again after this call
      Colordrawable colorDrawable = (ColorDrawable)background;
      colorDrawable.setColor(getResources().getColor(R.color.colorToSet));
    }*/
  }
  @Override
  public int getItemCount() {
    return (null != mlist ? mlist.size() : 0);
  }

 /* @Override
  public int getItemCount() {
    return new PlaceData().placeList().size();
  }*/

  public class ViewHolder extends RecyclerView.ViewHolder

//          implements View.OnClickListener
  {
    public LinearLayout placeHolder;
//    public LinearLayout placeNameHolder;
    public LinearLayoutCompat placeNameHolder;
    public TextView placeName;
    public TextView placeNameDesc;
    public ImageView placeImage;
      public ImageView placeImage2;
//    public ImageView addFavouriteImage;

      public Button claimButton;

    public ViewHolder(View itemView) {
      super(itemView);
      placeHolder = (LinearLayout) itemView.findViewById(R.id.mainHolder);
      placeName = (TextView) itemView.findViewById(R.id.placeName);
      placeNameDesc = (TextView) itemView.findViewById(R.id.placeNameDesc);  // to remove empty text space
//      placeNameHolder = (LinearLayout) itemView.findViewById(R.id.placeNameHolder);
      placeNameHolder = (LinearLayoutCompat) itemView.findViewById(R.id.placeNameHolder);
      placeImage = (ImageView) itemView.findViewById(R.id.placeImage11);
        placeImage2 = (ImageView) itemView.findViewById(R.id.placeImage112);
//      addFavouriteImage = (ImageView) itemView.findViewById(R.id.btn_add);


//      placeHolder.setOnClickListener(this);

        claimButton = (Button) itemView.findViewById(R.id.claimButton);
//        claimButton.setVisibility(View.GONE);
        claimButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(App.getInstance(),""+mlist.get(getPosition()).getId(),Toast.LENGTH_SHORT).show();
                Intent transitionIntent = new Intent(v.getContext(), CouponDetailsActivity.class);
                transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_ENTERING_TPYE, 3);
//                transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_ID, mCurrentWeather.get(position).getId());
                transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_ID, mlist.get(getPosition()).getId());
                v.getContext().startActivity(transitionIntent);
            }
        });
    }

    /*@Override
    public void onClick(View v) {
      if (mItemClickListener != null) {
        mItemClickListener.onItemClick(itemView, getPosition());
          Toast.makeText(App.getInstance(),""+getPosition(),Toast.LENGTH_SHORT).show();
      }
    }*/


  }




  public interface OnItemClickListener {
    void onItemClick(View view, int position);
  }

  public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
    this.mItemClickListener = mItemClickListener;
      Toast.makeText(App.getInstance(),"from mItemClickListener",Toast.LENGTH_SHORT).show();
  }



}
