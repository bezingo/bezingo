package com.majithg.rewardago.favourite;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.majithg.rewardago.MainActivity;
import com.majithg.rewardago.R;
import com.majithg.rewardago.home.Home;
import com.majithg.rewardago.home.content.CouponDetailsActivity;
import com.majithg.rewardago.home.content.Place;
import com.majithg.rewardago.home.content.TravelListAdapter;
import com.majithg.rewardago.util.AlertDialogFragment;
import com.majithg.rewardago.util.App;
import com.majithg.rewardago.util.FragmentLifecycle;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * ZLB
 */
public class Favourite extends Fragment implements FragmentLifecycle {
    private static final String ARG_PARAM1 = "param1";
    private static final int limit = 50;
    private static final String TAG = Favourite.class.getSimpleName();

    private int messageType;

    private RecyclerView recyclerView;
    private TextView emptyView;

    private FragmentActivity myContext;


    private FavListAdapter favListAdapter;

    private ProgressBar progressBar;
    private SwipeRefreshLayout mSwipeRefreshWidget;
    private String jsonData;

    private List<Place> mCurrentWeather = new ArrayList<>();
    private StaggeredGridLayoutManager mStaggeredLayoutManager;


    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }

    public Favourite() {
    }

    public static Favourite newInstance(int param1) {
        Favourite fragment = new Favourite();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            messageType = getArguments().getInt(ARG_PARAM1);
        } else {
            messageType = 0;
        }

        setHasOptionsMenu(true);

        getForecast();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.favourite, container, false);


        progressBar = (ProgressBar) root.findViewById(R.id.progress_bar_fav);
        mSwipeRefreshWidget = (SwipeRefreshLayout) root.findViewById(R.id.swipe_refresh_widget_fav);

        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerview_fav);
        emptyView = (TextView) root.findViewById(R.id.empty_view_fav);

        progressBar.setVisibility(View.VISIBLE);


        mSwipeRefreshWidget.setColorScheme(R.color.colorPrimaryDark, R.color.colorPrimaryDark,
                R.color.colorPrimaryDark, R.color.colorPrimaryDark);

        /*mSwipeRefreshWidget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSwipeRefreshWidget.setRefreshing(true);
                getForecast();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {


                        mSwipeRefreshWidget.post(new Runnable() {
                            @Override
                            public void run() {
//                                mSwipeRefreshLayout.setRefreshing(true);
                                mSwipeRefreshWidget.setRefreshing(false);
                            }
                        });


                    }
                }, 2000);

            }
        });*/

        mSwipeRefreshWidget.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getForecast();

                // this is also working...
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {


                        /*mSwipeRefreshWidget.post(new Runnable() {
                            @Override
                            public void run() {
//                                mSwipeRefreshLayout.setRefreshing(true);
                                mSwipeRefreshWidget.setRefreshing(false);
                            }
                        });*/

                        mSwipeRefreshWidget.setRefreshing(false);


                    }
                }, 2000);



            }
        });


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                mSwipeRefreshWidget.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
            }
        }, 5000);








        getFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {

            @Override
            public void onBackStackChanged() {

                if(getFragmentManager().getBackStackEntryCount()==0) {
                    onResume();
                }
            }
        });



        return root;
    }


    private void getForecast() {

        String forecastUrl = "http://rewardago.com/cron/favourite.php?id="
                +MainActivity.UID+"?_=" + System.currentTimeMillis();




        if (isNetworkAvailable()){







            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(forecastUrl)
                    .build();
            Call call = client.newCall(request);
            call.enqueue(new Callback() {



                @Override
                public void onFailure(Request request, IOException e) {
                    stopRefreshing();
                    alertUserAboutError();
                }
                @Override
                public void onResponse(Response response) throws IOException {
                    try {
                        jsonData = stripHtml(response.body().string());
                        if (response.isSuccessful()) {
                            mCurrentWeather = getCurrentDetails(jsonData);
                            stopRefreshing();
                        } else {
                            stopRefreshing();
                            alertUserAboutError();
                        }
                    }
                    catch (IOException e) {
                        stopRefreshing();
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                    catch (JSONException e) {
                        stopRefreshing();
                        Log.e(TAG, getString(R.string.log_exception_caught_error), e);
                    }
                }



            });






        }

        else {
            Toast.makeText(myContext, getString(R.string.network_unavailable_message),Toast.LENGTH_LONG).show();
//            mSwipeRefreshWidget.setRefreshing(false);   // i think.. an error may occur here, if we put
//            progressBar.setVisibility(View.GONE);
//            toggleRefresh();
//            stopRefreshing(); don't use anything here ...
        }

    }

    /*private void toggleRefresh() {
        myContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {



                *//*if(!progressBar.isLayoutRequested()){
                    progressBar.setVisibility(View.GONE);
                }*//*

                *//*if(  !(progressBar.getVisibility() == View.GONE)  ){
                    progressBar.setVisibility(View.GONE);
                }*//*

                if(mSwipeRefreshWidget.isRefreshing()){
                    mSwipeRefreshWidget.setRefreshing(false);
                }


            }
        });
    }*/

    private void stopRefreshing(){

        myContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshWidget.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
            }
        });

    }


    private List<Place> getCurrentDetails(String jsonData) throws JSONException {
        JSONObject forecast = new JSONObject(jsonData);

        JSONArray currently  = forecast.getJSONArray("favourite");

        List<Place> blogCoupons = new ArrayList<>();

        for (int i = 0; i < currently.length(); i++) {

            Place currentWeather = new Place();

            JSONObject post = currently.getJSONObject(i);

//            String title = post.getString("id");
            currentWeather.setId(post.getInt("coup_id"));
            currentWeather.setName(post.getString("title"));
            currentWeather.setName2(post.getString("Subtitle"));
//            currentWeather.setImageResourceId(post.getString("image"));
            currentWeather.setImageName(post.getString("image"));
            currentWeather.setImageName2(post.getString("store_logo"));

            blogCoupons.add(currentWeather);
        }


//        Log.d(TAG, currentWeather.getFormattedTime());


        myContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {



                //TODO plz enable later
                if (mCurrentWeather.isEmpty()) {
                    recyclerView.setVisibility(View.GONE);
                    emptyView.setVisibility(View.VISIBLE);

                }
                else {
                    recyclerView.setVisibility(View.VISIBLE);
                    emptyView.setVisibility(View.GONE);
                }


                mStaggeredLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(mStaggeredLayoutManager);
                recyclerView.setHasFixedSize(true);
                favListAdapter = new FavListAdapter(getActivity(), mCurrentWeather);
                recyclerView.setAdapter(favListAdapter);
//                mAdapter.setOnItemClickListener(onItemClickListener);

/*                favListAdapter.setOnItemClickListener(new FavListAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent transitionIntent = new Intent(view.getContext(), CouponDetailsActivity.class);
//                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_POSITION, position);
//                        Toast.makeText(myContext, "PP "+mCurrentWeather.get(position).getId(),Toast.LENGTH_LONG).show();
                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_ID, mCurrentWeather.get(position).getId());
//                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_TITLE, mCurrentWeather.get(position).getName());
//                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_SUBTITLE, mCurrentWeather.get(position).getName2());
//                        transitionIntent.putExtra(CouponDetailsActivity.EXTRA_PARAM_IMAGE, mCurrentWeather.get(position).getImageName());
                        myContext.startActivity(transitionIntent);
                    }
                });*/


//                progressBar.setVisibility(View.GONE);
//                mSwipeRefreshWidget.setRefreshing(false);

            }

        });


        return blogCoupons;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)
                myContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if(networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;

        }

        return isAvailable;
    }

    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(myContext.getFragmentManager(), "error_dialog");
    }


    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.back_arrow) {
//            bottomNavigation.setCurrentItem(0);
//            vp_message.setCurrentItem(0);
            MainActivity.toolbar.getMenu().clear();
            MainActivity.bottomNavigation.setCurrentItem(0);
            App.vp_message_main.setCurrentItem(0);

            App.HomeCanAddItem = false;

            return true;
        }
        /*else if (id == android.R.id.home) {
//            onBackPressed();
            sliderLayout.toggleMenu();
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onStart() {
        super.onStart();
//        adapter.notifyDataSetChanged();
//        App.adapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
//        adapter.notifyDataSetChanged();
//        App.adapter.notifyDataSetChanged();
//        setMainRecyclerView();
//        setCallLogs(curLog);
        getForecast();
    }

    @Override
    public void onPause() {
        super.onPause();
        mSwipeRefreshWidget.setRefreshing(false);
    }

    @Override
    public void onPauseFragment() {
        Log.i(TAG, "onPauseFragment()");
//        Toast.makeText(getActivity(), "onPauseFragment():" + TAG, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResumeFragment() {
        Log.i(TAG, "onResumeFragment()");
//        Toast.makeText(getActivity(), "onResumeFragment():" + TAG, Toast.LENGTH_SHORT).show();
    }




    public String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }







}
